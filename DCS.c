/**
 * @file   DCS.c
 *
 * @brief  DCS tasks manager.                       \n
 *         DCS, PTT, SD and WiFi initialization.    \n
 *         Data and buffers update.                 \n
 *         Connections and system status check.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "DCS.h"                  // Header file
#include "Protocols/I2C.h"        // Driver for inter-integrated circuit (I2C) communications
#include <avr/io.h>               // Driver for AVR input/output
#include "DCS_Tasks/PTT_Task.h"   // PTT manager task
#include "DCS_Tasks/SD_Task.h"    // SD card manager task
#include "DCS_Tasks/WiFi_Task.h"  // Ubidots connection manager task
#include "Utilities/Utilities.h"  // Various useful definitions, settings and functions

/* Maximum Value for Counters of Cooperative Tasks */
#define COUNTER_PTT_MAX    REPEAT_TIME   // 90 seconds
#define COUNTER_WIFI_MAX   SAMPLE_TIME   // 2 minutes
#define COUNTER_CHECK_MAX  CHECK_PERIOD  // 5 minutes
#define COUNTER_DATA_MAX   SAMPLE_TIME   // 2 minutes

/* Types Definition */
typedef struct task_running {
	uint8_t Init_Task             : 1;
	uint8_t Init_PTT_Task         : 1;
	uint8_t Init_SD_Task          : 1;
	uint8_t Init_WiFi_Task        : 1;
	uint8_t UpdateData_Task       : 1;
	uint8_t UpdateBuffers_Task    : 1;
	uint8_t PTT_Task              : 1;
	uint8_t SD_Task               : 1;
	uint8_t WiFi_Task             : 1;
	uint8_t ConectionsCheck_Task  : 1;
	uint8_t SystemMonitor_Task    : 1;
} task_running_t;

typedef struct counters {
	uint16_t PTT;
	uint16_t WiFi;
	uint16_t Check;
	uint16_t Data;
} counters_t;

/* Internal Functions Declaration */
void initPTT();
void initSD();
void initWiFi(); 

/* Internal Variables */
static led_status_t LedStatus;  // LED status that indicates if PTT, SD and WiFi are OK
static working_status_t WorkingStatus;  // Working status that indicates if PTT, SD and WiFi are working
static task_running_t TaskRunning;  // Indicates what tasks are in progress
static counters_t Counters;  // Counters for task that have subtasks
static uint8_t CounterButtonPressed;  // Counter for time that SD button has been pressed
enum subTaskState_Check_t {CheckPTT, CheckSD, CheckWiFi, InitPTT, InitSD, InitWiFi, AllChecked} SubTaskState_Check;  // Steps to init PTT

/**
 * @brief  Initialize DCS.                                              \n
 *         Set PTT, SD and WiFi status.                                 \n
 *         Initialize LEDs and buttons.                                 \n
 *         Initialize UARTs, I2C and ADC.                               \n
 *         Initialize data structure, PTT buffers and RTC date/time.    \n
 *         Execute PTT, SD and WiFi initialization.                     \n
 *         Reset tasks counters.
 *
 * @param  None
 *
 * @return Nothing
 */
void DCS_Init_Task(){
	// Initially, only init task is executing
	TaskRunning = (task_running_t) {LOCAL_RUNNING, LOCAL_STOPPED, LOCAL_STOPPED, LOCAL_STOPPED, LOCAL_STOPPED, LOCAL_STOPPED, LOCAL_STOPPED, LOCAL_STOPPED, LOCAL_STOPPED, LOCAL_STOPPED, LOCAL_STOPPED};
	
	#if DEBUG_EN
	UART_SendString(UART_DEBUG, "Initializing system...\r\n");
	#endif  // DEBUG_EN
	
	// Initially PTT, SD and WiFi are OK
	LedStatus = (led_status_t) {LOCAL_OFF, LOCAL_OFF, LOCAL_OFF};
	// Initially PTT, SD and WiFi aren't working
	WorkingStatus = (working_status_t) {LOCAL_FALSE, LOCAL_FALSE, LOCAL_FALSE};
	
	InitLEDs();  // Init leds for watchdog
	InitButtons();  // Init buttons
	
	UART_Init(UART_PTT);    // Init UART for PTT
	UART_Init(UART_RS232);  // Init UART for Weather Monitor
	UART_Init(UART_WIFI);   // Init UART for ESP8266
	I2C_Init();             // Init I2C to connect to RTC
	ADC_Init();             // Config ADC to get battery voltage
	Data_Init();            // Initialize data structure
	Buffer_Init();          // Init Buffers for PTT

	#if RTC_EN
	RTC_SetTime(RTC_Make(INIT_HOURS, INIT_MINUTES, INIT_SECONDS, INIT_DAY, INIT_MONTH, INIT_YEAR));  // Init DS3231 with hardcoded time
	#endif  // RTC_EN

	// Init PTT
	#if PTT_EN
	TaskRunning.Init_PTT_Task = LOCAL_RUNNING;
	#if DEBUG_EN
	UART_SendString(UART_DEBUG, "Initializing PTT...\r\n");
	#endif  // DEBUG_EN
	while(TaskRunning.Init_PTT_Task == LOCAL_RUNNING)  // Execute all steps of subtasks
		initPTT();  // Configure and check PTT
	#endif  // PTT_EN
	
	// Init SD
	#if SD_EN
	TaskRunning.Init_SD_Task = LOCAL_RUNNING;
	#if DEBUG_EN
	UART_SendString(UART_DEBUG, "Initializing SD...\r\n");
	#endif  // DEBUG_EN
	initSD();  // Initialize SD card
	#endif  // SD_EN
	
	// Init WiFi (ESP8266)
	#if WIFI_EN
	TaskRunning.Init_WiFi_Task = LOCAL_RUNNING;
	#if DEBUG_EN
	UART_SendString(UART_DEBUG, "Initializing WiFi...\r\n");
	#endif  // DEBUG_EN
	while(TaskRunning.Init_WiFi_Task == LOCAL_RUNNING)  // Execute all steps of subtasks
		initWiFi();;  // Configure and check ESP8266
	#endif  // WIFI_EN
	
	// Reset tasks counters
	Counters = (counters_t) {COUNTER_PTT_MAX, COUNTER_WIFI_MAX, COUNTER_CHECK_MAX, COUNTER_DATA_MAX};
	
	#if DEBUG_EN
	UART_SendString(UART_DEBUG, "Initialization finished\r\n");
	#endif  // DEBUG_EN
	
	TaskRunning.Init_Task = LOCAL_STOPPED;
}

/**
 * @brief  Initialize PTT.                                               \n
 *         The task is set up to work cooperatively, taking advantage
 *         of the PTT initialization subtasks in PTT_Task.c.
 *
 * @param  None
 *
 * @return Nothing
 */
void initPTT(){
	#if PTT_EN
	uint8_t status;
	if(TaskRunning.Init_PTT_Task == LOCAL_RUNNING){
		status = PTT_Task_Init();
		if(status != LOCAL_ERROR){
			WorkingStatus.PTT_WorkingStatus = LOCAL_TRUE;  // PTT is working now
			LedStatus.PTT_LedStatus = LOCAL_OFF;
			if(status == PTT_TASK_COMPLETE){
				TaskRunning.Init_PTT_Task = LOCAL_STOPPED;  // PTT init task is complete
				#if DEBUG_EN
				UART_SendString(UART_DEBUG, "PTT initialized successfully\r\n");
				#endif  // DEBUG_EN
			}
		} else {
			WorkingStatus.PTT_WorkingStatus = LOCAL_FALSE;  // PTT is not working
			LedStatus.PTT_LedStatus = LOCAL_ON;  // Set LED for SystemMonitorTask
			TaskRunning.Init_PTT_Task = LOCAL_STOPPED;  // PTT init task failed
			#if DEBUG_EN
			UART_SendString(UART_DEBUG, "PTT initialization failed\r\n");
			#endif  // DEBUG_EN
		}
	}
	#else
	TaskRunning.Init_PTT_Task = LOCAL_STOPPED;  // PTT is disabled
	#endif  // PTT_EN
}

/**
 * @brief  Initialize ESP8266 WiFi module.                               \n
 *         The task is set up to work cooperatively, taking advantage
 *         of the WiFi initialization subtasks in WiFi_Task.c.
 *
 * @param  None
 *
 * @return Nothing
 */
void initWiFi(){
	#if WIFI_EN
	uint8_t status;
	if(TaskRunning.Init_WiFi_Task == LOCAL_RUNNING){
		status = WiFi_Task_Init();
		if(status != LOCAL_ERROR){
			WorkingStatus.WiFi_WorkingStatus = LOCAL_TRUE;  // WiFi is working now
			LedStatus.WiFi_LedStatus = LOCAL_OFF;
			if(status == WIFI_TASK_COMPLETE){
				TaskRunning.Init_WiFi_Task = LOCAL_STOPPED;  // WiFi init task is complete
				#if DEBUG_EN
				UART_SendString(UART_DEBUG, "WiFi initialized successfully\r\n");
				#endif  // DEBUG_EN
			}
		} else {
			WorkingStatus.WiFi_WorkingStatus = LOCAL_FALSE;  // WiFi is not working
			LedStatus.WiFi_LedStatus = LOCAL_ON;  // Set LED for SystemMonitorTask
			TaskRunning.Init_WiFi_Task = LOCAL_STOPPED;  // WiFi init task failed
			#if DEBUG_EN
			UART_SendString(UART_DEBUG, "WiFi initialization failed\r\n");
			#endif  // DEBUG_EN
		}
	}
	#else
	TaskRunning.Init_WiFi_Task = LOCAL_STOPPED;  // WiFi is disabled
	#endif  // WIFI_EN
}

/**
 * @brief  Initialize SD card.
 *
 * @param  None
 *
 * @return Nothing
 */
void initSD(){
	#if SD_EN
	if(SD_Task_Init() == LOCAL_OK){
		WorkingStatus.SD_WorkingStatus = LOCAL_TRUE;  // SD is working now
		LedStatus.SD_LedStatus = LOCAL_OFF;
		TaskRunning.Init_SD_Task = LOCAL_STOPPED;  // SD init task is complete
		#if DEBUG_EN
		UART_SendString(UART_DEBUG, "SD initialized successfully\r\n");
		#endif  // DEBUG_EN
	} else {
		WorkingStatus.SD_WorkingStatus = LOCAL_FALSE;  // SD is not working
		LedStatus.SD_LedStatus = LOCAL_ON;  // Set LED for SystemMonitorTask
		TaskRunning.Init_SD_Task = LOCAL_STOPPED;  // SD init task failed
		#if DEBUG_EN
		UART_SendString(UART_DEBUG, "SD initialization failed\r\n");
		#endif  // DEBUG_EN
	}
	#endif  // SD_EN
}

/**
 * @brief  Updates data structure.                             \n
 *         The task is set up to work cooperatively, taking
 *         advantage of the data update subtasks in Data.c.
 *
 * @param  None
 *
 * @return Nothing
 */
 void DCS_UpdateData_Task(){
	uint8_t status, start_task=0;
	Counters.Data++;
	if(Counters.Data >= COUNTER_DATA_MAX){  // Start again
		TaskRunning.UpdateData_Task = LOCAL_RUNNING;
		Counters.Data = 0;
		start_task = 1;
	}
	if(TaskRunning.UpdateData_Task == LOCAL_RUNNING){
		#if DEBUG_EN
		if(start_task) UART_SendString(UART_DEBUG, "Updating data...\r\n");
		#endif  // DEBUG_EN
		
		status = Data_Update();  // Update data structure
		if(status == DATA_ERROR){
			#if DEBUG_EN
			UART_SendString(UART_DEBUG, "Failed to update data\r\n");
			#endif  // DEBUG_EN
		} else {
			if(status == DATA_TASK_COMPLETE){
				TaskRunning.UpdateData_Task = LOCAL_STOPPED;  // Update data task is complete
				#if DEBUG_EN
				UART_SendString(UART_DEBUG, "Data updated successfully\r\n");
				#endif  // DEBUG_EN
			}
		}
	}
}

/**
 * @brief  Send data to Ubidots.                                 \n
 *         The task is set up to work cooperatively, taking
 *         advantage of WiFi send data subtasks in WiFi_Task.c.
 *
 * @param  None
 *
 * @return Nothing
 */
void DCS_WiFi_Task(){
	#if WIFI_EN
	uint8_t status, start_task=0;
	Counters.WiFi++;
	if(Counters.WiFi >= COUNTER_WIFI_MAX){  // Start again
		TaskRunning.WiFi_Task = LOCAL_RUNNING;
		Counters.WiFi = 0;
		start_task = 1;
	}
	if((TaskRunning.WiFi_Task == LOCAL_RUNNING) && (WorkingStatus.WiFi_WorkingStatus == LOCAL_TRUE)){  // WiFi is OK and task must execute
		#if DEBUG_EN
		if(start_task) UART_SendString(UART_DEBUG, "Starting WiFi task...\r\n");
		#endif  // DEBUG_EN
		status = WiFi_Task_SendData();  // Try to send data to Ubidots
		if(status == 0){
			LedStatus.PTT_LedStatus = LOCAL_ON;  // WiFi failed --> Set LED for SystemMonitorTask
			TaskRunning.WiFi_Task = LOCAL_STOPPED;  // WiFi task failed
			#if DEBUG_EN
			UART_SendString(UART_DEBUG, "WiFi task failed\r\n");
			#endif  // DEBUG_EN
		} else {
			if(status == WIFI_TASK_COMPLETE){
				TaskRunning.WiFi_Task = LOCAL_STOPPED;  // WiFi task is complete
				#if DEBUG_EN
				UART_SendString(UART_DEBUG, "WiFi task finished\r\n");
				#endif  // DEBUG_EN
			}
		}
	}
	#endif  // WIFI_EN
}

/**
 * @brief  Store in SD all data in the structure.
 *
 * @param  None
 *
 * @return Nothing
 */
void DCS_SD_Task(){
	#if SD_EN
	TaskRunning.SD_Task = LOCAL_RUNNING;
	if(WorkingStatus.SD_WorkingStatus == LOCAL_TRUE){
		#if DEBUG_EN
		UART_SendString(UART_DEBUG, "Starting SD task...\r\n");
		#endif  // DEBUG_EN
		if(SD_Task_StoreData() == LOCAL_ERROR){
			LedStatus.SD_LedStatus = LOCAL_ON;  // SD failed --> Set LED for SystemMonitorTask
			#if DEBUG_EN
			UART_SendString(UART_DEBUG, "SD task failed\r\n");
			#endif  // DEBUG_EN
		}
	}
	TaskRunning.SD_Task = LOCAL_STOPPED;
	#endif  // SD_EN
}

/**
 * @brief  Update PTT buffers.
 *
 * @param  None
 *
 * @return Nothing
 */
void DCS_UpdateBuffers_Task(){
	#if PTT_EN
	TaskRunning.UpdateBuffers_Task = LOCAL_RUNNING;
	
	#if DEBUG_EN
	UART_SendString(UART_DEBUG, "Updating buffers...\r\n");
	#endif  // DEBUG_EN
	
	Buffer_Update();
	TaskRunning.UpdateBuffers_Task = LOCAL_STOPPED;
	#endif  // PTT_EN
}

/**
 * @brief  Send buffer to PTT and start transmission.           \n
 *         The task is set up to work cooperatively, taking
 *         advantage of PTT send data subtasks in PTT_Task.c.
 *
 * @param  None
 *
 * @return Nothing
 */
void DCS_PTT_Task(){
	#if PTT_EN
	uint8_t status, start_task=0;
	Counters.PTT++;
	if(Counters.PTT >= COUNTER_PTT_MAX){  // Start again
		TaskRunning.PTT_Task = LOCAL_RUNNING;
		Counters.PTT = 0;
		start_task = 1;
	}
	if((TaskRunning.PTT_Task == LOCAL_RUNNING) && (WorkingStatus.PTT_WorkingStatus == LOCAL_TRUE)){  // PTT is OK and task must execute
		#if DEBUG_EN
		if(start_task) UART_SendString(UART_DEBUG, "Starting PTT task...\r\n");
		#endif  // DEBUG_EN
		status = PTT_Task_SendData();  // Try to transmit data
		if(status == LOCAL_ERROR){
			LedStatus.PTT_LedStatus = LOCAL_ON;  // PTT failed --> Set LED for SystemMonitorTask
			TaskRunning.PTT_Task = LOCAL_STOPPED;  // PTT task failed
			#if DEBUG_EN
			UART_SendString(UART_DEBUG, "PTT task failed\r\n");
			#endif  // DEBUG_EN
		} else {
			if(status == PTT_TASK_COMPLETE){
				TaskRunning.PTT_Task = LOCAL_STOPPED;  // PTT task is complete
				#if DEBUG_EN
				UART_SendString(UART_DEBUG, "PTT task finished\r\n");
				#endif  // DEBUG_EN
			}
		}
	}
	#endif  // PTT_EN
}

/**
 * @brief  Check if PTT, SD and WiFi are working properly.                       \n
 *         If any of them do not work properly, it is re-initialized.            \n
 *         The task is set up to work cooperatively, checking each device
 *         and re-initializing each device is done in a different subtask.       \n
 *         It also takes advantage of the subtasks of initialization functions.
 *
 * @param  None
 *
 * @return Nothing
 */
void DCS_ConectionsCheck_Task(){
	Counters.Check++;
	if(Counters.Check >= COUNTER_CHECK_MAX){  // Start again
		#if DEBUG_EN
		UART_SendString(UART_DEBUG, "Starting connections checking...\r\n");
		#endif  // DEBUG_EN
		TaskRunning.ConectionsCheck_Task = LOCAL_RUNNING;
		Counters.Check = 0;
		SubTaskState_Check = CheckPTT;  // First step is to check PTT connection
	}
	switch(SubTaskState_Check){
		case CheckPTT:
			if(WorkingStatus.PTT_WorkingStatus == LOCAL_FALSE){
				#if DEBUG_EN && PTT_EN
				UART_SendString(UART_DEBUG, "PTT is not working properly\r\n");
				#endif  // DEBUG_EN && PTT_EN
				SubTaskState_Check = InitPTT;  // Next step is to init PTT
			} else {
				SubTaskState_Check = CheckSD;  // Next step is to check SD connection
			}
			break;
		case CheckSD:
			if(WorkingStatus.SD_WorkingStatus == LOCAL_FALSE){
				#if DEBUG_EN && SD_EN
				UART_SendString(UART_DEBUG, "SD is not working properly\r\n");
				#endif  // DEBUG_EN && SD_EN
				SubTaskState_Check = InitSD;  // Next step is to init SD
			} else {
				SubTaskState_Check = CheckWiFi;  // Next step is to check WiFi connection
			}
			break;
		case CheckWiFi:
			if(WorkingStatus.WiFi_WorkingStatus == LOCAL_FALSE){
				#if DEBUG_EN && WIFI_EN
				UART_SendString(UART_DEBUG, "WiFi is not working properly\r\n");
				#endif  // DEBUG_EN && WIFI_EN
				SubTaskState_Check = InitWiFi;  // Next step is to init WiFi
			} else {
				SubTaskState_Check = AllChecked;  // All modules have been checked
			}
			break;
		case InitPTT:
			TaskRunning.Init_PTT_Task = LOCAL_RUNNING;
			initPTT();
			if(TaskRunning.Init_PTT_Task == LOCAL_STOPPED){
				SubTaskState_Check = CheckSD;  // Next step is to check SD connection
			}
			break;
		case InitSD:
			TaskRunning.Init_SD_Task = LOCAL_RUNNING;
			initSD();
			SubTaskState_Check = CheckWiFi;  // Next step is to check WiFi connection
			break;
		case InitWiFi:
			TaskRunning.Init_WiFi_Task = LOCAL_RUNNING;
			initWiFi();
			if(TaskRunning.Init_WiFi_Task == LOCAL_STOPPED){
				SubTaskState_Check = AllChecked;  // All modules have been checked
			}
			break;
		case AllChecked:
			TaskRunning.ConectionsCheck_Task = LOCAL_STOPPED;
			break;
		default: break;
	}
}

/**
 * @brief  Check system status (PTT, SD and WiFi status).    \n
 *         Set PTT, SD and WiFi LEDs values.                 \n
 *         Check button press and SD extraction.             \n
 *         Store system status in SD and WiFi buffers.
 *
 * @param  None
 *
 * @return Nothing
 */
void DCS_SystemMonitor_Task(){
	TaskRunning.SystemMonitor_Task = LOCAL_RUNNING;
	static uint8_t ledStatus = 1;
	
	#if SD_BUTTON_EN
	if(Ports_Read(BUTTON_SD_IN, BUTTON_SD_PIN) == LOCAL_PRESSED){
		CounterButtonPressed++;
		if(CounterButtonPressed >= TIME_SD_EXTRACT){
			CounterButtonPressed = 0;
			if(SD_End() == SD_OK){
				WorkingStatus.SD_WorkingStatus = LOCAL_FALSE;
				LedStatus.SD_LedStatus = LOCAL_OFF;
				#if DEBUG_EN
				UART_SendString(UART_DEBUG, "SD card removed\r\n");
				#endif  // DEBUG_EN
			} else {
				#if DEBUG_EN
				UART_SendString(UART_DEBUG, "Failed to remove SD card\r\n");
				#endif  // DEBUG_EN
			}
		}
	} else {
		CounterButtonPressed = 0;
	}
	#endif  // SD_BUTTON_EN
	
	// RGB LED is for general DCS status
	// If everything is working, green LED should be on
	if((LedStatus.PTT_LedStatus == LOCAL_ON) || (LedStatus.SD_LedStatus == LOCAL_ON) || (LedStatus.WiFi_LedStatus == LOCAL_ON)){
		Ports_Clear(LED_GREEN_GPIO, LED_GREEN_PIN);
		Ports_Set(LED_RED_GPIO, LED_RED_PIN);
		// Check what is not working and toggle its associated LED or send/store its state
		Ports_SetValue(LED_PTT_GPIO, LED_PTT_PIN, LedStatus.PTT_LedStatus);
		Ports_SetValue(LED_SD_GPIO, LED_SD_PIN, LedStatus.SD_LedStatus);
		Ports_SetValue(LED_WIFI_GPIO, LED_WIFI_PIN, LedStatus.WiFi_LedStatus);
	} else {
		// Everything is working
		Ports_Clear(LED_PTT_GPIO, LED_SD_PIN);
		Ports_Clear(LED_SD_GPIO, LED_SD_PIN);
		Ports_Clear(LED_WIFI_GPIO, LED_WIFI_PIN);
		Ports_Clear(LED_RED_GPIO, LED_RED_PIN);
		Ports_Set(LED_GREEN_GPIO, LED_GREEN_PIN);
	}
	SD_Task_StoreSystemStatus(LedStatus);    // Store status of PTT, SD and WiFi in SD buffer
	WiFi_Task_StoreSystemStatus(LedStatus);  // Store status of PTT, SD and WiFi in WiFi buffer
	TaskRunning.SystemMonitor_Task = LOCAL_STOPPED;
}