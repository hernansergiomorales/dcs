/**
 * @file   WiFi_Task.h
 *
 * @brief  WiFi_Task.c header.                \n
 *         Ubidots connection manager task.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _WIFI_TASK_H_
#define _WIFI_TASK_H_

/* Inclusions */
#include <avr/io.h>  // Driver for AVR input/output
#include "../DCS.h"  // DCS tasks manager

/* Constants for WiFi Task */
#define WIFI_TASK_COMPLETE  0xFF  // Code that indicates WiFi task has been completed
#define WIFI_MAX_ATTEMPTS   10    // Maximum number of attempts when joining an access point
// Constants for Ubidots
#define WIFI_UBIDOTS_DEVICE  "dcs"  // Ubidots's device name that contains variables to show
#define WIFI_UBIDOTS_TOKEN   "BBFF-O3R2mZfT9SqSstj959EcQlP6TJALmM"  // Access token for Ubidots
// WiFi Buffer Positions
#define WIFI_POSITION_INSIDE_TEMPERATURE   0
#define WIFI_POSITION_OUTSIDE_TEMPERATURE  18
#define WIFI_POSITION_DEW_POINT            37
#define WIFI_POSITION_INSIDE_HUMIDITY      52
#define WIFI_POSITION_OUTSIDE_HUMIDITY     67
#define WIFI_POSITION_WIND_SPEED           85
#define WIFI_POSITION_WIND_CHILL           103
#define WIFI_POSITION_WIND_DIRECTION       120
#define WIFI_POSITION_BAROMETRIC_PRESSURE  137
#define WIFI_POSITION_DAILY_RAIN           154
#define WIFI_POSITION_YEARLY_RAIN          168
#define WIFI_POSITION_BATTERY_VOLTAGE      184
#define WIFI_POSITION_PTT_STATE            197
#define WIFI_POSITION_WIFI_STATE           207
#define WIFI_POSITION_SD_STATE             217

/* Functions Declaration */
uint8_t WiFi_Task_Init();
uint8_t WiFi_Task_SendData();
void WiFi_Task_StoreSystemStatus(led_status_t ledStatus);

#endif  // _WIFI_TASK_H_