/**
 * @file   PTT_Task.h
 *
 * @brief  PTT_Task.c header.   \n
 *         PTT manager task.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _PTT_TASK_H_
#define _PTT_TASK_H_

/* Inclusions */
#include <avr/io.h>  // Driver for AVR input/output
#include "../DCS.h"  // DCS tasks manager

/* Constants for PTT Task */
#define PTT_TASK_COMPLETE 0xFF  // Code that indicates PTT task has been completed

/* Functions Declaration */
uint8_t PTT_Task_Init();
uint8_t PTT_Task_SendData();

#endif  // _PTT_TASK_H_