/**
 * @file   SD_Task.h
 *
 * @brief  SD_Task.c header.      \n
 *         SD card manager task.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _SD_TASK_H_
#define _SD_TASK_H_

/* Inclusions */
#include <avr/io.h>  // Driver for AVR input/output
#include "../DCS.h"  // DCS tasks manager

/* Constants for SD Task */
#define SD_BUFFER_LENGTH  247
#define SD_FILENAME       "Datalog.txt"  // Name of log file on SD card
// SD Buffer Positions
#define SD_POSITION_DAY                          0
#define SD_POSITION_MONTH                        3
#define SD_POSITION_YEAR                         6
#define SD_POSITION_HOUR                         11
#define SD_POSITION_MINUTE                       14
#define SD_POSITION_SECOND                       17
#define SD_POSITION_CURRENT_INSIDE_TEMPERATURE   21
#define SD_POSITION_AVERAGE_INSIDE_TEMPERATURE   28
#define SD_POSITION_MAX_INSIDE_TEMPERATURE       35
#define SD_POSITION_MIN_INSIDE_TEMPERATURE       42
#define SD_POSITION_CURRENT_OUTSIDE_TEMPERATURE  49
#define SD_POSITION_AVERAGE_OUTSIDE_TEMPERATURE  56
#define SD_POSITION_MAX_OUTSIDE_TEMPERATURE      63
#define SD_POSITION_MIN_OUTSIDE_TEMPERATURE      70
#define SD_POSITION_CURRENT_DEW_POINT            77
#define SD_POSITION_AVERAGE_DEW_POINT            83
#define SD_POSITION_MAX_DEW_POINT                89
#define SD_POSITION_MIN_DEW_POINT                95
#define SD_POSITION_CURRENT_INSIDE_HUMIDITY      101
#define SD_POSITION_AVERAGE_INSIDE_HUMIDITY      106
#define SD_POSITION_MAX_INSIDE_HUMIDITY          111
#define SD_POSITION_MIN_INSIDE_HUMIDITY          116
#define SD_POSITION_CURRENT_OUTSIDE_HUMIDITY     121
#define SD_POSITION_AVERAGE_OUTSIDE_HUMIDITY     126
#define SD_POSITION_MAX_OUTSIDE_HUMIDITY         131
#define SD_POSITION_MIN_OUTSIDE_HUMIDITY         136
#define SD_POSITION_CURRENT_WIND_SPEED           141
#define SD_POSITION_AVERAGE_WIND_SPEED           146
#define SD_POSITION_MAX_WIND_SPEED               151
#define SD_POSITION_MIN_WIND_SPEED               156
#define SD_POSITION_CURRENT_WIND_CHILL           161
#define SD_POSITION_AVERAGE_WIND_CHILL           167
#define SD_POSITION_MAX_WIND_CHILL               173
#define SD_POSITION_MIN_WIND_CHILL               179
#define SD_POSITION_CURRENT_WIND_DIRECTION       185
#define SD_POSITION_CURRENT_BAROMETRIC_PRESSURE  190
#define SD_POSITION_AVERAGE_BAROMETRIC_PRESSURE  198
#define SD_POSITION_MAX_BAROMETRIC_PRESSURE      206
#define SD_POSITION_MIN_BAROMETRIC_PRESSURE      214
#define SD_POSITION_DAILY_RAIN                   222
#define SD_POSITION_YEARLY_RAIN                  227
#define SD_POSITION_CURRENT_BATTERY_VOLTAGE      232
#define SD_POSITION_PTT_STATE                    238
#define SD_POSITION_WIFI_STATE                   241
#define SD_POSITION_SD_STATE                     244

/* Functions Declaration */
uint8_t SD_Task_Init();
uint8_t SD_Task_StoreData();
void SD_Task_StoreSystemStatus(led_status_t ledStatus);

#endif  // _SD_TASK_H_