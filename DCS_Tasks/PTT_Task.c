/**
 * @file   PTT_Task.c
 *
 * @brief  PTT manager task.                                             \n
 *         Initialize PTT, send buffers to PTT and start transmission.
 * 
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "PTT_Task.h"   // Header file

/* Sub-states of PTT init and PTT task */
enum subInitState_PTT_t {CheckConnection, Stop, SetMode} SubInitState_PTT = CheckConnection;  // Steps to init PTT
enum subTaskState_PTT_t {Init, Check, StoreCommand, StoreData, Start} SubTaskState_PTT;  // Steps to send data to PTT

/* Internal Variables */
static uint8_t Buffer[BUFFER_LENGTH];  // Buffer to send to PTT
static uint8_t BufferNumber = 0;  // Number of buffer to send (0-2)
static uint16_t Step;  // Sending buffer current step

/**
 * @brief  Initialize PTT.                                                      \n
 *         Check PTT status and set PTT mode.                                   \n
 *         The task is set up to work cooperatively, the task is divided into
 *         subtasks and on each function call one of the parts is executed.
 *
 * @param  None
 *
 * @return Task status: error, OK, complete.
 */
uint8_t PTT_Task_Init(){
	switch(SubInitState_PTT){
		case CheckConnection:
			if(PTT_Nop() == PTT_ACK){  // Check if PTT is connected
				SubInitState_PTT = Stop;  // Next step is to stop PTT
				return LOCAL_OK;  // Return OK
			}
			break;
		case Stop:
			if(PTT_Stop() == PTT_ACK){  // Stop PTT just in case it was running
				SubInitState_PTT = SetMode;  // Next step is to set PTT mode
				return LOCAL_OK;  // Return OK
			}
			break;
		case SetMode:
			if(PTT_SetMode(PTT_STMODE_AUTOREPEAT_OFF | PTT_STMODE_BUF1) == PTT_ACK){  // Set PTT mode: Buffer 1 & Autorepeat off
				SubInitState_PTT = CheckConnection;  // First step is to check if PTT is connected
				SubTaskState_PTT = Init;  // First step is to select buffer and to calculate IDUA
				return PTT_TASK_COMPLETE;  // PTT init task has been completed
			}
			break;
		default:
			break;
	}
	return LOCAL_ERROR;  // PTT init failed
}

/**
 * @brief  Send buffer to PTT.                                                   \n
 *         Calculate IDUA, check PTT status, send data and start transmission.   \n
 *         The task is set up to work cooperatively, the task is divided into
 *         subtasks and on each function call one of the parts is executed.
 *
 * @param  None
 *
 * @return Task status: error, OK, complete.
 */
uint8_t PTT_Task_SendData(){
	uint16_t idua;
	rtc_t rtc;
	
	switch(SubTaskState_PTT){
		case Init:
			Buffer_Get(BufferNumber, Buffer);  // Get buffer
			#if RTC_EN
			RTC_GetTime(&rtc);
			#endif  // RTC_EN
			idua = (rtc.hh * 3600 + rtc.mm * 60 + rtc.ss) % OBSERVATION_TIME;  // Set IDUA as current time - observation time
			Buffer[BUFFER_IDUA] = idua >> 8;
			Buffer[BUFFER_IDUA + 1] = (uint8_t)idua;
			SubTaskState_PTT = Check;  // Next step is to check Monitor connection
			Step = 0;
			return LOCAL_OK;  // Return OK
			break;
		case Check:
			if(PTT_Nop() == PTT_ACK){  // Wait until PTT is ready
				SubTaskState_PTT = StoreCommand;  // Next step is to send STDATA command
				return LOCAL_OK;  // Return OK
			}
			break;
		case StoreCommand:
			if(PTT_StoreData(PTT_BUF1, 32, Buffer, &Step) == PTT_ACK){  // Store the data
				SubTaskState_PTT = StoreData;  // Next step is to send buffered data
				return LOCAL_OK;  // Return OK
			}
			break;
		case StoreData:
			if(PTT_StoreData(PTT_BUF1, 32, Buffer, &Step) == PTT_TRANSMISSION_COMPLETE){  // Store the data
				SubTaskState_PTT = Start;  // Next step is to start transmission
			}
			return LOCAL_OK;  // Return OK
			break;
		case Start:
			if(PTT_Start() == PTT_ACK){  // Start sending the data
				BufferNumber = (BufferNumber + 2) % 3;  // Update buffer number
				SubTaskState_PTT = Init;  // First step is to select buffer and to calculate IDUA
				return PTT_TASK_COMPLETE;  // PTT task has been completed
			}
			break;
		default:
			break;
	}
	return LOCAL_ERROR;  // Transmission to PTT failed
}