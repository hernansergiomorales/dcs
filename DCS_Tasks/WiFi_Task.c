/**
 * @file   WiFi_Task.c
 * 
 * @brief  Ubidots connection manager task.                                           \n
 *         Initialize ESP8266 module, establish connection to Ubidots and send data. 
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "WiFi_Task.h"  // Header file
#include <stdio.h>      // Standard input/output library

/* Internal Functions Declaration */
void makeBufferWiFi();

/* Fragments of message to send to Ubidots */
#define WIFI_UBIDOTS_MESSAGE_PART1  "POST /api/v1.6/devices/"  // First part of message to send to Ubidots
#define WIFI_UBIDOTS_MESSAGE_PART2  "/?token="  // Second part of message to send to Ubidots
#define WIFI_UBIDOTS_MESSAGE_PART3  " HTTP/1.1\r\nHost: things.ubidots.com\r\nContent-Type: application/json\r\nContent-Length: 230\r\n\r\n"  // Third part of message to send to Ubidots
#define WIFI_UBIDOTS_MESSAGE_PART4  "{\"in-temp\":STT.T, \"out-temp\":STT.T, \"dew-point\":DD.D, \"in-hum\":HHH, \"out-hum\":HHH, \"wind-speed\":WWW, \"wind-chill\":WW.W, \"wind-dir\":WWW, \"bar-press\":BB.BBB, \"d-rain\":DDD, \"y-rain\":YYY, \"bat-volt\":V.VV, \"p-st\":P, \"w-st\":W, \"s-st\":S}"  // Last part of message to send to Ubidots
/* Buffer to send to Ubidots */
#define WIFI_BUFFER_LENGTH (sizeof(WIFI_UBIDOTS_DEVICE) + sizeof(WIFI_UBIDOTS_TOKEN) + sizeof(WIFI_UBIDOTS_MESSAGE_PART1) + sizeof(WIFI_UBIDOTS_MESSAGE_PART2) + sizeof(WIFI_UBIDOTS_MESSAGE_PART3) + sizeof(WIFI_UBIDOTS_MESSAGE_PART4) - 6)
#define OFFSET (sizeof(WIFI_UBIDOTS_DEVICE) + sizeof(WIFI_UBIDOTS_TOKEN) + sizeof(WIFI_UBIDOTS_MESSAGE_PART1) + sizeof(WIFI_UBIDOTS_MESSAGE_PART2) + sizeof(WIFI_UBIDOTS_MESSAGE_PART3) - 5 + 11)

/* Internal Variables */
static uint8_t WiFi_Buffer[WIFI_BUFFER_LENGTH];  // Buffer to send to Ubidots
static uint8_t JoinAP_Counter;                   // Counter to not exceed the maximum number of attempts when joining an AP

/* Sub-states of WiFi init and WiFi task */
enum subInitState_WiFi_t {Begin, WiFiMode, ConnectionMode, ApplicationMode, CheckConnected, JoinAP} SubInitState_WiFi = Begin;  // Steps to init PTT
enum SubTaskState_Wifi_t {Connect, CheckConnectionEstablished, SendData, Close, CheckConnectionClosed} SubTaskState_Wifi;  // Steps to send data to Ubidots

/**
 * @brief  WiFi connection initialization.                                      \n
 *         Check ESP8266 module status, set connection mode,
 *         connect to an access point and check WiFi connection.                \n
 *         The task is set up to work cooperatively, the task is divided into
 *         subtasks and on each function call one of the parts is executed.
 *
 * @param  None
 *
 * @return Task status: error, OK, complete.
 */
uint8_t WiFi_Task_Init(){
	enum ESP8266_CONNECT_STATUS status;
	switch(SubInitState_WiFi){
		case Begin:
			JoinAP_Counter = 0;
			if(ESP8266_Begin() == ESP8266_RESPONSE_FINISHED){  // Check if ESP8266 is working
				SubInitState_WiFi = WiFiMode;  // Next step is to set WiFi mode
				return LOCAL_OK;
			}
			break;
		case WiFiMode:
			if(ESP8266_WIFIMode(ESP8266_BOTH_STATION_AND_ACCESSPOINT) == ESP8266_RESPONSE_FINISHED){  // Set WiFi mode: Station & Access point
				SubInitState_WiFi = ConnectionMode;  // Next step is to set connection mode
				return LOCAL_OK;
			}
			break;
		case ConnectionMode:
			if(ESP8266_ConnectionMode(ESP8266_SINGLE) == ESP8266_RESPONSE_FINISHED){  // Set connection mode: Single connection
				SubInitState_WiFi = ApplicationMode;  // Next step is to set application mode
				return LOCAL_OK;
			}
			break;
		case ApplicationMode:
			if(ESP8266_ApplicationMode(ESP8266_NORMAL) == ESP8266_RESPONSE_FINISHED){  // Set transmission mode: Normal
				SubInitState_WiFi = CheckConnected;  // Next step is to check WiFi connection
				return LOCAL_OK;
			}
			break;
		case CheckConnected:
			status = ESP8266_ConnectionStatus();  // Get ESP8266 status
			if((status == ESP8266_NOT_CONNECTED_TO_AP) && (JoinAP_Counter < WIFI_MAX_ATTEMPTS)){  // Check if WiFi is connected
				SubInitState_WiFi = JoinAP;  // Next step is to join to an Access Point
				JoinAP_Counter++;
				return LOCAL_OK;
			}
			if(status == ESP8266_ConnectionStatus_TO_AP){	// Check if WiFi is connected
				// Make buffer to send to Ubidots
				sprintf((char*)WiFi_Buffer, "%s%s%s%s%s%s", WIFI_UBIDOTS_MESSAGE_PART1, WIFI_UBIDOTS_DEVICE, WIFI_UBIDOTS_MESSAGE_PART2, WIFI_UBIDOTS_TOKEN, WIFI_UBIDOTS_MESSAGE_PART3, WIFI_UBIDOTS_MESSAGE_PART4);
				SubInitState_WiFi = Begin;  // First step on next initialization is to check if ESP8266 is OK
				SubTaskState_Wifi = Connect;  // Next step is to establish connection with Ubidots
				return WIFI_TASK_COMPLETE;
			}
			break;
		case JoinAP:
			if(ESP8266_JoinAccessPoint(ESP8266_SSID, ESP8266_PASSWORD) == ESP8266_WIFI_CONNECTED){  // Connect to an access point
				SubInitState_WiFi = CheckConnected;  // Next step is to check WiFi connection again
				return LOCAL_OK;
			}
			break;
		default:
			break;
	}
	ESP8266_Reset();  // ESP8266 don't work properly  -->  Restart it
	return LOCAL_ERROR;  // ESP8266 init and WiFi connection failed
}

/**
 * @brief  Manage Ubidots connection.                                                  \n
 *         Check ESP8266 status, connect to Ubidots, send data and close connection.   \n
 *         The task is set up to work cooperatively, the task is divided into
 *         subtasks and on each function call one of the parts is executed.
 *
 * @param  None
 *
 * @return Task status: error, OK, complete.
 */
uint8_t WiFi_Task_SendData(){
	switch(SubTaskState_Wifi){
		case Connect:
			if(ESP8266_Begin() == ESP8266_RESPONSE_FINISHED){  // Check ESP8266 connection
				ESP8266_Start(0, ESP8266_DOMAIN, ESP8266_PORT);  // Start TCP connection to Ubidots
				SubTaskState_Wifi = CheckConnectionEstablished;  // Next step is to check if connection is established
				return LOCAL_OK;
			}
			break;
		case CheckConnectionEstablished:
			if(ESP8266_ConnectionStatus() == ESP8266_CREATED_TRANSMISSION){  // Connection established
				makeBufferWiFi();  // Make buffer to send to Ubidots
				SubTaskState_Wifi = SendData;  // Next step is to send data to Ubidots
				return LOCAL_OK;
			}
			break;
		case SendData:
			ESP8266_Send(WiFi_Buffer);  // Send data to Ubidots
			SubTaskState_Wifi = Close;  // Next step is to close the connection
			return LOCAL_OK;
			break;
		case Close:
			ESP8266_Close();  // Close the connection
			SubTaskState_Wifi = CheckConnectionClosed;  // Next step is to check if connection is closed
			return LOCAL_OK;
			break;
		case CheckConnectionClosed:
			if(ESP8266_ConnectionStatus() == ESP8266_TRANSMISSION_DISCONNECTED){  // Connection closed
				SubTaskState_Wifi = Connect;  // First step is to establish connection with Ubidots
				return WIFI_TASK_COMPLETE;
			}
			break;
		default:
			break;
	}
	ESP8266_Reset();  // ESP8266 don't work properly  -->  Restart it
	return LOCAL_ERROR;  // Connection with Ubidots failed
}

/**
 * @brief  Store system status on buffer to send to Ubidots.             \n
 *         Store on buffer PTT, SD and WiFi status (they work or not).
 *
 * @param  ledStatus : System status (PTT, SD and WiFi module).
 *
 * @return Nothing
 */
void WiFi_Task_StoreSystemStatus(led_status_t ledStatus){
	if(ledStatus.PTT_LedStatus)
		WiFi_Buffer[OFFSET + WIFI_POSITION_PTT_STATE] = '0';
	else
		WiFi_Buffer[OFFSET + WIFI_POSITION_PTT_STATE] = '1';
	
	if(ledStatus.SD_LedStatus)
		WiFi_Buffer[OFFSET + WIFI_POSITION_SD_STATE] = '0';
	else
		WiFi_Buffer[OFFSET + WIFI_POSITION_SD_STATE] = '1';
	
	if(ledStatus.WiFi_LedStatus)
		WiFi_Buffer[OFFSET + WIFI_POSITION_WIFI_STATE] = '0';
	else
		WiFi_Buffer[OFFSET + WIFI_POSITION_WIFI_STATE] = '1';
}

/**
 * @brief  Make buffer with data to send to Ubidots.                               \n
 *         Store on buffer values of intern and extern temperature and humidity,
 *         dew point, wind speed and direction, wind chill, barometric pressure,
 *         daily and yearly rain, and battery voltage.
 *
 * @param  None
 *
 * @return Nothing
 */
void makeBufferWiFi(){
	uint16_t data;
	
	// Inside Temperature
	Data_GetCurrentInsideTemperature((int16_t*) &data);
	if((int16_t)data < 0){
		WiFi_Buffer[OFFSET + WIFI_POSITION_INSIDE_TEMPERATURE] = '-';
	} else {
		WiFi_Buffer[OFFSET + WIFI_POSITION_INSIDE_TEMPERATURE] = ' ';
	}
	if((data / 100) % 10){
		WiFi_Buffer[OFFSET + WIFI_POSITION_INSIDE_TEMPERATURE + 1] = Int2Char((data / 100) % 10);
	} else {
		WiFi_Buffer[OFFSET + WIFI_POSITION_INSIDE_TEMPERATURE + 1] = ' ';
	}
	WiFi_Buffer[OFFSET + WIFI_POSITION_INSIDE_TEMPERATURE + 2] = Int2Char((data / 10) % 10);
	WiFi_Buffer[OFFSET + WIFI_POSITION_INSIDE_TEMPERATURE + 4] = Int2Char(data % 10);

	// Outside Temperature
	Data_GetCurrentOutsideTemperature((int16_t*) &data);
	if((int16_t)data < 0){
		WiFi_Buffer[OFFSET + WIFI_POSITION_OUTSIDE_TEMPERATURE] = '-';
	} else {
		WiFi_Buffer[OFFSET + WIFI_POSITION_OUTSIDE_TEMPERATURE] = ' ';
	}
	if((data / 100) % 10){
		WiFi_Buffer[OFFSET + WIFI_POSITION_OUTSIDE_TEMPERATURE + 1] = Int2Char((data / 100) % 10);
	} else {
		WiFi_Buffer[OFFSET + WIFI_POSITION_OUTSIDE_TEMPERATURE + 1] = ' ';
	}
	WiFi_Buffer[OFFSET + WIFI_POSITION_OUTSIDE_TEMPERATURE + 2] = Int2Char((data / 10) % 10);
	WiFi_Buffer[OFFSET + WIFI_POSITION_OUTSIDE_TEMPERATURE + 4] = Int2Char(data % 10);

	// Dew Point
	Data_GetCurrentDewPoint(&data);
	if((data / 100) % 10){
		WiFi_Buffer[OFFSET + WIFI_POSITION_DEW_POINT] = Int2Char((data / 100) % 10);
	} else {
		WiFi_Buffer[OFFSET + WIFI_POSITION_DEW_POINT] = ' ';
	}
	WiFi_Buffer[OFFSET + WIFI_POSITION_DEW_POINT + 1] = Int2Char((data / 10) % 10);
	WiFi_Buffer[OFFSET + WIFI_POSITION_DEW_POINT + 3] = Int2Char(data % 10);

	// Inside Humidity
	Data_GetCurrentInsideHumidity(&data);
	if((data / 100) % 10){
		WiFi_Buffer[OFFSET + WIFI_POSITION_INSIDE_HUMIDITY] = Int2Char((data / 100) % 10);
		WiFi_Buffer[OFFSET + WIFI_POSITION_INSIDE_HUMIDITY + 1] = Int2Char((data / 10) % 10);
	} else {
		WiFi_Buffer[OFFSET + WIFI_POSITION_INSIDE_HUMIDITY] = ' ';
		if((data / 10) % 10){
			WiFi_Buffer[OFFSET + WIFI_POSITION_INSIDE_HUMIDITY + 1] = Int2Char((data / 10) % 10);
		} else {
			WiFi_Buffer[OFFSET + WIFI_POSITION_INSIDE_HUMIDITY + 1] = ' ';
		}
	}
	WiFi_Buffer[OFFSET + WIFI_POSITION_INSIDE_HUMIDITY + 2] = Int2Char(data % 10);

	// Outside Humidity
	Data_GetCurrentOutsideHumidity(&data);
	if((data / 100) % 10){
		WiFi_Buffer[OFFSET + WIFI_POSITION_OUTSIDE_HUMIDITY] = Int2Char((data / 100) % 10);
		WiFi_Buffer[OFFSET + WIFI_POSITION_OUTSIDE_HUMIDITY + 1] = Int2Char((data / 10) % 10);
	} else {
		WiFi_Buffer[OFFSET + WIFI_POSITION_OUTSIDE_HUMIDITY] = ' ';
		if((data / 10) % 10){
			WiFi_Buffer[OFFSET + WIFI_POSITION_OUTSIDE_HUMIDITY + 1] = Int2Char((data / 10) % 10);
		} else {
			WiFi_Buffer[OFFSET + WIFI_POSITION_OUTSIDE_HUMIDITY + 1] = ' ';
		}
	}
	WiFi_Buffer[OFFSET + WIFI_POSITION_OUTSIDE_HUMIDITY + 2] = Int2Char(data % 10);

	// Wind Speed
	Data_GetCurrentWindSpeed(&data);
	if((data / 100) % 10){
		WiFi_Buffer[OFFSET + WIFI_POSITION_WIND_SPEED] = Int2Char((data / 100) % 10);
		WiFi_Buffer[OFFSET + WIFI_POSITION_WIND_SPEED + 1] = Int2Char((data / 10) % 10);
	} else {
		WiFi_Buffer[OFFSET + WIFI_POSITION_WIND_SPEED] = ' ';
		if((data / 10) % 10){
			WiFi_Buffer[OFFSET + WIFI_POSITION_WIND_SPEED + 1] = Int2Char((data / 10) % 10);
		} else {
			WiFi_Buffer[OFFSET + WIFI_POSITION_WIND_SPEED + 1] = ' ';
		}
	}
	WiFi_Buffer[OFFSET + WIFI_POSITION_WIND_SPEED + 2] = Int2Char(data % 10);

	// Wind Chill
	Data_GetCurrentWindChill(&data);
	if((data / 100) % 10){
		WiFi_Buffer[OFFSET + WIFI_POSITION_WIND_CHILL] = Int2Char((data / 100) % 10);
	} else {
		WiFi_Buffer[OFFSET + WIFI_POSITION_WIND_CHILL] = ' ';
	}
	WiFi_Buffer[OFFSET + WIFI_POSITION_WIND_CHILL + 1] = Int2Char((data / 10) % 10);
	WiFi_Buffer[OFFSET + WIFI_POSITION_WIND_CHILL + 3] = Int2Char(data % 10);

	// Wind Direction
	Data_GetCurrentWindDirection(&data);
	if((data / 100) % 10){
		WiFi_Buffer[OFFSET + WIFI_POSITION_WIND_DIRECTION] = Int2Char((data / 100) % 10);
		WiFi_Buffer[OFFSET + WIFI_POSITION_WIND_DIRECTION + 1] = Int2Char((data / 10) % 10);
	} else {
		WiFi_Buffer[OFFSET + WIFI_POSITION_WIND_DIRECTION] = ' ';
		if((data / 10) % 10){
			WiFi_Buffer[OFFSET + WIFI_POSITION_WIND_DIRECTION + 1] = Int2Char((data / 10) % 10);
		} else {
			WiFi_Buffer[OFFSET + WIFI_POSITION_WIND_DIRECTION + 1] = ' ';
		}
	}
	WiFi_Buffer[OFFSET + WIFI_POSITION_WIND_DIRECTION + 2] = Int2Char(data % 10);

	// Barometric Pressure
	Data_GetCurrentBarometricPressure(&data);
	if((data / 10000) % 10){
		WiFi_Buffer[OFFSET + WIFI_POSITION_BAROMETRIC_PRESSURE] = Int2Char((data / 10000) % 10);
	} else {
		WiFi_Buffer[OFFSET + WIFI_POSITION_BAROMETRIC_PRESSURE] = ' ';
	}
	WiFi_Buffer[OFFSET + WIFI_POSITION_BAROMETRIC_PRESSURE + 1] = Int2Char((data / 1000) % 10);
	WiFi_Buffer[OFFSET + WIFI_POSITION_BAROMETRIC_PRESSURE + 3] = Int2Char((data / 100) % 10);
	WiFi_Buffer[OFFSET + WIFI_POSITION_BAROMETRIC_PRESSURE + 4] = Int2Char((data / 10) % 10);
	WiFi_Buffer[OFFSET + WIFI_POSITION_BAROMETRIC_PRESSURE + 5] = Int2Char(data % 10);

	// Daily Rain
	Data_GetCurrentDailyRain(&data);
	if((data / 100) % 10){
		WiFi_Buffer[OFFSET + WIFI_POSITION_DAILY_RAIN] = Int2Char((data / 100) % 10);
		WiFi_Buffer[OFFSET + WIFI_POSITION_DAILY_RAIN + 1] = Int2Char((data / 10) % 10);
	} else {
		WiFi_Buffer[OFFSET + WIFI_POSITION_DAILY_RAIN] = ' ';
		if((data / 10) % 10){
			WiFi_Buffer[OFFSET + WIFI_POSITION_DAILY_RAIN + 1] = Int2Char((data / 10) % 10);
		} else {
			WiFi_Buffer[OFFSET + WIFI_POSITION_DAILY_RAIN + 1] = ' ';
		}
	}
	WiFi_Buffer[OFFSET + WIFI_POSITION_DAILY_RAIN + 2] = Int2Char(data % 10);

	// Yearly Rain
	Data_GetCurrentYearlyRain(&data);
	if((data / 100) % 10){
		WiFi_Buffer[OFFSET + WIFI_POSITION_YEARLY_RAIN] = Int2Char((data / 100) % 10);
		WiFi_Buffer[OFFSET + WIFI_POSITION_YEARLY_RAIN + 1] = Int2Char((data / 10) % 10);
	} else {
		WiFi_Buffer[OFFSET + WIFI_POSITION_YEARLY_RAIN] = ' ';
		if((data / 10) % 10){
			WiFi_Buffer[OFFSET + WIFI_POSITION_YEARLY_RAIN + 1] = Int2Char((data / 10) % 10);
		} else {
			WiFi_Buffer[OFFSET + WIFI_POSITION_YEARLY_RAIN + 1] = ' ';
		}
	}
	WiFi_Buffer[OFFSET + WIFI_POSITION_YEARLY_RAIN + 2] = Int2Char(data % 10);

	// Battery Voltage
	Data_GetCurrentBatteryVoltage(&data);
	WiFi_Buffer[OFFSET + WIFI_POSITION_BATTERY_VOLTAGE] = Int2Char((data / 100) % 10);
	WiFi_Buffer[OFFSET + WIFI_POSITION_BATTERY_VOLTAGE + 2] = Int2Char((data / 10) % 10);
	WiFi_Buffer[OFFSET + WIFI_POSITION_BATTERY_VOLTAGE + 3] = Int2Char(data % 10);
}