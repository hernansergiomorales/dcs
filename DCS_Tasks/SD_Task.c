/**
 * @file   SD_Task.c
 *
 * @brief  SD card manager task.                           \n
 *         Initialize SD card and write data on log file.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "SD_Task.h"    // Header file

/* Internal Functions Declaration */
void makeBufferSD();  // Make buffer to write to SD

/* Internal Variables */
FIL FileData;  // Log file
static uint8_t sdBuffer[SD_BUFFER_LENGTH] = "DD/MM/YYYY HH:MM:SS: STT.T, STT.T, STT.T, STT.T, STT.T, STT.T, STT.T, STT.T, DD.D, DD.D, DD.D, DD.D, HHH, HHH, HHH, HHH, HHH, HHH, HHH, HHH, WWW, WWW, WWW, WWW, WW.W, WW.W, WW.W, WW.W, WWW, BB.BBB, BB.BBB, BB.BBB, BB.BBB, DDD, YYY, V.VV, P, W, S\r\n";

/**
 * @brief  Check SD card status.                               \n
 *         Mount SD card filesystem, open or create log file
 *         to check for errors and then close it again.
 *
 * @param  None
 *
 * @return Task status: error, OK.
 */
uint8_t SD_Task_Init(){
	if(SD_Begin() == SD_OK){  // SD mounted successfully
		if(SD_FileOpen(&FileData, SD_FILENAME, SD_APPEND_MODE) == SD_OK){  // File opened or created successfully
			SD_FileClose(&FileData);  // Close file
			return LOCAL_OK;  // SD works OK
		}
	}
	return LOCAL_ERROR;  // SD initialization failed
}

/**
 * @brief  Store data on SD card.                               \n
 *         Make buffer with the data to write, open log file,
 *         write buffer on it and close it again.
 *
 * @param  None
 *
 * @return Task status: error (0), OK (1).
 */
uint8_t SD_Task_StoreData(){
	makeBufferSD();
	if(SD_FileOpen(&FileData, SD_FILENAME, SD_APPEND_MODE) == SD_OK){  // Open file in append mode
		uint16_t bytes_written;
		if((SD_FileWrite(&FileData, sdBuffer, SD_BUFFER_LENGTH, &bytes_written) == SD_OK) && (bytes_written == SD_BUFFER_LENGTH)){  // Write sdBuffer to the SD
			#if DEBUG_SD_EN
			UART_SendString(UART_DEBUG, "(SD) File written successfully\r\n");
			#endif  // DEBUG_SD_EN
			SD_FileClose(&FileData);
			return LOCAL_OK;  // Return OK
		} else {
			SD_FileClose(&FileData);
			return LOCAL_ERROR;  // Failed to write data
		}
	}
	return LOCAL_ERROR;  // Failed to write data
}

/**
 * @brief  Store system status on the buffer to write to SD card.            \n
 *         Store on the buffer PTT, SD and WiFi status (they work or not).
 *
 * @param  ledStatus : System status (PTT, SD and WiFi module).
 *
 * @return Nothing
 */
void SD_Task_StoreSystemStatus(led_status_t ledStatus){
	/* Store PTT status */
	if(ledStatus.PTT_LedStatus){
		sdBuffer[SD_POSITION_PTT_STATE] = '0';
	} else {
		sdBuffer[SD_POSITION_PTT_STATE] = '1';
	}
	/* Store SD status */
	if(ledStatus.SD_LedStatus){
		sdBuffer[SD_POSITION_SD_STATE] = '0';
	} else {
		sdBuffer[SD_POSITION_SD_STATE] = '1';
	}
	/* Store WiFi status */
	if(ledStatus.WiFi_LedStatus){
		sdBuffer[SD_POSITION_WIFI_STATE] = '0';
	} else {
		sdBuffer[SD_POSITION_WIFI_STATE] = '1';
	}
}

/**
 * @brief  Make buffer with data to write to SD card.                              \n
 *         Complete buffer with current date and time, current, maximum, minimum
 *         and average values of intern and extern temperature and humidity,
 *         dew point, wind speed, wind chill and barometric pressure, and
 *         wind direction, daily and yearly rain, and battery voltage.
 *
 * @param  None
 *
 * @return Nothing
 */
void makeBufferSD(){
	/* Internal Declarations */
	rtc_t rtc;
	uint16_t data;
	RTC_GetTime(&rtc);  // Get Current Time
	/* Date Format DD/MM/YYYY */
	sdBuffer[SD_POSITION_DAY] = Int2Char((rtc.DD / 10) % 10);
	sdBuffer[SD_POSITION_DAY + 1] = Int2Char(rtc.DD % 10);
	sdBuffer[SD_POSITION_MONTH] = Int2Char((rtc.MM / 10) % 10);
	sdBuffer[SD_POSITION_MONTH + 1] = Int2Char(rtc.MM % 10);
	sdBuffer[SD_POSITION_YEAR] = Int2Char((rtc.YY / 1000) % 10);
	sdBuffer[SD_POSITION_YEAR + 1] = Int2Char((rtc.YY / 100) % 10);
	sdBuffer[SD_POSITION_YEAR + 2] = Int2Char((rtc.YY / 10) % 10);
	sdBuffer[SD_POSITION_YEAR + 3] = Int2Char((rtc.YY % 10) % 10);

	/* Time Format HH:MM:SS */
	sdBuffer[SD_POSITION_HOUR] = Int2Char((rtc.hh / 10) % 10);
	sdBuffer[SD_POSITION_HOUR + 1] = Int2Char(rtc.hh % 10);
	sdBuffer[SD_POSITION_MINUTE] = Int2Char((rtc.mm / 10) % 10);
	sdBuffer[SD_POSITION_MINUTE + 1] = Int2Char(rtc.mm % 10);
	sdBuffer[SD_POSITION_SECOND] = Int2Char((rtc.ss / 10) % 10);
	sdBuffer[SD_POSITION_SECOND + 1] = Int2Char(rtc.ss % 10);

	/* Current Inside Temperature */
	Data_GetCurrentInsideTemperature((int16_t*) &data);
	if((int16_t)data < 0){
		sdBuffer[SD_POSITION_CURRENT_INSIDE_TEMPERATURE] = '-';
	} else {
		sdBuffer[SD_POSITION_CURRENT_INSIDE_TEMPERATURE] = '0';
	}
	sdBuffer[SD_POSITION_CURRENT_INSIDE_TEMPERATURE + 1] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_CURRENT_INSIDE_TEMPERATURE + 2] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_CURRENT_INSIDE_TEMPERATURE + 4] = Int2Char(data % 10);
	
	/* Average Inside Temperature */
	Data_GetAverageInsideTemperature((int16_t*) &data);
	if((int16_t)data < 0){
		sdBuffer[SD_POSITION_AVERAGE_INSIDE_TEMPERATURE] = '-';
	} else {
		sdBuffer[SD_POSITION_AVERAGE_INSIDE_TEMPERATURE] = '0';
	}
	sdBuffer[SD_POSITION_AVERAGE_INSIDE_TEMPERATURE + 1] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_AVERAGE_INSIDE_TEMPERATURE + 2] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_AVERAGE_INSIDE_TEMPERATURE + 4] = Int2Char(data % 10);

	/* Max Inside Temperature */
	Data_GetMaxInsideTemperature((int16_t*) &data);
	if((int16_t)data < 0){
		sdBuffer[SD_POSITION_MAX_INSIDE_TEMPERATURE] = '-';
	} else {
		sdBuffer[SD_POSITION_MAX_INSIDE_TEMPERATURE] = '0';
	}
	sdBuffer[SD_POSITION_MAX_INSIDE_TEMPERATURE + 1] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_MAX_INSIDE_TEMPERATURE + 2] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_MAX_INSIDE_TEMPERATURE + 4] = Int2Char(data % 10);
	
	/* Min Inside Temperature */
	Data_GetMinInsideTemperature((int16_t*) &data);
	if((int16_t)data < 0){
		sdBuffer[SD_POSITION_MIN_INSIDE_TEMPERATURE] = '-';
	} else {
		sdBuffer[SD_POSITION_MIN_INSIDE_TEMPERATURE] = '0';
	}
	sdBuffer[SD_POSITION_MIN_INSIDE_TEMPERATURE + 1] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_MIN_INSIDE_TEMPERATURE + 2] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_MIN_INSIDE_TEMPERATURE + 4] = Int2Char(data % 10);

	/* Current Outside Temperature */
	Data_GetCurrentOutsideTemperature((int16_t*) &data);
	if((int16_t)data < 0){
		sdBuffer[SD_POSITION_CURRENT_OUTSIDE_TEMPERATURE] = '-';
	} else {
		sdBuffer[SD_POSITION_CURRENT_OUTSIDE_TEMPERATURE] = '0';
	}
	sdBuffer[SD_POSITION_CURRENT_OUTSIDE_TEMPERATURE + 1] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_CURRENT_OUTSIDE_TEMPERATURE + 2] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_CURRENT_OUTSIDE_TEMPERATURE + 4] = Int2Char(data % 10);
	
	/* Average Outside Temperature */
	Data_GetAverageOutsideTemperature((int16_t*) &data);
	if((int16_t)data < 0){
		sdBuffer[SD_POSITION_AVERAGE_OUTSIDE_TEMPERATURE] = '-';
	} else {
		sdBuffer[SD_POSITION_AVERAGE_OUTSIDE_TEMPERATURE] = '0';
	}
	sdBuffer[SD_POSITION_AVERAGE_OUTSIDE_TEMPERATURE + 1] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_AVERAGE_OUTSIDE_TEMPERATURE + 2] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_AVERAGE_OUTSIDE_TEMPERATURE + 4] = Int2Char(data % 10);
	
	/* Max Outside Temperature */
	Data_GetMaxOutsideTemperature((int16_t*) &data);
	if((int16_t) data < 0){
		sdBuffer[SD_POSITION_MAX_OUTSIDE_TEMPERATURE] = '-';
	} else {
		sdBuffer[SD_POSITION_MAX_OUTSIDE_TEMPERATURE] = '0';
	}
	sdBuffer[SD_POSITION_MAX_OUTSIDE_TEMPERATURE + 1] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_MAX_OUTSIDE_TEMPERATURE + 2] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_MAX_OUTSIDE_TEMPERATURE + 4] = Int2Char(data % 10);

	/* Min Outside Temperature */
	Data_GetMinOutsideTemperature((int16_t*) &data);
	if((int16_t)data < 0){
		sdBuffer[SD_POSITION_MIN_OUTSIDE_TEMPERATURE] = '-';
	} else {
		sdBuffer[SD_POSITION_MIN_OUTSIDE_TEMPERATURE] = '0';
	}
	sdBuffer[SD_POSITION_MIN_OUTSIDE_TEMPERATURE + 1] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_MIN_OUTSIDE_TEMPERATURE + 2] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_MIN_OUTSIDE_TEMPERATURE + 4] = Int2Char(data % 10);

	/* Current Dew Point */
	Data_GetCurrentDewPoint(&data);
	sdBuffer[SD_POSITION_CURRENT_DEW_POINT] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_CURRENT_DEW_POINT + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_CURRENT_DEW_POINT + 3] = Int2Char(data % 10);
	
	/* Average Dew Point */
	Data_GetAverageDewPoint(&data);
	sdBuffer[SD_POSITION_AVERAGE_DEW_POINT] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_AVERAGE_DEW_POINT + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_AVERAGE_DEW_POINT + 3] = Int2Char(data % 10);
	
	/* Max Dew Point */
	Data_GetMaxDewPoint(&data);
	sdBuffer[SD_POSITION_MAX_DEW_POINT] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_MAX_DEW_POINT + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_MAX_DEW_POINT + 3] = Int2Char(data % 10);
	
	/* Min Dew Point */
	Data_GetMinDewPoint(&data);
	sdBuffer[SD_POSITION_MIN_DEW_POINT] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_MIN_DEW_POINT + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_MIN_DEW_POINT + 3] = Int2Char(data % 10);
	
	/* Current Inside Humidity */
	Data_GetCurrentInsideHumidity(&data);
	sdBuffer[SD_POSITION_CURRENT_INSIDE_HUMIDITY] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_CURRENT_INSIDE_HUMIDITY + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_CURRENT_INSIDE_HUMIDITY + 2] = Int2Char(data % 10);
	
	/* Average Inside Humidity */
	Data_GetAverageInsideHumidity(&data);
	sdBuffer[SD_POSITION_AVERAGE_INSIDE_HUMIDITY] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_AVERAGE_INSIDE_HUMIDITY + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_AVERAGE_INSIDE_HUMIDITY + 2] = Int2Char(data % 10);
	
	/* Max Inside Humidity */
	Data_GetMaxInsideHumidity(&data);
	sdBuffer[SD_POSITION_MAX_INSIDE_HUMIDITY] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_MAX_INSIDE_HUMIDITY + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_MAX_INSIDE_HUMIDITY + 2] = Int2Char(data % 10);

	/* Min Inside Humidity */
	Data_GetMinInsideHumidity(&data);
	sdBuffer[SD_POSITION_MIN_INSIDE_HUMIDITY] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_MIN_INSIDE_HUMIDITY + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_MIN_INSIDE_HUMIDITY + 2] = Int2Char(data % 10);
	
	Data_GetCurrentOutsideHumidity(&data);
	sdBuffer[SD_POSITION_CURRENT_OUTSIDE_HUMIDITY] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_CURRENT_OUTSIDE_HUMIDITY + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_CURRENT_OUTSIDE_HUMIDITY + 2] = Int2Char(data % 10);
	
	/* Average Inside Humidity */
	Data_GetAverageOutsideHumidity(&data);
	sdBuffer[SD_POSITION_AVERAGE_OUTSIDE_HUMIDITY] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_AVERAGE_OUTSIDE_HUMIDITY + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_AVERAGE_OUTSIDE_HUMIDITY + 2] = Int2Char(data % 10);
	
	/* Max Inside Humidity */
	Data_GetMaxOutsideHumidity(&data);
	sdBuffer[SD_POSITION_MAX_OUTSIDE_HUMIDITY] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_MAX_OUTSIDE_HUMIDITY + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_MAX_OUTSIDE_HUMIDITY + 2] = Int2Char(data % 10);
	
	/* Min Inside Humidity */
	Data_GetMinOutsideHumidity(&data);
	sdBuffer[SD_POSITION_MIN_OUTSIDE_HUMIDITY] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_MIN_OUTSIDE_HUMIDITY + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_MIN_OUTSIDE_HUMIDITY + 2] = Int2Char(data % 10);

	/* Current Wind Speed */
	Data_GetCurrentWindSpeed(&data);
	sdBuffer[SD_POSITION_CURRENT_WIND_SPEED] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_CURRENT_WIND_SPEED + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_CURRENT_WIND_SPEED + 2] = Int2Char(data % 10);
	
	/* Average Wind Speed */
	Data_GetAverageWindSpeed(&data);
	sdBuffer[SD_POSITION_AVERAGE_WIND_SPEED] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_AVERAGE_WIND_SPEED + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_AVERAGE_WIND_SPEED + 2] = Int2Char(data % 10);
	
	/* Max Wind Speed */
	Data_GetMaxWindSpeed(&data);
	sdBuffer[SD_POSITION_MAX_WIND_SPEED] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_MAX_WIND_SPEED + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_MAX_WIND_SPEED + 2] = Int2Char(data % 10);

	/* Min Wind Speed */
	Data_GetMinWindSpeed(&data);
	sdBuffer[SD_POSITION_MIN_WIND_SPEED] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_MIN_WIND_SPEED + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_MIN_WIND_SPEED + 2] = Int2Char(data % 10);
	
	/* Current Wind Chill */
	Data_GetCurrentWindChill(&data);
	sdBuffer[SD_POSITION_CURRENT_WIND_CHILL] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_CURRENT_WIND_CHILL + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_CURRENT_WIND_CHILL + 3] = Int2Char(data % 10);
	
	/* Average Wind Chill */
	Data_GetAverageWindChill(&data);
	sdBuffer[SD_POSITION_AVERAGE_WIND_CHILL] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_AVERAGE_WIND_CHILL + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_AVERAGE_WIND_CHILL + 3] = Int2Char(data % 10);
	
	/* Max Wind Chill */
	Data_GetMaxWindChill(&data);
	sdBuffer[SD_POSITION_MAX_WIND_CHILL] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_MAX_WIND_CHILL + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_MAX_WIND_CHILL + 3] = Int2Char(data % 10);

	/* Min Wind Chill */
	Data_GetMinWindChill(&data);
	sdBuffer[SD_POSITION_MIN_WIND_CHILL] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_MIN_WIND_CHILL + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_MIN_WIND_CHILL + 3] = Int2Char(data % 10);
	
	/* Current Wind Direction */
	Data_GetCurrentWindDirection(&data);
	sdBuffer[SD_POSITION_CURRENT_WIND_DIRECTION] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_CURRENT_WIND_DIRECTION + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_CURRENT_WIND_DIRECTION + 2] = Int2Char(data % 10);
	
	/* Current Barometric Pressure */
	Data_GetCurrentBarometricPressure(&data);
	sdBuffer[SD_POSITION_CURRENT_BAROMETRIC_PRESSURE] = Int2Char((data / 10000) % 10);
	sdBuffer[SD_POSITION_CURRENT_BAROMETRIC_PRESSURE + 1] = Int2Char((data / 1000) % 10);
	sdBuffer[SD_POSITION_CURRENT_BAROMETRIC_PRESSURE + 3] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_CURRENT_BAROMETRIC_PRESSURE + 4] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_CURRENT_BAROMETRIC_PRESSURE + 5] = Int2Char(data % 10);
	
	/* Average Barometric Pressure */
	Data_GetAverageBarometricPressure(&data);
	sdBuffer[SD_POSITION_AVERAGE_BAROMETRIC_PRESSURE] = Int2Char((data / 10000) % 10);
	sdBuffer[SD_POSITION_AVERAGE_BAROMETRIC_PRESSURE + 1] = Int2Char((data / 1000) % 10);
	sdBuffer[SD_POSITION_AVERAGE_BAROMETRIC_PRESSURE + 3] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_AVERAGE_BAROMETRIC_PRESSURE + 4] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_AVERAGE_BAROMETRIC_PRESSURE + 5] = Int2Char(data % 10);
	
	/* Max Barometric Pressure */
	Data_GetMaxBarometricPressure(&data);
	sdBuffer[SD_POSITION_MAX_BAROMETRIC_PRESSURE] = Int2Char((data / 10000) % 10);
	sdBuffer[SD_POSITION_MAX_BAROMETRIC_PRESSURE + 1] = Int2Char((data / 1000) % 10);
	sdBuffer[SD_POSITION_MAX_BAROMETRIC_PRESSURE + 3] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_MAX_BAROMETRIC_PRESSURE + 4] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_MAX_BAROMETRIC_PRESSURE + 5] = Int2Char(data % 10);

	/* Min Barometric Pressure */
	Data_GetMinBarometricPressure(&data);
	sdBuffer[SD_POSITION_MIN_BAROMETRIC_PRESSURE] = Int2Char((data / 10000) % 10);
	sdBuffer[SD_POSITION_MIN_BAROMETRIC_PRESSURE + 1] = Int2Char((data / 1000) % 10);
	sdBuffer[SD_POSITION_MIN_BAROMETRIC_PRESSURE + 3] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_MIN_BAROMETRIC_PRESSURE + 4] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_MIN_BAROMETRIC_PRESSURE + 5] = Int2Char(data % 10);

	/* Current Daily Rain */
	Data_GetCurrentDailyRain(&data);
	sdBuffer[SD_POSITION_DAILY_RAIN] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_DAILY_RAIN + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_DAILY_RAIN + 2] = Int2Char(data % 10);

	/* Current Yearly Rain */
	Data_GetCurrentYearlyRain(&data);
	sdBuffer[SD_POSITION_YEARLY_RAIN] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_YEARLY_RAIN + 1] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_YEARLY_RAIN + 2] = Int2Char(data % 10);

	/* Current Battery Voltage */
	Data_GetCurrentBatteryVoltage(&data);
	sdBuffer[SD_POSITION_CURRENT_BATTERY_VOLTAGE] = Int2Char((data / 100) % 10);
	sdBuffer[SD_POSITION_CURRENT_BATTERY_VOLTAGE + 2] = Int2Char((data / 10) % 10);
	sdBuffer[SD_POSITION_CURRENT_BATTERY_VOLTAGE + 3] = Int2Char(data % 10);
}