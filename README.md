<div align="center"><h2>Re-implementaci�n del Sistema de Adquisici�n de Datos Ambientales para el Sistema DCS</h2></div>
<h3>
<b>Proyecto:</b> Pr�ctica Profesional Supervisada<br>
<b>Instituci�n:</b> [Universidad Nacional de La Plata](https://unlp.edu.ar/)<br>
<b>Carrera:</b> [Ingenier�a en Computaci�n](http://ic.info.unlp.edu.ar/)<br>
<b>A�o:</b> 2021<br>
<b>Entidad:</b> [GrIDComD](https://gridcomd.ing.unlp.edu.ar/) (UIDET de la Facultad de Ingenier�a de la UNLP)
</h4>
<h4><i>Hern�n Sergio Morales</i></h4>
<div align="justify">
<p>
El Sistema Satelital de Recolecci�n de Datos (DCS, Data Collection System) es un sistema en el cual se recolectan datos ambientales de plataformas aut�nomas (DCP, Data Collection Platforms), fijas o m�viles, ubicadas en lugares de dif�cil acceso, como monta�as, r�os, selvas o boyas marinas, y son transmitidos hacia uno o varios sat�lites en �rbita. Los receptores DCS, ubicados en los sat�lites, procesan los datos recibidos, los almacenan y, finalmente, los transmiten a las estaciones terrenas argentinas para su posterior procesamiento y distribuci�n. De esta forma, el sistema est� formado por tres segmentos: el segmento espacial (receptor DCS y dem�s componentes a bordo de los sat�lites), las plataformas (adquisici�n de datos ambientales y transmisi�n) y el segmento terrestre (procesamiento de datos y distribuci�n a los usuarios).
</p>
<p>
En la misi�n satelital SAC-D/Aquarius se incluy� en el sat�lite el primer receptor DCS dise�ado e implementado �ntegramente por el GrIDComD; sin embargo, el SAC-D finaliz� su operaci�n a mediados del a�o 2015. Posteriormente, en el a�o 2017, se dise�� y construy� un sistema de adquisici�n de datos ambientales basado en la EDU-CIAA, con el fin de servir de modelo para el desarrollo y validaci�n de componentes del sistema DCS. Debido a problemas con el consumo en el sistema desarrollado, y a errores en el funcionamiento del mismo, se decidi� re-implementar el sistema, bas�ndose en un nuevo microcontrolador y utilizando una arquitectura de programa m�s eficiente.
</p>
</div>
