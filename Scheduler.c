/**
 * @file   Scheduler.c
 *
 * @brief  DCS tasks scheduler.                                   \n
 *         Schedule and dispatch tasks.                           \n
 *         Uses Timer4 for periodic interrupts (system ticks).
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "Scheduler.h"         // Header file
#include "Utilities/System.h"  // Global system settings
#include "DCS.h"               // DCS tasks manager
#include <avr/interrupt.h>     // Driver for AVR interrupts
#include "Utilities/Timer.h"   // Timing functions
#include "Protocols/UART.h"    // Driver for UART communications

/* Internal Functions Declaration */
void timer2_init();

/* Internal Variables */
static volatile uint8_t Flags[SCHEDULER_TASKS_NUMBER];          // Flags that indicate if a task must be dispatched
static volatile int16_t Counters[SCHEDULER_TASKS_NUMBER];       // Counter for every task
static void (*p_TaskFunctions[SCHEDULER_TASKS_NUMBER])(void);   // Pointers to functions for every scheduled task for dispatching
static uint16_t Scheduler_TasksPeriod[SCHEDULER_TASKS_NUMBER];  // Period for every task
static uint16_t Scheduler_TasksOffset[SCHEDULER_TASKS_NUMBER];  // Offset for every task to execute
static uint8_t Scheduler_TasksStarted[SCHEDULER_TASKS_NUMBER];  // Indicates when tasks have delayed their offset time
static uint8_t SchedulerCounter = 0;                            // Counter for interrupts before checking tasks

/**
 * @brief  Initialize scheduler and dispatcher.                               \n
 *         Reset flags and counters, set function pointers and start timer.
 *
 * @param  None
 *
 * @return Nothing
 */
void Scheduler_Init(){
	uint8_t i;
	// Reset period counters
	Scheduler_TasksPeriod[0] = T_DCS_UpdateData_Task;
	Scheduler_TasksPeriod[1] = T_DCS_UpdateBuffers_Task;
	Scheduler_TasksPeriod[2] = T_DCS_PTT_Task;
	Scheduler_TasksPeriod[3] = T_DCS_SD_Task;
	Scheduler_TasksPeriod[4] = T_DCS_WiFi_Task;
	Scheduler_TasksPeriod[5] = T_DCS_ConectionsCheck_Task;
	Scheduler_TasksPeriod[6] = T_DCS_SystemMonitor_Task;
	// Reset offset counters
	Scheduler_TasksOffset[0] = O_DCS_UpdateData_Task;
	Scheduler_TasksOffset[1] = O_DCS_UpdateBuffers_Task;
	Scheduler_TasksOffset[2] = O_DCS_PTT_Task;
	Scheduler_TasksOffset[3] = O_DCS_SD_Task;
	Scheduler_TasksOffset[4] = O_DCS_WiFi_Task;
	Scheduler_TasksOffset[5] = O_DCS_ConectionsCheck_Task;
	Scheduler_TasksOffset[6] = O_DCS_SystemMonitor_Task;
	// Reset flags and task counters.
	Scheduler_DispatchFlag = 0;
	for(i=0; i<SCHEDULER_TASKS_NUMBER; i++){
		Flags[i] = 0;
		Scheduler_TasksStarted[i] = 0;
		Counters[i] = 0;
	}
	// Set function pointers
	p_TaskFunctions[0] = DCS_UpdateData_Task;
	p_TaskFunctions[1] = DCS_UpdateBuffers_Task;
	p_TaskFunctions[2] = DCS_PTT_Task;
	p_TaskFunctions[3] = DCS_SD_Task;
	p_TaskFunctions[4] = DCS_WiFi_Task;
	p_TaskFunctions[5] = DCS_ConectionsCheck_Task;
	p_TaskFunctions[6] = DCS_SystemMonitor_Task;
	// Start timer
	timer2_init();  // Configure Timer4 for interrupts every 100ms
}

/**
 * @brief  Check if any task must execute.                          \n
 *         Set flags and reset counters if any task must execute.
 *
 * @param  None
 *
 * @return Nothing
 */
void Scheduler_Check(){
	uint8_t i;
	for(i=0; i<SCHEDULER_TASKS_NUMBER; i++){
		Counters[i]++;
		if((!Scheduler_TasksStarted[i]) && (Counters[i] >= Scheduler_TasksOffset[i])){
			Scheduler_TasksStarted[i] = 1;
			Counters[i] = Scheduler_TasksPeriod[i];
		}
		if(Scheduler_TasksStarted[i]){
			if(Counters[i] >= Scheduler_TasksPeriod[i]){
				Counters[i] = 0;  // Restart counter
				Flags[i] = 1;  // Task must be dispatched
			}
		}
	}
}

/**
 * @brief  Check if any task has to be dispatched and execute it.
 *
 * @param  None
 *
 * @return Nothing
 */
void Scheduler_Dispatch(){
	uint8_t i;
	for(i=0; i<SCHEDULER_TASKS_NUMBER; i++){
		if(Flags[i]){
			Flags[i] = 0;
			p_TaskFunctions[i]();  // Dispatch task
		}
	}
	Scheduler_DispatchFlag = 0;
}

/**
 * @brief  Initialize Timer2 for interrupting every 20 or 10ms.      \n
 *         System tick depends on F_CPU (8 or 16MHz respectively).
 *
 * @param  None
 *
 * @return Nothing
 */
void timer2_init(){
	TCCR2A |= (1<<WGM21);                         // Mode: CTC
	TCCR2B |= (1<<CS22) | (1<<CS21) | (1<<CS20);  // Prescaler: 1024
	OCR2A = 156;                                  // (8MHz / 1024) / 156 --> 20ms, (16MHz / 1024) / 156 --> 10ms
	TIMSK2 |= (1<<OCIE2A);                        // Enable output compare A match interrupt
}

/* Interrupt service routine for Timer2 channel A compare interrupt */
ISR(TIMER2_COMPA_vect){
	SchedulerCounter++;
	if(SchedulerCounter >= TICK_FRECUENCY){
		Scheduler_Check();
		Scheduler_DispatchFlag = 1;  // New system tick
		SchedulerCounter = 0;
	} else {
		Scheduler_DispatchFlag = 0;
	}
}