var searchData=
[
  ['i2c_5finit_392',['I2C_Init',['../_i2_c_8c.html#a84df9a5887b8b8230cfe85d18e53900e',1,'I2C_Init(void):&#160;I2C.c'],['../_i2_c_8h.html#a84df9a5887b8b8230cfe85d18e53900e',1,'I2C_Init(void):&#160;I2C.c']]],
  ['i2c_5fread_393',['I2C_Read',['../_i2_c_8c.html#a995a488a068b739fc85cbc69367fabbd',1,'I2C_Read(uint8_t isLast):&#160;I2C.c'],['../_i2_c_8h.html#a995a488a068b739fc85cbc69367fabbd',1,'I2C_Read(uint8_t isLast):&#160;I2C.c']]],
  ['i2c_5fstart_394',['I2C_Start',['../_i2_c_8c.html#a32e58d55017fc54118e3b7a5eda44d8c',1,'I2C_Start(void):&#160;I2C.c'],['../_i2_c_8h.html#a32e58d55017fc54118e3b7a5eda44d8c',1,'I2C_Start(void):&#160;I2C.c']]],
  ['i2c_5fstop_395',['I2C_Stop',['../_i2_c_8c.html#a80d6addfa2666f16452c9bdb7a5e0d32',1,'I2C_Stop():&#160;I2C.c'],['../_i2_c_8h.html#a80d6addfa2666f16452c9bdb7a5e0d32',1,'I2C_Stop():&#160;I2C.c']]],
  ['i2c_5fwrite_396',['I2C_Write',['../_i2_c_8c.html#abc46b0fb0330d9f70551ef11b4a33d12',1,'I2C_Write(unsigned char data):&#160;I2C.c'],['../_i2_c_8h.html#abc46b0fb0330d9f70551ef11b4a33d12',1,'I2C_Write(unsigned char data):&#160;I2C.c']]],
  ['initptt_397',['initPTT',['../_d_c_s_8c.html#afd6912a90c9655e2441d041d565396d2',1,'DCS.c']]],
  ['initsd_398',['initSD',['../_d_c_s_8c.html#af25a1f794fa12539ca1c4cba5122952d',1,'DCS.c']]],
  ['initwifi_399',['initWiFi',['../_d_c_s_8c.html#abf4d12a26fa69fa1dd75d6cac9c0f740',1,'DCS.c']]]
];
