var searchData=
[
  ['readresponse_420',['readResponse',['../_e_s_p8266_8c.html#ac7360353a30ef8db928976dc35b8df09',1,'ESP8266.c']]],
  ['rtc_5fgettime_421',['RTC_GetTime',['../_r_t_c_8c.html#a325524393d57526c279ed57432aa523d',1,'RTC_GetTime(rtc_t *time):&#160;RTC.c'],['../_r_t_c_8h.html#a325524393d57526c279ed57432aa523d',1,'RTC_GetTime(rtc_t *time):&#160;RTC.c']]],
  ['rtc_5fmake_422',['RTC_Make',['../_r_t_c_8c.html#a9e84ef4a07935a9623f93d52e72669d3',1,'RTC_Make(uint8_t hh, uint8_t mm, uint8_t ss, uint8_t DD, uint8_t MM, uint16_t YY):&#160;RTC.c'],['../_r_t_c_8h.html#a9e84ef4a07935a9623f93d52e72669d3',1,'RTC_Make(uint8_t hh, uint8_t mm, uint8_t ss, uint8_t DD, uint8_t MM, uint16_t YY):&#160;RTC.c']]],
  ['rtc_5fsettime_423',['RTC_SetTime',['../_r_t_c_8c.html#abc1da8f72ca895cd759fcda76079bd52',1,'RTC_SetTime(rtc_t time):&#160;RTC.c'],['../_r_t_c_8h.html#abc1da8f72ca895cd759fcda76079bd52',1,'RTC_SetTime(rtc_t time):&#160;RTC.c']]]
];
