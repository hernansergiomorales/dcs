var searchData=
[
  ['adc_5fdisableinterrupt_321',['ADC_DisableInterrupt',['../_a_d_c_8c.html#af6d22532375e4d3b95051218539f4103',1,'ADC_DisableInterrupt():&#160;ADC.c'],['../_a_d_c_8h.html#af6d22532375e4d3b95051218539f4103',1,'ADC_DisableInterrupt():&#160;ADC.c']]],
  ['adc_5fenableinterrupt_322',['ADC_EnableInterrupt',['../_a_d_c_8c.html#aab367b43897cc361507458204782a380',1,'ADC_EnableInterrupt():&#160;ADC.c'],['../_a_d_c_8h.html#aab367b43897cc361507458204782a380',1,'ADC_EnableInterrupt():&#160;ADC.c']]],
  ['adc_5fget_323',['ADC_Get',['../_a_d_c_8c.html#a94abb99fb27ee0392b0d6564b5a5d5bb',1,'ADC_Get(uint16_t *p_value):&#160;ADC.c'],['../_a_d_c_8h.html#a94abb99fb27ee0392b0d6564b5a5d5bb',1,'ADC_Get(uint16_t *p_value):&#160;ADC.c']]],
  ['adc_5finit_324',['ADC_Init',['../_a_d_c_8c.html#a3fdaf3b8949082abe85a7e7083087eb7',1,'ADC_Init():&#160;ADC.c'],['../_a_d_c_8h.html#a3fdaf3b8949082abe85a7e7083087eb7',1,'ADC_Init():&#160;ADC.c']]],
  ['adc_5fstartconversion_325',['ADC_StartConversion',['../_a_d_c_8c.html#a0b51d2cac0bf67bd049c0d088c5ca1b4',1,'ADC_StartConversion():&#160;ADC.c'],['../_a_d_c_8h.html#a0b51d2cac0bf67bd049c0d088c5ca1b4',1,'ADC_StartConversion():&#160;ADC.c']]],
  ['adc_5fstopconversion_326',['ADC_StopConversion',['../_a_d_c_8c.html#af7172872001aa763fef17fefee175bc8',1,'ADC_StopConversion():&#160;ADC.c'],['../_a_d_c_8h.html#af7172872001aa763fef17fefee175bc8',1,'ADC_StopConversion():&#160;ADC.c']]]
];
