var searchData=
[
  ['readme_132',['README',['../md__home_hernan__u_n_l_p__p_p_s__d_c_s_dcs__r_e_a_d_m_e.html',1,'']]],
  ['readresponse_133',['readResponse',['../_e_s_p8266_8c.html#ac7360353a30ef8db928976dc35b8df09',1,'ESP8266.c']]],
  ['rtc_134',['rtc',['../structrtc.html',1,'']]],
  ['rtc_2ec_135',['RTC.c',['../_r_t_c_8c.html',1,'']]],
  ['rtc_2eh_136',['RTC.h',['../_r_t_c_8h.html',1,'']]],
  ['rtc_5fgettime_137',['RTC_GetTime',['../_r_t_c_8c.html#a325524393d57526c279ed57432aa523d',1,'RTC_GetTime(rtc_t *time):&#160;RTC.c'],['../_r_t_c_8h.html#a325524393d57526c279ed57432aa523d',1,'RTC_GetTime(rtc_t *time):&#160;RTC.c']]],
  ['rtc_5fmake_138',['RTC_Make',['../_r_t_c_8c.html#a9e84ef4a07935a9623f93d52e72669d3',1,'RTC_Make(uint8_t hh, uint8_t mm, uint8_t ss, uint8_t DD, uint8_t MM, uint16_t YY):&#160;RTC.c'],['../_r_t_c_8h.html#a9e84ef4a07935a9623f93d52e72669d3',1,'RTC_Make(uint8_t hh, uint8_t mm, uint8_t ss, uint8_t DD, uint8_t MM, uint16_t YY):&#160;RTC.c']]],
  ['rtc_5fsettime_139',['RTC_SetTime',['../_r_t_c_8c.html#abc1da8f72ca895cd759fcda76079bd52',1,'RTC_SetTime(rtc_t time):&#160;RTC.c'],['../_r_t_c_8h.html#abc1da8f72ca895cd759fcda76079bd52',1,'RTC_SetTime(rtc_t time):&#160;RTC.c']]]
];
