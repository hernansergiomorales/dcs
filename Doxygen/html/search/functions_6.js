var searchData=
[
  ['main_400',['main',['../main_8c.html#a840291bc02cba5474a4cb46a9b9566fe',1,'main.c']]],
  ['makebuffersd_401',['makeBufferSD',['../_s_d___task_8c.html#a2f164b1a757f9c14db42f09357b2e2e6',1,'SD_Task.c']]],
  ['makebufferwifi_402',['makeBufferWiFi',['../_wi_fi___task_8c.html#a87c471c38ec28fdc0863f24ce908ea38',1,'WiFi_Task.c']]],
  ['mmc_5fclock_5fand_5frelease_403',['mmc_clock_and_release',['../mmc__if_8c.html#a037c696ae5f35d1cd7a1a3ac77f16d9b',1,'mmc_if.c']]],
  ['mmc_5fdatatoken_404',['mmc_datatoken',['../mmc__if_8c.html#a889043d4c369db3d1ca00312d46d45df',1,'mmc_if.c']]],
  ['mmc_5fget_405',['mmc_get',['../mmc__if_8c.html#a2fb09bf63cb5d5a3c28f2931fe66a81c',1,'mmc_if.c']]],
  ['mmc_5finit_406',['mmc_init',['../mmc__if_8c.html#ae5fdd633809bced672d377c8504af7b6',1,'mmc_init(void):&#160;mmc_if.c'],['../mmc__if_8h.html#ad4172df10a56b55ab6b6456b083600f5',1,'mmc_init(void):&#160;mmc_if.c']]],
  ['mmc_5freadsector_407',['mmc_readsector',['../mmc__if_8c.html#ab974288b4f67d4f27de7c3b33d8178fa',1,'mmc_readsector(uint32_t lba, uint8_t *buffer):&#160;mmc_if.c'],['../mmc__if_8h.html#a78c20d6919ea9522f3d80cd4c64c27cb',1,'mmc_readsector(DWORD lba, BYTE *buffer):&#160;mmc_if.h']]],
  ['mmc_5fsend_5fcommand_408',['mmc_send_command',['../mmc__if_8c.html#ab6d1b3c9e8fe5601bc7651b9b5c5429e',1,'mmc_if.c']]],
  ['mmc_5fwritesector_409',['mmc_writesector',['../mmc__if_8c.html#aec1d4ba450fc527f04ffc325e0015f3a',1,'mmc_writesector(uint32_t lba, uint8_t *buffer):&#160;mmc_if.c'],['../mmc__if_8h.html#a52e808250409c7dc3ef931ccb1c38d32',1,'mmc_writesector(DWORD lba, BYTE *buffer):&#160;mmc_if.h']]]
];
