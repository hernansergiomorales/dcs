var indexSectionsWithContent =
{
  0: "_abcdeghilmprstuvw",
  1: "_clrtuw",
  2: "abdeimprstuw",
  3: "abdegimprstuw",
  4: "bhlvw",
  5: "s",
  6: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "defines",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Macros",
  6: "Pages"
};

