/**
 * @file   Utilities.h
 *
 * @brief  Various useful definitions, settings and functions.                          \n
 *         Definitions and initialization of LEDs and buttons.                          \n
 *         Integer to char converser.                                                   \n
 *         Various useful definitions (true/false, high/low, on/off, OK/error, etc.).
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _UTILITIES_H_
#define _UTILITIES_H_

/* Inclusions */
#include <avr/io.h>  // Driver for AVR input/output
#include "Ports.h"   // Driver for ports handling

/* Definition of LEDs Port and Pin */
#define LED_RED_GPIO    Ports_A
#define LED_RED_DDR     Ports_A_DDR
#define LED_RED_PIN     0
#define LED_GREEN_GPIO  Ports_A
#define LED_GREEN_DDR   Ports_A_DDR
#define LED_GREEN_PIN   1
#define LED_BLUE_GPIO   Ports_A
#define LED_BLUE_DDR    Ports_A_DDR
#define LED_BLUE_PIN    2
#define LED_PTT_GPIO    Ports_A
#define LED_PTT_DDR     Ports_A_DDR
#define LED_PTT_PIN     3
#define LED_SD_GPIO     Ports_A
#define LED_SD_DDR      Ports_A_DDR
#define LED_SD_PIN      4
#define LED_WIFI_GPIO   Ports_A
#define LED_WIFI_DDR    Ports_A_DDR
#define LED_WIFI_PIN    5

/* Definition of Buttons Port and Pin */
#define BUTTON_SD_GPIO  Ports_L
#define BUTTON_SD_DDR   Ports_L_DDR
#define BUTTON_SD_IN    Ports_L_IN
#define BUTTON_SD_PIN   0

/* LED Numbers */
#define LED_RED    0
#define LED_GREEN  1
#define LED_BLUE   2
#define LED_PTT    3
#define LED_SD     4
#define LED_WIFI   5

/* Button Numbers */
#define BUTTON_SD  0

/* Various Useful Definitions */
// True/False Values
#define LOCAL_TRUE       1
#define LOCAL_FALSE      0
// High/Low Values
#define LOCAL_HIGH       1
#define LOCAL_LOW        0
// On/Off Values
#define LOCAL_ON         1
#define LOCAL_OFF        0
// Enable/Disable Values
#define LOCAL_ENABLE     1
#define LOCAL_DISABLE    0
// Running/Stopped Values
#define LOCAL_RUNNING    1
#define LOCAL_STOPPED    0
// Pressed/Unpressed Values
#define LOCAL_UNPRESSED  1
#define LOCAL_PRESSED    0
// Return Codes
#define LOCAL_ERROR      1
#define LOCAL_OK         0

/* Integer to Char Conversion */
#define Int2Char(c) ((c) + 0x30)

/**
 * @brief  Initialization of buttons.                       \n
 *         Configure SD button GPIO as input with pull-up.
 *
 * @param  None
 *
 * @return Nothing
 */
static inline void InitButtons(){
	Ports_ModeInputPullUp(BUTTON_SD_GPIO, BUTTON_SD_DDR, BUTTON_SD_PIN);
}

/**
 * @brief  Initialization of LEDs.                                                                 \n
 *         Configure all LEDs GPIO (red, green, blue, PTT, SD and WiFi) as output and clear them.
 *
 * @param  None
 *
 * @return Nothing
 */
static inline void InitLEDs(){
	// Config LEDs GPIO as output
	Ports_ModeOutput(LED_RED_GPIO, LED_RED_DDR, LED_RED_PIN);
	Ports_ModeOutput(LED_GREEN_GPIO, LED_GREEN_DDR, LED_GREEN_PIN);
	Ports_ModeOutput(LED_BLUE_GPIO, LED_BLUE_DDR, LED_BLUE_PIN);
	Ports_ModeOutput(LED_PTT_GPIO, LED_PTT_DDR, LED_PTT_PIN);
	Ports_ModeOutput(LED_SD_GPIO, LED_SD_DDR, LED_SD_PIN);
	Ports_ModeOutput(LED_WIFI_GPIO, LED_WIFI_DDR, LED_WIFI_PIN);

	// Clear Value
	Ports_SetValue(LED_RED_GPIO, LED_RED_PIN, LOCAL_OFF);
	Ports_SetValue(LED_GREEN_GPIO, LED_GREEN_PIN, LOCAL_OFF);
	Ports_SetValue(LED_BLUE_GPIO, LED_BLUE_PIN, LOCAL_OFF);
	Ports_SetValue(LED_PTT_GPIO, LED_PTT_PIN, LOCAL_OFF);
	Ports_SetValue(LED_SD_GPIO, LED_SD_PIN, LOCAL_OFF);
	Ports_SetValue(LED_WIFI_GPIO, LED_WIFI_PIN, LOCAL_OFF);
}

#endif  // _UTILITIES_H_ 