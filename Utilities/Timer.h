/**
 * @file   Timer.h
 *
 * @brief  Timer.c header.    \n
 *         Timing functions.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _TIMER_H_
#define _TIMER_H_

/* Inclusions */
#include <avr/io.h>  // Driver for AVR input/output

/* Functions Declaration */
// Main Timer
void Timer_Start();
uint32_t Timer_GetTime();
uint32_t Timer_Stop();
// Secondary Timer
void Timer2_Start();
uint32_t Timer2_GetTime();
uint32_t Timer2_Stop();

#endif  // _TIMER_H_