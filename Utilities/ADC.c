/**
 * @file   ADC.c
 *
 * @brief  Driver for analogical-digital converser.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "ADC.h"            // Header file
#include <avr/interrupt.h>  // Driver for AVR input/output
#include "Utilities.h"      // Various useful definitions, settings and functions

/* Internal Variables */
static uint8_t InterruptEnabled = 0;        // Conversion complete interrupt enabled/disabled
static uint8_t ConversionCompleteFlag = 0;  // Conversion complete flag
static uint16_t ConversionValue = 0;        // Last conversion value

/* Internal Functions Declaration */
void getValue();

/**
 * @brief  Initialize ADC module.
 *
 * @param  None
 *
 * @return Nothing
 *
 * @note   This function must be called before using any other functionality.
 */
void ADC_Init(){
	ADMUX |= (ADC_VREF_SELECT << REFS0) | (ADC_ADLAR_SELECT << ADLAR) | ((ADC_CHN_SELECT & 0x1F) << MUX0);
	ADCSRB |= ((ADC_CHN_SELECT >> 5) << MUX5) | (ADC_AUTO_TRIGGER << ADTS0);
	ADCSRA |= (1 << ADEN) | (ADC_PRESCALER << ADPS0);
	DIDR0 |= ((ADC_DigitalInputPin & 0xFF) << ADC0D);
	DIDR2 |= ((ADC_DigitalInputPin >> 8) << ADC8D);
}

/**
 * @brief  Enable interrupt of conversion completed.
 *
 * @param  None
 *
 * @return Nothing
 */
void ADC_EnableInterrupt(){
	ADCSRA |= (1 << ADIE);
	InterruptEnabled = 1;
}

/**
 * @brief  Disable interrupt of conversion completed.
 *
 * @param  None
 *
 * @return Nothing
 */
void ADC_DisableInterrupt(){
	ADCSRA &= ~(1 << ADIE);
	InterruptEnabled = 0;
}

/**
 * @brief  Start one or more conversions.
 *
 * @param  None
 *
 * @return Conversion result, if ADC interrupts are not used.
 */
uint16_t ADC_StartConversion(){
	ADCSRA |= (1 << ADSC);					 // Start conversion
	if(!InterruptEnabled){					 // Polling
		while((ADCSRA & (1 << ADIF)) == 0);	 // Wait for conversion to finish
		ADCSRA |= (1<<ADIF);				 // Clear flag
		getValue();							 // Get conversion value
		return ConversionValue;
	}
	return LOCAL_OK;  // Non useful return
}

/**
 * @brief  Stop conversions.
 *
 * @param  None
 *
 * @return Nothing
 */
void ADC_StopConversion(){
	ADCSRA &= ~(1 << ADSC);
}

/**
 * @brief  Get result value of last conversion.
 *
 * @param  p_value : Point to where return last conversion result.
 *
 * @return Conversion result is valid (OK) or it is not available yet (error).
 */
uint8_t ADC_Get(uint16_t *p_value){
	if(ConversionCompleteFlag){
		*p_value = ConversionValue;
		ConversionCompleteFlag = 0;	 // Wait for new conversion
		return LOCAL_OK;
	} else {
		return LOCAL_ERROR;  // Non conversion complete
	}
}

/**
 * @brief  Get conversion result.
 *
 * @param  None
 *
 * @return Nothing
 */
void getValue(){
	ConversionValue = ADCL;
	ConversionValue += ((uint16_t) ADCH) << 8;
}

/* Interrupt service routine for ADC conversion completed interrupt */
ISR(ADC_vect){
	getValue();
	ConversionCompleteFlag = 1;
}