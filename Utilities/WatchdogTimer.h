/**
 * @file   WatchdogTimer.h
 *
 * @brief  WatchdogTimer.c header.                            \n
 *         Driver for watchdog timer.                         \n
 *         Definitions of timeout values and watchdog modes.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _WDTIMER_H_
#define _WDTIMER_H_

/* Setting Values */
// Watchdog Timer Timeout Values
#define WDTimer_Timeout_16ms   ((0 << WDP3) | (0 << WDP2) | (0 << WDP1) | (0 << WDP0))
#define WDTimer_Timeout_32ms   ((0 << WDP3) | (0 << WDP2) | (0 << WDP1) | (1 << WDP0))
#define WDTimer_Timeout_64ms   ((0 << WDP3) | (0 << WDP2) | (1 << WDP1) | (0 << WDP0))
#define WDTimer_Timeout_150ms  ((0 << WDP3) | (0 << WDP2) | (1 << WDP1) | (1 << WDP0))
#define WDTimer_Timeout_250ms  ((0 << WDP3) | (1 << WDP2) | (0 << WDP1) | (0 << WDP0))
#define WDTimer_Timeout_500ms  ((0 << WDP3) | (1 << WDP2) | (0 << WDP1) | (1 << WDP0))
#define WDTimer_Timeout_1s     ((0 << WDP3) | (1 << WDP2) | (1 << WDP1) | (0 << WDP0))
#define WDTimer_Timeout_2s     ((0 << WDP3) | (1 << WDP2) | (1 << WDP1) | (1 << WDP0))
#define WDTimer_Timeout_4s     ((1 << WDP3) | (0 << WDP2) | (0 << WDP1) | (0 << WDP0))
#define WDTimer_Timeout_8s     ((1 << WDP3) | (0 << WDP2) | (0 << WDP1) | (1 << WDP0))
// Watchdog Timer Modes
#define WDTimer_Mode_Stopped                  ((0 << WDE) | (0 << WDIE))
#define WDTimer_Mode_Interrupt                ((0 << WDE) | (1 << WDIE))
#define WDTimer_Mode_SystemReset              ((1 << WDE) | (0 << WDIE))
#define WDTimer_Mode_InterruptAndSystemReset  ((1 << WDE) | (1 << WDIE))
// Watchdog Timer Change Enabled
#define WDTimer_CHEnable  (1 << WDCE)

/* Functions Declaration */
void WatchdogTimer_Start();
void WatchdogTimer_Stop();
void WatchdogTimer_Reset();

#endif  // _WDTIMER_H_