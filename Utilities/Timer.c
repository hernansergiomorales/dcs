/**
 * @file   Timer.c
 *
 * @brief  Timing functions.                                          \n
 *         It can start timer, then get elapsed time or stop timer.   \n
 *         Two timers are available.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "Timer.h"          // Header file
#include "System.h"         // Global system settings
#include <avr/interrupt.h>  // Driver for AVR interrupts
#include <util/atomic.h>    // Atomically execution

/* Intern Variables */
uint32_t TickVal_us = 1024000000UL / F_CPU;  // Tick value in �s used to count time elapsed

/**
 * @brief  Start main timer.
 *
 * @param  None
 *
 * @return Nothing
 */
void Timer_Start(){
	TCNT1 = 0;
	TCCR1B |= (1 << CS10) | (1 << CS12);  // Prescaler: 1024
}

/**
 * @brief  Get time elapsed from main timer.
 *
 * @param  None
 *
 * @return Time elapsed in �s.
 */
uint32_t Timer_GetTime(){
	uint32_t counter = TCNT1*TickVal_us;
	return counter;  // Return time elapsed in �s
}

/**
 * @brief  Stop main timer.
 *
 * @param  None
 *
 * @return Time elapsed in �s.
 */
uint32_t Timer_Stop(){
	uint32_t counter = TCNT1*TickVal_us;
	TCCR1B &= ~((1 << CS10) | (1 << CS12));  // Disable timer
	return counter;  // Return time elapsed in �s
}

/**
 * @brief  Start secondary timer.
 *
 * @param  None
 *
 * @return Nothing
 */
void Timer2_Start(){
	TCNT3 = 0;
	TCCR3B |= (1 << CS30) | (1 << CS32);  // Prescaler: 1024
}

/**
 * @brief  Get time elapsed from secondary timer.
 *
 * @param  None
 *
 * @return Time elapsed in �s.
 */
uint32_t Timer2_GetTime(){
	uint32_t counter = TCNT3*TickVal_us;
	return counter;  // Return time elapsed in �s
}

/**
 * @brief  Stop secondary timer.
 *
 * @param  None
 *
 * @return Time elapsed in �s.
 */
uint32_t Timer2_Stop(){
	uint32_t counter = TCNT3*TickVal_us;
	TCCR3B &= ~((1 << CS30) | (1 << CS32));  // Disable timer
	return counter;  // Return time elapsed in �s
}