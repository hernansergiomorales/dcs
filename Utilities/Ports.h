/**
 * @file   Ports.h
 *
 * @brief  Driver for ports handling.                                                       \n
 *         Operations with ports are implemented as macros.                                 \n
 *         Ports can be configured as input, input with pull-up or output.                  \n
 *         Ports can be cleared (0), set (1), toggled, read or set with a specific value.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _PORTS_H_
#define _PORTS_H_

/* Ports Definitions */
// Ports Data Register
#define Ports_A  PORTA
#define Ports_B  PORTB
#define Ports_C  PORTC
#define Ports_D  PORTD
#define Ports_E  PORTE
#define Ports_F  PORTF
#define Ports_G  PORTG
#define Ports_H  PORTH
#define Ports_J  PORTJ
#define Ports_K  PORTK
#define Ports_L  PORTL
// Ports Data Direction
#define Ports_A_DDR  DDRA
#define Ports_B_DDR  DDRB
#define Ports_C_DDR  DDRC
#define Ports_D_DDR  DDRD
#define Ports_E_DDR  DDRE
#define Ports_F_DDR  DDRF
#define Ports_G_DDR  DDRG
#define Ports_H_DDR  DDRH
#define Ports_J_DDR  DDRJ
#define Ports_K_DDR  DDRK
#define Ports_L_DDR  DDRL
// Ports Input Pin
#define Ports_A_IN  PINA
#define Ports_B_IN  PINB
#define Ports_C_IN  PINC
#define Ports_D_IN  PIND
#define Ports_E_IN  PINE
#define Ports_F_IN  PINF
#define Ports_G_IN  PING
#define Ports_H_IN  PINH
#define Ports_J_IN  PINJ
#define Ports_K_IN  PINK
#define Ports_L_IN  PINL

/* Ports Mode Setting */
#define Ports_ModeInput(port, ddr, pin)        ddr &= ~(1<<pin); port &= ~(1<<pin)
#define Ports_ModeInputPullUp(port, ddr, pin)  ddr &= ~(1<<pin); port |= (1<<pin)
#define Ports_ModeOutput(port, ddr, pin)       ddr |= (1<<pin); port &= ~(1<<pin)

/* Ports Operations */
#define Ports_Set(port, pin)              port |= (1<<pin)
#define Ports_Clear(port, pin)            port &= ~(1<<pin)
#define Ports_Toggle(port, pin)           port ^= (1<<pin)
#define Ports_Read(inP, pin)              (inP & (1<<pin))
#define Ports_SetValue(port, pin, value)  if(value) Ports_Set(port, pin); else Ports_Clear(port, pin)

#endif  // _PORTS_H_