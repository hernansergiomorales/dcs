/**
 * @file   System.h
 *
 * @brief  Global system settings: devices, debugging, CPU clock frequency, system execution and UARTs.       \n
 *         Weather monitor, PTT, SD, WiFi, RTC and button for extracting SD can be enabled or disabled.       \n
 *         General debugging and weather monitor, PTT, SD and WiFi debugging can be enabled or disabled.      \n
 *         System execution can be a simulation, or program can runs on Arduino Mega 2560 or ATmega2560.      \n
 *         CPU clock frequency can be set to 16MHz (Arduino) or 8MHz (ATmega2560).                            \n
 *         UARTs used for debugging and to connect to weather monitor, PTT and ESP8266 can be interchanged.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _SYSTEM_H_
#define _SYSTEM_H_

/* Global system settings  */
// Devices settings
#define WEATHER_MONITOR_EN  1           // Enable weather monitor or simulate it
#define PTT_EN              1           // Enable PTT or ignore it
#define SD_EN               1           // Enable SD or ignore it
#define WIFI_EN             1           // Enable WiFi or ignore it
#define RTC_EN              1           // Enable RTC or ignore it
#define SD_BUTTON_EN        1 && SD_EN  // Enable button for extracting SD card (SD must be enabled)
// Debug settings
#define DEBUG_EN       1                         // Enable general debugging
#define DEBUG_WM_EN    1 && DEBUG_EN             // Enable weather monitor debugging (general debugging must be enabled)
#define DEBUG_PTT_EN   1 && DEBUG_EN && PTT_EN   // Enable PTT debugging (PTT and general debugging must be enabled)
#define DEBUG_SD_EN    1 && DEBUG_EN && SD_EN    // Enable SD debugging (SD and general debugging must be enabled)
#define DEBUG_WIFI_EN  1 && DEBUG_EN && WIFI_EN  // Enable WiFi debugging (WiFi and general debugging must be enabled)
// Execution settings
#define SIMULATION  0  // The system is simulated (running on Proteus) or not (running on a real microcontroller)
#define ARDUINO     1  // The system runs on Arduino Mega 2560 or on ATmega2560 microcontroller
// CPU clock frequency setting
#if ARDUINO
#define F_CPU  16000000UL  // Arduino clock frequency is 16MHz
#else
#define F_CPU  8000000UL   // ATmega2560 clock frequency is 8MHz
#endif  // ARDUINO
// UART settings
#define UART_DEBUG  0  // UART0 is used for debugging
#define UART_RS232  1  // UART1 is used to connect to Davis Monitor weather station
#define UART_PTT    2  // UART2 is used to connect to PTT
#define UART_WIFI   3  // UART3 is used to connect to ESP8266 WiFi module

#endif  // _SYSTEM_H_