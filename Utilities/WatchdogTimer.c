/**
 * @file   WatchdogTimer.c
 *
 * @brief  Driver for watchdog timer.                           \n
 *         Watchdog timer can be started, stopped or reseted.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "WatchdogTimer.h"  // Header file
#include "System.h"         // Global system settings
#include <avr/wdt.h>        // Driver for AVR watchdog timer
#include <avr/interrupt.h>  // Driver or AVR interrupts

/* Watchdog Timer Timeout Selection */
#define WDTimer_Timeout  WDTimer_Timeout_8s

/**
 * @brief  Start watchdog timer.                              \n
 *         Reset watchdog timer, initialize it and start it.
 *
 * @param  None
 *
 * @return Nothing
 */
void WatchdogTimer_Start(){
	cli();
	wdt_reset();
	WDTCSR = WDTimer_CHEnable | WDTimer_Mode_SystemReset;             // Restart timer
	WDTCSR = WDTimer_Mode_InterruptAndSystemReset | WDTimer_Timeout;  // Timeout setting, Mode: Interrupt and System Reset Mode
	sei();
}

/**
 * @brief  Stop watchdog timer.
 *
 * @param  None
 *
 * @return Nothing
 */
void WatchdogTimer_Stop(){
	cli();
	WDTCSR = WDTimer_Mode_Stopped;
	sei();
}

/**
 * @brief  Reset watchdog timer.
 *
 * @param  None
 *
 * @return Nothing
 */
void WatchdogTimer_Reset(){
	cli();
	wdt_reset();
	sei();
}

/* Interrupt service routine for watchdog timer interrupt */
ISR(WDT_vect){}
