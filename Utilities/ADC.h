/**
 * @file   ADC.h
 *
 * @brief  ADC.c header
 *         Driver for analogical-digital converser.                                                 \n
 *         Definitions of voltage reference, alignment, prescaler and auto trigger source values.   \n
 *         ADC configuration values for initializing.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _ADC_H_
#define _ADC_H_

/* Inclusions */
#include <avr/io.h>  // Driver for AVR input/output

/* Setting Values */
// ADC Voltage Reference
#define ADC_VREF_AREF  0
#define ADC_VREF_AVCC  1
#define ADC_VREF_1V1   2
#define ADC_VREF_2V56  3
// ADC Alignment
#define ADC_ADLAR_Right  0
#define ADC_ADLAR_Left   1
// ADC Prescaler
#define ADC_Psc_2    0
#define ADC_Psc_4    2
#define ADC_Psc_8    3
#define ADC_Psc_16   4
#define ADC_Psc_32   5
#define ADC_Psc_64   6
#define ADC_Psc_128  7
// ADC Auto Trigger Source
#define ADC_FreeRunning  0
#define ADC_AnalogComp   1
#define ADC_ExtIntReq0   2
#define ADC_Timer0CompA  3
#define ADC_Timer0OVF    4
#define ADC_Timer1CompB  5
#define ADC_Timer1OVF    6
#define ADC_Timer1Capt   7

/* Settings */
#define ADC_VREF_SELECT      ADC_VREF_AREF    // Select ADC voltage reference
#define ADC_ADLAR_SELECT     ADC_ADLAR_Right  // Select ADC alignment
#define ADC_CHN_SELECT       0                // Select ADC channel
#define ADC_AUTO_TRIGGER     0                // Enable/disable ADC auto trigger
#define ADC_PRESCALER        ADC_Psc_8        // Select ADC prescaler
#define ADC_ADATE_MODE       ADC_FreeRunning  // Select ADC auto trigger source
#define ADC_DigitalInputPin  1                // Select digital pin to disable

/* Functions Declaration */
void ADC_Init();                     // Initialize ADC
void ADC_EnableInterrupt();          // Enable conversion complete interrupt
void ADC_DisableInterrupt();         // Disable conversion complete interrupt
uint16_t ADC_StartConversion();      // Start one or various conversions
void ADC_StopConversion();           // Stop conversions
uint8_t ADC_Get(uint16_t *p_value);  // Get value from last conversion

#endif  // _ADC_H_