/**
 * @file   main.c
 *
 * @brief  This file contains main program, the entry point.                               \n
 *         The main program handles system initialization, tasks scheduler/dispatcher
 *         and watchdog timer execution, and puts MCU in low power mode when it is idle.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "Utilities/System.h"         // Global system settings
#include <avr/io.h>                   // Driver for AVR input/output
#include <avr/interrupt.h>            // Driver for AVR interrupts
#include <util/delay.h>               // Delay functions
#include <avr/sleep.h>                // Driver for sleep modes
#include "DCS.h"                      // DCS tasks manager
#include "Scheduler.h"                // DCS tasks scheduler
#include "Utilities/WatchdogTimer.h"  // Driver for watchdog timer
#include "Utilities/Timer.h"          // Timing functions
#include "Protocols/UART.h"           // Driver for UART communications

/* Definitions */
#define START_SYSTEM_DELAY_MS		5000  // Time in ms to wait before system starts
#define START_SCHEDULER_DELAY_MS	1000  // Time in ms to wait before scheduler starts after system has started

/* Variables */
extern volatile uint8_t Scheduler_DispatchFlag;  // Flag that indicates if scheduler dispatch must be executed

/** @brief Main program.                                                             \n
 *         Initialize DCS tasks and tasks scheduler, start watchdog timer,
 *         execute tasks dispatcher and put MCU in low power mode when it is idle.
 *  @return It should not return.
 */
int main(void){
	#if DEBUG_EN
	UART_Init(UART_DEBUG);  // Init UART for debugging
	UART_SendString(UART_DEBUG, "\r\n  --------------------------------\r\n");
	UART_SendString(UART_DEBUG, "  <<<< DCS DEBUGGING TERMINAL >>>>\r\n");
	UART_SendString(UART_DEBUG, "  --------------------------------\r\n\r\n");
	#endif  // DEBUG_EN
	
	_delay_ms(START_SYSTEM_DELAY_MS);     // Configurable start system delay
	DCS_Init_Task();                      // Init DCS tasks
	_delay_ms(START_SCHEDULER_DELAY_MS);  // Configurable start scheduler delay
	Scheduler_Init();                     // Start scheduler
	WatchdogTimer_Start();                // Start watchdog timer
	sei();                                // Enable global interrupts
	while(1){
		if(Scheduler_DispatchFlag){
			WatchdogTimer_Reset();  // Watchdog timer must be reseted after 125ms occurred
			Scheduler_Dispatch();   // Scheduler dispatch must execute each 100ms
		}
		// Sleep Mode
		#if (!SIMULATION && !DEBUG_EN)
		set_sleep_mode(SLEEP_MODE_PWR_SAVE);  // Config Power Save sleep mode
		sleep_mode();                         // Put MCU in sleep mode
		#endif  // !SIMULATION && !DEBUG_EN
	}
}
