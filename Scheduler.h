/**
 * @file   Scheduler.h
 *
 * @brief  Scheduler.c header.                                               \n
 *         DCS tasks scheduler.                                              \n
 *         Definition of tasks period and offset.                            \n
 *         Different periods values if system is simulated or not.           \n
 *         Public flag variable to indicate dispatcher should be executed.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _SCHEDULER_H_
#define _SCHEDULER_H_

/* Inclusions */
#include "DCS.h"     // DCS tasks manager
#include <avr/io.h>  // Driver for AVR input/output

/* Definitions */
#define SCHEDULER_TASKS_NUMBER      7                        // Number of tasks for scheduling and dispatching
#define TICK_FRECUENCY              (5 * F_CPU / 8000000UL)  // Ticks per second
// Tasks Period (in 100ms)
#define T_DCS_UpdateData_Task       TICK_FRECUENCY                       // 1 second
#define T_DCS_UpdateBuffers_Task    (OBSERVATION_TIME * TICK_FRECUENCY)  // 4 hours
#define T_DCS_PTT_Task              TICK_FRECUENCY                       // 1 second
#define T_DCS_SD_Task               (SAMPLE_TIME * TICK_FRECUENCY)       // 2 minutes
#define T_DCS_WiFi_Task             TICK_FRECUENCY                       // 1 second
#define T_DCS_ConectionsCheck_Task  TICK_FRECUENCY                       // 1 second
#define T_DCS_SystemMonitor_Task    TICK_FRECUENCY                       // 1 second
// Tasks Offset
#if SIMULATION
#define O_DCS_UpdateData_Task       25   // 2.5 seconds
#define O_DCS_UpdateBuffers_Task    100  // 10 seconds
#define O_DCS_PTT_Task              127  // 12.7 seconds
#define O_DCS_SD_Task               150  // 150 seconds
#define O_DCS_WiFi_Task             179  // 17.9 seconds
#define O_DCS_ConectionsCheck_Task  13   // 1.3 seconds
#define O_DCS_SystemMonitor_Task    1    // 0.1 seconds
#else
#define O_DCS_UpdateData_Task       25   // 2.5 seconds
#define O_DCS_UpdateBuffers_Task    300  // 30 seconds
#define O_DCS_PTT_Task              407  // 40.7 seconds
#define O_DCS_SD_Task               500  // 50 seconds
#define O_DCS_WiFi_Task             609  // 60.9 seconds
#define O_DCS_ConectionsCheck_Task  13   // 1.3 seconds
#define O_DCS_SystemMonitor_Task    1    // 0.1 seconds
#endif  // SIMULATION

/* Public Variables */
volatile uint8_t Scheduler_DispatchFlag;

/* Functions Declaration */
void Scheduler_Init();
void Scheduler_Check();
void Scheduler_Dispatch();

#endif  // _SCHEDULER_H_