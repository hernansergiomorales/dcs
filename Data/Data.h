/**
 * @file   Data.h
 *
 * @brief  Data.c header.                    \n
 *         Weather variables data storage.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _DATA_H_
#define _DATA_H_

/* Inclusions */
#include <avr/io.h>

/* Constants for Data Storage */
// Power Supply in mV
#define DATA_POWER_SUPPLY         7000
// Weather Macros
#define DATA_WEATHER_VARIABLES    12
#define DATA_WEATHER_PARAMETERS   5   // Current, maximum, minimum and average (2 bytes) values
// Weather Data
#define DATA_INSIDE_TEMPERATURE   0   // INT Value
#define DATA_OUTSIDE_TEMPERATURE  1   // INT Value
#define DATA_DEW_POINT            2
#define DATA_INSIDE_HUMIDITY      3
#define DATA_OUTSIDE_HUMIDITY     4
#define DATA_WIND_SPEED           5
#define DATA_WIND_CHILL           6
#define DATA_WIND_DIRECTION       7   // Only current data
#define DATA_BAROMETRIC_PRESSURE  8
#define DATA_DAILY_RAIN           9   // Only current data
#define DATA_YEARLY_RAIN          10  // Only current data
#define DATA_BATTERY_VOLTAGE      11  // Only current data
// Weather Parameters
#define DATA_CURRENT_VALUE        0
#define DATA_MAX_VALUE            1
#define DATA_MIN_VALUE            2
#define DATA_AVG_VALUE            3
// Return Codes
#define DATA_ERROR                0x00
#define DATA_NO_ERROR             0x01
#define DATA_PENDING_RESPONSE     0x02
#define DATA_TASK_COMPLETE        0xFF

/* Functions Declaration */
// Data
void Data_Init();  // Initialize data structures
uint8_t Data_Update();
void Data_RestartStatiscalData();  // Restart maximum, minimum and average statistics
// Inside Temperature
void Data_GetCurrentInsideTemperature(int16_t *data);
void Data_GetAverageInsideTemperature(int16_t *avg);
void Data_GetMaxInsideTemperature(int16_t *max);
void Data_GetMinInsideTemperature(int16_t *min);
// Outside Temperature
void Data_GetCurrentOutsideTemperature(int16_t *data);
void Data_GetAverageOutsideTemperature(int16_t *avg);
void Data_GetMaxOutsideTemperature(int16_t *max);
void Data_GetMinOutsideTemperature(int16_t *min);
// Dew Point
void Data_GetCurrentDewPoint(uint16_t *data);
void Data_GetAverageDewPoint(uint16_t *avg);
void Data_GetMaxDewPoint(uint16_t *max);
void Data_GetMinDewPoint(uint16_t *min);
// Inside Humidity
void Data_GetCurrentInsideHumidity(uint16_t *data);
void Data_GetAverageInsideHumidity(uint16_t *avg);
void Data_GetMaxInsideHumidity(uint16_t *max);
void Data_GetMinInsideHumidity(uint16_t *min);
// Outside Humidity
void Data_GetCurrentOutsideHumidity(uint16_t *data);
void Data_GetAverageOutsideHumidity(uint16_t *avg);
void Data_GetMaxOutsideHumidity(uint16_t *max);
void Data_GetMinOutsideHumidity(uint16_t *min);
// Wind Speed
void Data_GetCurrentWindSpeed(uint16_t *data);
void Data_GetAverageWindSpeed(uint16_t *avg);
void Data_GetMaxWindSpeed(uint16_t *max);
void Data_GetMinWindSpeed(uint16_t *min);
// Wind Chill
void Data_GetCurrentWindChill(uint16_t *data);
void Data_GetAverageWindChill(uint16_t *avg);
void Data_GetMaxWindChill(uint16_t *max);
void Data_GetMinWindChill(uint16_t *min);
// Wind Direction*/
void Data_GetCurrentWindDirection(uint16_t *data);
// Barometric Pressure
void Data_GetCurrentBarometricPressure(uint16_t *data);
void Data_GetAverageBarometricPressure(uint16_t *avg);
void Data_GetMaxBarometricPressure(uint16_t *max);
void Data_GetMinBarometricPressure(uint16_t *min);
// Daily Rain*/
void Data_GetCurrentDailyRain(uint16_t *data);
// Yearly Rain*/
void Data_GetCurrentYearlyRain(uint16_t *data);
// Battery Voltage
void Data_GetCurrentBatteryVoltage(uint16_t *data);
// Cardinal Point
char* Data_CardinalPoint(uint16_t point);

#endif  // _DATA_H_