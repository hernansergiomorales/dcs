/**
 * @file   Buffer.c
 *
 * @brief  Manage buffers to send to PTT.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "../Utilities/System.h"  // Global system settings
#include "Buffer.h"               // Header file
#include "Data.h"                 // Weather variables data
#include "../Devices/RTC.h"       // Driver for real time clock
#include "../Protocols/UART.h"    // Driver for UART

/* Internal Variables */
static uint8_t Buffer[BUFFER_NUMBER][BUFFER_LENGTH];  // Data structure that represents buffers to be sent
static uint8_t BufferNumber;                          // Current buffer number

/**
 * @brief  Initialize buffers
 *
 * @param  None
 *
 * @return Nothing
 */
void Buffer_Init(){
	uint8_t i, j;
	for(i=0; i<BUFFER_NUMBER; i++){
		for(j=0; j<BUFFER_LENGTH; j++){
			Buffer[i][j] = 0;
		}
		// Special values
		Buffer[i][BUFFER_NOB] = 0xFF;
		Buffer[i][BUFFER_LT] = 0x7F;
		Buffer[i][BUFFER_LT + 1] = 0xFF;
		Buffer[i][BUFFER_LH] = 0x7F;
	}

	BufferNumber = 0;
}

/**
 * @brief  Update buffers with current and updated weather variables.
 *
 * @param  None
 *
 * @return Nothing
 */
void Buffer_Update(){ 
	// Internal variables
	uint16_t data;
	uint8_t i, nob=0;
	
	// Set NOB of all buffers
	for(i=0; i<3; i++){
		Buffer[BufferNumber][BUFFER_NOB] = nob;
		BufferNumber = (BufferNumber + 2) % 3;
		nob++;
	}
	
	// Update Outside Temperature
	Data_GetCurrentOutsideTemperature((int16_t*) &data);
	Buffer[BufferNumber][BUFFER_OT] = (uint8_t)(data >> 8);
	Buffer[BufferNumber][BUFFER_OT + 1] = (uint8_t)data;
	// Update High Outside Temperature
	Data_GetMaxOutsideTemperature((int16_t*) &data);
	Buffer[BufferNumber][BUFFER_HT] = (uint8_t)(data >> 8);
	Buffer[BufferNumber][BUFFER_HT + 1] = (uint8_t)data;
	// Update Low Outside Temperature
	Data_GetMinOutsideTemperature((int16_t*) &data);
	Buffer[BufferNumber][BUFFER_LT] = (uint8_t)(data >> 8);
	Buffer[BufferNumber][BUFFER_LT + 1] = (uint8_t)data;
	// Update Average Outside Temperature
	Data_GetAverageOutsideTemperature((int16_t*) &data);
	Buffer[BufferNumber][BUFFER_AVGT] = (uint8_t)(data >> 8);
	Buffer[BufferNumber][BUFFER_AVGT + 1] = (uint8_t)data;
	
	// Update Dew Point
	Data_GetCurrentDewPoint(&data);
	Buffer[BufferNumber][BUFFER_DP] = (uint8_t)(data >> 8);
	Buffer[BufferNumber][BUFFER_DP + 1] = (uint8_t)(data);
	
	// Update Outside Humidity
	Data_GetCurrentOutsideHumidity(&data);
	Buffer[BufferNumber][BUFFER_OH] = (uint8_t)(data);
	// Update High Outside Humidity
	Data_GetMaxOutsideHumidity(&data);
	Buffer[BufferNumber][BUFFER_HH] = (uint8_t)data;
	// Update Low Outside Humidity
	Data_GetMinOutsideHumidity(&data);
	Buffer[BufferNumber][BUFFER_LH] = (uint8_t)data;
	
	// Update Wind Speed
	Data_GetCurrentWindSpeed(&data);
	Buffer[BufferNumber][BUFFER_WS] = (uint8_t)(data);
	// Update High Wind Speed
	Data_GetMaxWindSpeed(&data);
	Buffer[BufferNumber][BUFFER_HWS] = (uint8_t)data;
	// Update Average Wind Speed
	Data_GetAverageWindSpeed(&data);
	Buffer[BufferNumber][BUFFER_AVGWS] = (uint8_t)data;
	
	// Update Wind Chill
	Data_GetCurrentWindChill(&data);
	Buffer[BufferNumber][BUFFER_WC] = (uint8_t)(data >> 8);
	Buffer[BufferNumber][BUFFER_WC + 1] = (uint8_t)(data);
	
	// Update Wind Direction
	Data_GetCurrentWindDirection(&data);
	Buffer[BufferNumber][BUFFER_WD] = (uint8_t)(data >> 8);
	Buffer[BufferNumber][BUFFER_WD + 1] = (uint8_t)(data);
	
	// Update Daily Rain
	Data_GetCurrentDailyRain(&data);
	Buffer[BufferNumber][BUFFER_DR] = (uint8_t)(data >> 8);
	Buffer[BufferNumber][BUFFER_DR + 1] = (uint8_t)(data);
	
	// Update Yearly Rain
	Data_GetCurrentYearlyRain(&data);
	Buffer[BufferNumber][BUFFER_YR] = (uint8_t)(data >> 8);
	Buffer[BufferNumber][BUFFER_YR + 1] = (uint8_t)(data);
	
	// Update Barometric Pressure
	Data_GetCurrentBarometricPressure(&data);
	Buffer[BufferNumber][BUFFER_BP] = (uint8_t)(data >> 8);
	Buffer[BufferNumber][BUFFER_BP + 1] = (uint8_t)(data);
	
	// Update Battery Voltage
	Data_GetCurrentBatteryVoltage(&data);
	Buffer[BufferNumber][BUFFER_BV] = (uint8_t)(data >> 8);
	Buffer[BufferNumber][BUFFER_BV + 1] = (uint8_t)(data);
	
	// Set CRC
	Buffer[BufferNumber][BUFFER_CRC] = (uint8_t)(Buffer[BufferNumber][BUFFER_OT]     + Buffer[BufferNumber][BUFFER_OT + 1]   + Buffer[BufferNumber][BUFFER_HT]     + 
	                                             Buffer[BufferNumber][BUFFER_HT + 1] + Buffer[BufferNumber][BUFFER_LT]       + Buffer[BufferNumber][BUFFER_LT + 1] +
	                                             Buffer[BufferNumber][BUFFER_AVGT]   + Buffer[BufferNumber][BUFFER_AVGT + 1] + Buffer[BufferNumber][BUFFER_BP]     +
	                                             Buffer[BufferNumber][BUFFER_BP + 1] + Buffer[BufferNumber][BUFFER_OH]       + Buffer[BufferNumber][BUFFER_HH]     +
	                                             Buffer[BufferNumber][BUFFER_LH]     + Buffer[BufferNumber][BUFFER_WS]       + Buffer[BufferNumber][BUFFER_HWS]    + 
	                                             Buffer[BufferNumber][BUFFER_AVGWS]  + Buffer[BufferNumber][BUFFER_WD]       + Buffer[BufferNumber][BUFFER_WD + 1] +
	                                             Buffer[BufferNumber][BUFFER_YR]     + Buffer[BufferNumber][BUFFER_YR + 1]   + Buffer[BufferNumber][BUFFER_DR]     +
	                                             Buffer[BufferNumber][BUFFER_DR + 1] + Buffer[BufferNumber][BUFFER_WC]       + Buffer[BufferNumber][BUFFER_WC + 1] +
	                                             Buffer[BufferNumber][BUFFER_DP]     + Buffer[BufferNumber][BUFFER_DP + 1]   + Buffer[BufferNumber][BUFFER_BV]     +
	                                             Buffer[BufferNumber][BUFFER_BV + 1]);
	
	nob++;  // Update NOB
	BufferNumber = (BufferNumber + 1) % BUFFER_NUMBER;  // Update Buffer and it's variables
	Data_RestartStatiscalData();  // Restart statical data: max, min and average
}

/**
 * @brief   Get a specific buffer
 *
 * @param   buffer_number : Buffer to get
 * @param   buff          : Pointer to where to put data  
 *
 * @return  Nothing
 */
void Buffer_Get(uint8_t buffer_number, uint8_t buff[BUFFER_LENGTH]){
	uint8_t i;
	for(i=0; i<BUFFER_LENGTH; i++){
		buff[i] = Buffer[buffer_number][i];
	}
}