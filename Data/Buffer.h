/**
 * @file   Buffer.h
 *
 * @brief  Buffer.c header.                \n
 *         Manage buffers to send to PTT.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _BUFFER_H_
#define _BUFFER_H_

/* Inclusions */
#include <avr/io.h>  // Driver for AVR input/output

/* Definitions */
#define BUFFER_NUMBER  3	// Number of available buffers
#define BUFFER_LENGTH  32
// Buffer Positions
#define BUFFER_CRC    0   // Last byte of two's complement sum (1 byte)
#define BUFFER_NOB    1   // Number of Observation (1 byte)
#define BUFFER_IDUA   2   // Interval from last sample (2 bytes)
#define BUFFER_OT     4   // Outside Temperature (2 bytes)
#define BUFFER_AVGT   6   // Average Outside Temperature (2 bytes)
#define BUFFER_HT     8   // High Outside Temperature (2 bytes)
#define BUFFER_LT     10  // Low Outside Temperature (2 bytes)
#define BUFFER_DP     12  // Dew Point (2 bytes)
#define BUFFER_OH     14  // Outside Humidity (1 byte)
#define BUFFER_HH     15  // High Outside Humidity (1 byte)
#define BUFFER_LH     16  // Low Outside Humidity (1 byte)
#define BUFFER_WS     17  // Wind Speed (1 byte)
#define BUFFER_AVGWS  18  // Average Wind Speed (1 byte)
#define BUFFER_HWS    19  // High Wind Speed (1 byte)
#define BUFFER_WC     20  // Wind Chill (2 bytes)
#define BUFFER_WD     22  // Wind Direction (2 bytes)
#define BUFFER_BP     24  // Barometric Pressure (2 bytes)
#define BUFFER_DR     26  // Yearly Rain (2 bytes)
#define BUFFER_YR     28  // Daily Rain (2 bytes)
#define BUFFER_BV     30  // Battery Voltage (2 bytes)

/* Functions Declaration */
void Buffer_Init();
void Buffer_Update();
void Buffer_Get(uint8_t bufferNumber, uint8_t buff[BUFFER_LENGTH]);

#endif  // _BUFFER_H_