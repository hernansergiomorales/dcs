/**
 * @file   Data.c
 *
 * @brief  Weather variables data storage.                      \n
 *         Data can be gotten or updated, and statistics data
 *         (maximum, minimum and average data) can be reset.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "Data.h"                       // Header file
#include "../Devices/WeatherStation.h"  // Driver for Davis Monitor weather station
#include "../Utilities/ADC.h"           // Driver for analogical-digital converser

/* Internal Variables */
static uint16_t WeatherData[DATA_WEATHER_VARIABLES][DATA_WEATHER_PARAMETERS];  // Data structure
static uint32_t Samples;  // Number of samples of weather variables, to calculate average values
enum subTaskState_Data_t {InTemp, OutTemp, DewPoint, InHum, OutHum, WindSpeed, WindChill, WindDir, BarPress, DailyRain, YearlyRain, BatVolt} SubTaskState_Data;  // Steps to get data from Weather Monitor

/* Internal Functions Declaration */
void updateInsideTemperature(uint16_t data);
void updateOutsideTemperature(uint16_t data);
void updateDewPoint(uint16_t data);
void updateInsideHumidity(uint16_t data);
void updateOutsideHumidity(uint16_t data);
void updateWindSpeed(uint16_t data);
void updateWindDirection(uint16_t data);
void updateWindChill(uint16_t data);
void updateBarometricPressure(uint16_t data);
void updateDailyRain(uint16_t data);
void updateYearlyRain(uint16_t data);
void updateBatteryVoltage(uint16_t data);

/**
 * @brief   Translates a number representing a cardinal point to a legible cardinal point.
 *
 * @param   point : Number representing cardinal point.
 *
 * @return  String with legible cardinal point.
 */
char* Data_CardinalPoint(uint16_t point){
    if ((point >= 0) && (point < 23))
        return "N";
    else if ((point >= 23) && (point < 46))
        return "NNE";
    else if ((point >= 46) && (point < 69))
        return "NE";
    else if ((point >= 69) && (point < 92))
        return "ENE";
    else if ((point >= 92) && (point < 115))
        return "E";
    else if ((point >= 115) && (point < 138))
        return "ESE";
    else if ((point >= 138) && (point < 161))
        return "SE";
    else if ((point >= 161) && (point < 184))
        return "SSE";
    else if ((point >= 184) && (point < 207))
        return "S";
    else if ((point >= 207) && (point < 230))
        return "SSW";
    else if ((point >= 230) && (point < 252))
        return "SW";
    else if ((point >= 252) && (point < 276))
        return "WSW";
    else if ((point >= 276) && (point < 299))
        return "W";
    else if ((point >= 299) && (point < 322))
        return "WNW";
    else if ((point >= 322) && (point < 345))
        return "NW";
    else if ((point >= 345) && (point < 361))
        return "NNW";
    else
        return "ERROR";
}

/**
 * @brief  Initialize internal data structure.
 *
 * @param  None
 *
 * @return Nothing
 */
void Data_Init(){
	// Internal variables
	uint8_t i, j;
	for(i=0; i<DATA_WEATHER_VARIABLES; i++){
		for(j=0; j<DATA_WEATHER_PARAMETERS; j++){
			if(j == DATA_MIN_VALUE)
				WeatherData[i][j] = 0x7FFF;
			else
				WeatherData[i][j] = 0;   
		}
	}
	Samples = 0;
	SubTaskState_Data = InTemp;  // First step is getting inside temperature
}

/**
 * @brief  Updates internal data structure.                                                \n
 *         The task is set up to work cooperatively, the task is divided into
 *         subtasks and on each function call one of the parts is executed.                \n
 *         Every weather variable is read in a different subtask.                          \n
 *         It also takes advantage of the subtasks of obtaining data in WeatherStation.c.
 *
 * @param  None
 *
 * @return Nothing
 */
uint8_t Data_Update(){
	// Internal variables
    uint16_t data;
	uint8_t result;
	
	#if WEATHER_MONITOR_EN
	switch(SubTaskState_Data){
		case InTemp:  // Inside Temperature
			Samples++;  // One more sample
			result = WeatherStation_GetInsideTemperature((int16_t*) &data);
			if(result != WS_NO_ERROR){
				if(result == WS_PENDING_RESPONSE)
					return DATA_PENDING_RESPONSE;
				#if DEBUG_WM_EN
				UART_SendString(UART_DEBUG, "Failed to get inside temperature\r\n");
				#endif  // DEBUG_WM_EN
				return DATA_ERROR;
			}		
			updateInsideTemperature(data);
			SubTaskState_Data = OutTemp;  // Next step is getting outside temperature
			break;

		case OutTemp:  // Outside Temperature
			result = WeatherStation_GetOutsideTemperature((int16_t*) &data);
			if(result != WS_NO_ERROR){
				if(result == WS_PENDING_RESPONSE)
					return DATA_PENDING_RESPONSE;
				#if DEBUG_WM_EN
				UART_SendString(UART_DEBUG, "Failed to get outside temperature\r\n");
				#endif  // DEBUG_WM_EN
				return DATA_ERROR;
			}
			updateOutsideTemperature(data);
			SubTaskState_Data = DewPoint;  // Next step is getting dew point
			break;

		case DewPoint:  // Dew Point
			result = WeatherStation_GetDewPoint(&data);
			if(result != WS_NO_ERROR){
				if(result == WS_PENDING_RESPONSE)
					return DATA_PENDING_RESPONSE;
				#if DEBUG_WM_EN
				UART_SendString(UART_DEBUG, "Failed to get dew point\r\n");
				#endif  // DEBUG_WM_EN
				return DATA_ERROR;
			}
			updateDewPoint(data);
			SubTaskState_Data = InHum;  // Next step is getting inside humidity
			break;

		case InHum:  // Inside Humidity
			result = WeatherStation_GetInsideHumidity(&data);
			if(result != WS_NO_ERROR){
				if(result == WS_PENDING_RESPONSE)
					return DATA_PENDING_RESPONSE;
				#if DEBUG_WM_EN
				UART_SendString(UART_DEBUG, "Failed to get inside humidity\r\n");
				#endif  // DEBUG_WM_EN
				return DATA_ERROR;
			}
			updateInsideHumidity(data);
			SubTaskState_Data = OutHum;  // Next step is getting outside humidity
			break;

		case OutHum:  // Outside Humidity
			result = WeatherStation_GetOutsideHumidity(&data);
			if(result != WS_NO_ERROR){
				if(result == WS_PENDING_RESPONSE)
					return DATA_PENDING_RESPONSE;
				#if DEBUG_WM_EN
				UART_SendString(UART_DEBUG, "Failed to get outside humidity\r\n");
				#endif  // DEBUG_WM_EN
				return DATA_ERROR;
			}
			updateOutsideHumidity(data);
			SubTaskState_Data = WindSpeed;  // Next step is getting wind speed
			break;

		case WindSpeed:  // Wind Speed
			result = WeatherStation_GetWindSpeed(&data);
			if(result != WS_NO_ERROR){
				if(result == WS_PENDING_RESPONSE)
					return DATA_PENDING_RESPONSE;
				#if DEBUG_WM_EN
				UART_SendString(UART_DEBUG, "Failed to get wind speed\r\n");
				#endif  // DEBUG_WM_EN
				return DATA_ERROR;
			}
			updateWindSpeed(data);
			SubTaskState_Data = WindChill;  // Next step is getting wind chill
			break;
			
		case WindChill:  // Wind Chill		
			result = WeatherStation_GetWindChill(&data);
			if(result != WS_NO_ERROR){
				if(result == WS_PENDING_RESPONSE)
					return DATA_PENDING_RESPONSE;
				#if DEBUG_WM_EN
				UART_SendString(UART_DEBUG, "Failed to get wind chill\r\n");
				#endif  // DEBUG_WM_EN
				return DATA_ERROR;
			}
			updateWindChill(data);
			SubTaskState_Data = WindDir;  // Next step is getting wind direction
			break;

		case WindDir:  // Wind Direction
			result = WeatherStation_GetWindDirection(&data);
			if(result != WS_NO_ERROR){
				if(result == WS_PENDING_RESPONSE)
					return DATA_PENDING_RESPONSE;
				#if DEBUG_WM_EN
				UART_SendString(UART_DEBUG, "Failed to get wind direction\r\n");
				#endif  // DEBUG_WM_EN
				return DATA_ERROR;
			}
			updateWindDirection(data);
			SubTaskState_Data = BarPress;  // Next step is getting barometric pressure
			break;

		case BarPress:  // Barometric Pressure
			result = WeatherStation_GetBarometricPressure(&data);
			if(result != WS_NO_ERROR){
				if(result == WS_PENDING_RESPONSE)
					return DATA_PENDING_RESPONSE;
				#if DEBUG_WM_EN
				UART_SendString(UART_DEBUG, "Failed to get barometric pressure\r\n");
				#endif  // DEBUG_WM_EN
				return DATA_ERROR;
			}
			updateBarometricPressure(data);
			SubTaskState_Data = DailyRain;  // Next step is getting daily rain
			break;

		case DailyRain:  // Daily Rain
			result = WeatherStation_GetDailyRain(&data);
			if(result != WS_NO_ERROR){
				if(result == WS_PENDING_RESPONSE)
					return DATA_PENDING_RESPONSE;
				#if DEBUG_WM_EN
				UART_SendString(UART_DEBUG, "Failed to get daily rain\r\n");
				#endif  // DEBUG_WM_EN
				return DATA_ERROR;
			}
			updateDailyRain(data);
			SubTaskState_Data = YearlyRain;  // Next step is getting yearly rain
			break;

		case YearlyRain:  // Yearly Rain
			result = WeatherStation_GetYearlyRain(&data);
			if(result != WS_NO_ERROR){
				if(result == WS_PENDING_RESPONSE)
					return DATA_PENDING_RESPONSE;
				#if DEBUG_WM_EN
				UART_SendString(UART_DEBUG, "Failed to get yearly rain\r\n");
				#endif  // DEBUG_WM_EN
				return DATA_ERROR;
			}
			updateYearlyRain(data);
			SubTaskState_Data = BatVolt;  // Next step is getting battery voltage
			break;

		case BatVolt:  // Battery Voltage
			data = (ADC_StartConversion() * (DATA_POWER_SUPPLY / 10UL)) / 1023;
			updateBatteryVoltage(data);
			SubTaskState_Data = InTemp;  // First step is getting inside temperature
			return DATA_TASK_COMPLETE;
			break;
			
		default:
			return DATA_ERROR;
			break;
	}
	#else
	// Weather monitor is not available, so it must be simulated
	Samples++;  // One more sample
	data = WeatherData[DATA_INSIDE_TEMPERATURE][DATA_CURRENT_VALUE] + 1;
	updateInsideTemperature(data);
	data = WeatherData[DATA_OUTSIDE_TEMPERATURE][DATA_CURRENT_VALUE] + 1;
	updateOutsideTemperature(data);
	data = WeatherData[DATA_DEW_POINT][DATA_CURRENT_VALUE] + 1;
	updateDewPoint(data);
	data = WeatherData[DATA_INSIDE_HUMIDITY][DATA_CURRENT_VALUE] + 1;
	updateInsideHumidity(data);
	data = WeatherData[DATA_OUTSIDE_HUMIDITY][DATA_CURRENT_VALUE] + 1;
	updateOutsideHumidity(data);
	data = WeatherData[DATA_WIND_SPEED][DATA_CURRENT_VALUE] + 1;
	updateWindSpeed(data);
	data = WeatherData[DATA_WIND_CHILL][DATA_CURRENT_VALUE] + 1;
	updateWindChill(data);
	data = WeatherData[DATA_WIND_DIRECTION][DATA_CURRENT_VALUE] + 1;
	updateWindDirection(data);
	data = WeatherData[DATA_BAROMETRIC_PRESSURE][DATA_CURRENT_VALUE] + 1;
	updateBarometricPressure(data);
	data = WeatherData[DATA_DAILY_RAIN][DATA_CURRENT_VALUE] + 1;
	updateDailyRain(data);
	data = WeatherData[DATA_YEARLY_RAIN][DATA_CURRENT_VALUE] + 1;
	updateYearlyRain(data);
	data = ADC_StartConversion() * 330UL / 1023;
	updateBatteryVoltage(data);
	return DATA_TASK_COMPLETE;
	#endif  // WEATHER_MONITOR_EN
	
	return DATA_NO_ERROR;
}

/**
 * @brief  Current inside temperature getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetCurrentInsideTemperature(int16_t *data){
	*data = (int16_t)WeatherData[DATA_INSIDE_TEMPERATURE][DATA_CURRENT_VALUE];
}

/**
 * @brief  Average inside temperature getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetAverageInsideTemperature(int16_t *avg){
	// To avoid division by 0
	if(Samples){
		*avg = (int16_t)((((uint32_t)WeatherData[DATA_INSIDE_TEMPERATURE][DATA_AVG_VALUE] << 16) + WeatherData[DATA_INSIDE_TEMPERATURE][DATA_AVG_VALUE + 1]) / Samples);
	} else {
		*avg = 0;
	}
}

/**
 * @brief  Maximum inside temperature getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetMaxInsideTemperature(int16_t *max){
	*max = (int16_t) WeatherData[DATA_INSIDE_TEMPERATURE][DATA_MAX_VALUE];
}

/**
 * @brief  Minimum inside temperature getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetMinInsideTemperature(int16_t *min){
	*min = (int16_t) WeatherData[DATA_INSIDE_TEMPERATURE][DATA_MIN_VALUE];
}

/**
 * @brief  Current outside temperature getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetCurrentOutsideTemperature(int16_t *data){
	*data = (int16_t)WeatherData[DATA_OUTSIDE_TEMPERATURE][DATA_CURRENT_VALUE];
}

/**
 * @brief  Average outside temperature getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetAverageOutsideTemperature(int16_t *avg){
	// To avoid division by 0
	if(Samples)
		*avg = (int16_t)((((uint32_t)WeatherData[DATA_OUTSIDE_TEMPERATURE][DATA_AVG_VALUE] << 16) + WeatherData[DATA_OUTSIDE_TEMPERATURE][DATA_AVG_VALUE + 1]) / Samples);
	else
		*avg = 0;
}

/**
 * @brief  Maximum outside temperature getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetMaxOutsideTemperature(int16_t *max){
	*max = (int16_t) WeatherData[DATA_OUTSIDE_TEMPERATURE][DATA_MAX_VALUE];
}

/**
 * @brief  Minimum outside temperature getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetMinOutsideTemperature(int16_t *min){
	*min = (int16_t) WeatherData[DATA_OUTSIDE_TEMPERATURE][DATA_MIN_VALUE];
}

/**
 * @brief  Current dew point getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetCurrentDewPoint(uint16_t *data){
	*data = WeatherData[DATA_DEW_POINT][DATA_CURRENT_VALUE];
}

/**
 * @brief  Average dew point getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetAverageDewPoint(uint16_t *avg){
	// To avoid division by 0
	if(Samples)	
		*avg = (uint16_t)((((uint32_t)WeatherData[DATA_DEW_POINT][DATA_AVG_VALUE] << 16) + WeatherData[DATA_DEW_POINT][DATA_AVG_VALUE + 1]) / Samples);
	else
		*avg = 0;
}

/**
 * @brief  Maximum dew point getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetMaxDewPoint(uint16_t *max){
	*max = WeatherData[DATA_DEW_POINT][DATA_MAX_VALUE];
}

/**
 * @brief  Minimum dew point getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetMinDewPoint(uint16_t *min){
	*min = WeatherData[DATA_DEW_POINT][DATA_MIN_VALUE];
}

/**
 * @brief  Current inside humidity getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetCurrentInsideHumidity(uint16_t *data){
	*data = WeatherData[DATA_INSIDE_HUMIDITY][DATA_CURRENT_VALUE];
}

/**
 * @brief  Average inside humidity getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetAverageInsideHumidity(uint16_t *avg){
	// To avoid division by 0
	if(Samples)
		*avg = (uint16_t)((((uint32_t)WeatherData[DATA_INSIDE_HUMIDITY][DATA_AVG_VALUE] << 16) + WeatherData[DATA_INSIDE_HUMIDITY][DATA_AVG_VALUE + 1]) / Samples);
	else
		*avg = 0;
}

/**
 * @brief  Maximum inside humidity getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetMaxInsideHumidity(uint16_t *max){
	*max = WeatherData[DATA_INSIDE_HUMIDITY][DATA_MAX_VALUE];
}

/**
 * @brief  Minimum inside humidity getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetMinInsideHumidity(uint16_t *min){
	*min = WeatherData[DATA_INSIDE_HUMIDITY][DATA_MIN_VALUE];
}

/**
 * @brief  Current outside humidity getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetCurrentOutsideHumidity(uint16_t *data){
	*data = WeatherData[DATA_OUTSIDE_HUMIDITY][DATA_CURRENT_VALUE];
}

/**
 * @brief  Average outside humidity getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetAverageOutsideHumidity(uint16_t *avg){
	// To avoid division by 0
	if(Samples)	
		*avg = (uint16_t)((((uint32_t)WeatherData[DATA_OUTSIDE_HUMIDITY][DATA_AVG_VALUE] << 16) + WeatherData[DATA_OUTSIDE_HUMIDITY][DATA_AVG_VALUE + 1]) / Samples);
	else
		*avg = 0;
}

/**
 * @brief  Maximum outside humidity getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetMaxOutsideHumidity(uint16_t *max){
	*max = WeatherData[DATA_OUTSIDE_HUMIDITY][DATA_MAX_VALUE];
}

/**
 * @brief  Minimum outside humidity getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetMinOutsideHumidity(uint16_t *min){
	*min = WeatherData[DATA_OUTSIDE_HUMIDITY][DATA_MIN_VALUE];
}

/**
 * @brief  Current wind speed getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetCurrentWindSpeed(uint16_t *data){
	*data = WeatherData[DATA_WIND_SPEED][DATA_CURRENT_VALUE];
}

/**
 * @brief  Average wind speed getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetAverageWindSpeed(uint16_t *avg){
	// To avoid division by 0
	if(Samples)
		*avg = (uint16_t)((((uint32_t)WeatherData[DATA_WIND_SPEED][DATA_AVG_VALUE] << 16) + WeatherData[DATA_WIND_SPEED][DATA_AVG_VALUE + 1]) / Samples);
	else
		*avg = 0;
}

/**
 * @brief  Maximum wind speed getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetMaxWindSpeed(uint16_t *max){
	*max = WeatherData[DATA_WIND_SPEED][DATA_MAX_VALUE];
}

/**
 * @brief  Minimum wind speed getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetMinWindSpeed(uint16_t *min){
	*min = WeatherData[DATA_WIND_SPEED][DATA_MIN_VALUE];
}

/**
 * @brief  Current wind chill getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetCurrentWindChill(uint16_t *data){
	*data = WeatherData[DATA_WIND_CHILL][DATA_CURRENT_VALUE];
}

/**
 * @brief  Average wind chill getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetAverageWindChill(uint16_t *avg){
	// To avoid division by 0
	if(Samples)
		*avg = (uint16_t)((((uint32_t)WeatherData[DATA_WIND_CHILL][DATA_AVG_VALUE] << 16) + WeatherData[DATA_WIND_CHILL][DATA_AVG_VALUE + 1]) / Samples);
	else
		*avg = 0;
}

/**
 * @brief  Maximum wind chill getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetMaxWindChill(uint16_t *max){
	*max = WeatherData[DATA_WIND_CHILL][DATA_MAX_VALUE];
}

/**
 * @brief  Minimum wind chill getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetMinWindChill(uint16_t *min){
	*min = WeatherData[DATA_WIND_CHILL][DATA_MIN_VALUE];
}

/**
 * @brief  Current wind direction getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetCurrentWindDirection(uint16_t *data){
	*data = WeatherData[DATA_WIND_DIRECTION][DATA_CURRENT_VALUE];
}

/**
 * @brief  Current barometric pressure getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetCurrentBarometricPressure(uint16_t *data){
	*data = WeatherData[DATA_BAROMETRIC_PRESSURE][DATA_CURRENT_VALUE];
}

/**
 * @brief  Average barometric pressure getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetAverageBarometricPressure(uint16_t *avg){
	// To avoid division by 0
	if(Samples)
		*avg = (uint16_t)((((uint32_t)WeatherData[DATA_BAROMETRIC_PRESSURE][DATA_AVG_VALUE] << 16) + WeatherData[DATA_BAROMETRIC_PRESSURE][DATA_AVG_VALUE + 1]) / Samples);
	else
		*avg = 0;
}

/**
 * @brief  Maximum barometric pressure getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetMaxBarometricPressure(uint16_t *max){
	*max = WeatherData[DATA_BAROMETRIC_PRESSURE][DATA_MAX_VALUE];
}

/**
 * @brief  Minimum barometric pressure getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetMinBarometricPressure(uint16_t *min){
	*min = WeatherData[DATA_BAROMETRIC_PRESSURE][DATA_MIN_VALUE];
}

/**
 * @brief  Current daily rain getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetCurrentDailyRain(uint16_t *data){
	*data = WeatherData[DATA_DAILY_RAIN][DATA_CURRENT_VALUE];
}

/**
 * @brief  Current yearly rain getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetCurrentYearlyRain(uint16_t *data){
	*data = WeatherData[DATA_YEARLY_RAIN][DATA_CURRENT_VALUE];
}

/**
 * @brief  Current battery voltage getter.
 *
 * @param  data : Pointer to where to store data
 *
 * @return Nothing
 */
void Data_GetCurrentBatteryVoltage(uint16_t *data){
	*data = WeatherData[DATA_BATTERY_VOLTAGE][DATA_CURRENT_VALUE];
}

/**
 * @brief  Update inside temperature data.                          \n
 *         Update current, maximum, minimum and average values.
 *
 * @param  data : Current value
 *
 * @return Nothing
 */
void updateInsideTemperature(uint16_t data){
	uint32_t aux;
	
	#if DEBUG_WM_EN
	double inTemp_F = ((int16_t) data) / 10.0;  // Inside temperature in �F
	double inTemp_C = (inTemp_F - 32) / 1.8;  // Inside temperature in �C
	UART_SendString(UART_DEBUG, "(Data) Inside temperature: ");
	UART_SendDouble(UART_DEBUG, inTemp_F, 0, 1);
	UART_SendString(UART_DEBUG, "F / ");
	UART_SendDouble(UART_DEBUG, inTemp_C, 0, 1);
	UART_SendString(UART_DEBUG, "C\r\n");
	#endif  // DEBUG_WM_EN
	
	WeatherData[DATA_INSIDE_TEMPERATURE][DATA_CURRENT_VALUE] = data;  // Update current data
	if((int16_t) data > (int16_t) WeatherData[DATA_INSIDE_TEMPERATURE][DATA_MAX_VALUE]){
		WeatherData[DATA_INSIDE_TEMPERATURE][DATA_MAX_VALUE] = data;  // Update max data
	}
	if((int16_t) data < (int16_t)WeatherData[DATA_INSIDE_TEMPERATURE][DATA_MIN_VALUE]){
		WeatherData[DATA_INSIDE_TEMPERATURE][DATA_MIN_VALUE] = data;  // Update min data
	}
	// Update avg data
	aux = ((uint32_t)WeatherData[DATA_INSIDE_TEMPERATURE][DATA_AVG_VALUE] << 16) + WeatherData[DATA_INSIDE_TEMPERATURE][DATA_AVG_VALUE + 1] + data;
	WeatherData[DATA_INSIDE_TEMPERATURE][DATA_AVG_VALUE] = (uint16_t)(aux >> 16);
	WeatherData[DATA_INSIDE_TEMPERATURE][DATA_AVG_VALUE + 1] = (uint16_t)(aux);
}

/**
 * @brief  Update outside temperature data.                       \n
 *         Update current, maximum, minimum and average values.
 *
 * @param  data : Current value
 *
 * @return Nothing
 */
void updateOutsideTemperature(uint16_t data){
	uint32_t aux;
	
	#if DEBUG_WM_EN
	double outTemp_F = ((int16_t) data) / 10.0;  // Outside temperature in �F
	double outTemp_C = (outTemp_F - 32) / 1.8;  // Outside temperature in �C
	UART_SendString(UART_DEBUG, "(Data) Outside temperature: ");
	UART_SendDouble(UART_DEBUG, outTemp_F, 0, 1);
	UART_SendString(UART_DEBUG, "F / ");
	UART_SendDouble(UART_DEBUG, outTemp_C, 0, 1);
	UART_SendString(UART_DEBUG, "C\r\n");
	#endif  // DEBUG_WM_EN
	
	WeatherData[DATA_OUTSIDE_TEMPERATURE][DATA_CURRENT_VALUE] = data;  // Update current data
	if((int16_t) data > (int16_t) WeatherData[DATA_OUTSIDE_TEMPERATURE][DATA_MAX_VALUE]){
		WeatherData[DATA_OUTSIDE_TEMPERATURE][DATA_MAX_VALUE] = data;  // Update max data
	}
	if((int16_t) data < (int16_t) WeatherData[DATA_OUTSIDE_TEMPERATURE][DATA_MIN_VALUE]){
		WeatherData[DATA_OUTSIDE_TEMPERATURE][DATA_MIN_VALUE] = data;  // Update min data
	}
	// Update avg data
	aux = ((uint32_t)WeatherData[DATA_OUTSIDE_TEMPERATURE][DATA_AVG_VALUE] << 16) + WeatherData[DATA_OUTSIDE_TEMPERATURE][DATA_AVG_VALUE + 1] + data;
	WeatherData[DATA_OUTSIDE_TEMPERATURE][DATA_AVG_VALUE] = (uint16_t)(aux >> 16);
	WeatherData[DATA_OUTSIDE_TEMPERATURE][DATA_AVG_VALUE + 1] = (uint16_t)(aux);
}

/**
 * @brief  Update dew point data.                                 \n
 *         Update current, maximum, minimum and average values.
 *
 * @param  data : Current value
 *
 * @return Nothing
 */
void updateDewPoint(uint16_t data){
	uint32_t aux;
	
	#if DEBUG_WM_EN
	double dewPointF = ((int16_t) data) / 10.0;  // Dew point in �F
	double dewPointC = (dewPointF - 32) / 1.8;  // Dew point in �C
	UART_SendString(UART_DEBUG, "(Data) Dew point: ");
	UART_SendDouble(UART_DEBUG, dewPointF, 0, 1);
	UART_SendString(UART_DEBUG, "F / ");
	UART_SendDouble(UART_DEBUG, dewPointC, 0, 1);
	UART_SendString(UART_DEBUG, "C\r\n");
	#endif  // DEBUG_WM_EN
	
	WeatherData[DATA_DEW_POINT][DATA_CURRENT_VALUE] = data;  // Update current data
	if(data > WeatherData[DATA_DEW_POINT][DATA_MAX_VALUE]){
		WeatherData[DATA_DEW_POINT][DATA_MAX_VALUE] = data;  // Update max data
	}
	if(data < WeatherData[DATA_DEW_POINT][DATA_MIN_VALUE]){
		WeatherData[DATA_DEW_POINT][DATA_MIN_VALUE] = data;  // Update min data
	}
	// Update avg data
	aux = ((uint32_t)WeatherData[DATA_DEW_POINT][DATA_AVG_VALUE] << 16) + WeatherData[DATA_DEW_POINT][DATA_AVG_VALUE + 1] + data;
	WeatherData[DATA_DEW_POINT][DATA_AVG_VALUE] = (uint16_t)(aux >> 16);
	WeatherData[DATA_DEW_POINT][DATA_AVG_VALUE + 1] = (uint16_t)(aux);
}

/**
 * @brief  Update inside humidity data.                           \n
 *         Update current, maximum, minimum and average values.
 *
 * @param  data : Current value
 *
 * @return Nothing
 */
void updateInsideHumidity(uint16_t data){
	uint32_t aux;
	
	#if DEBUG_WM_EN
	uint16_t inHum = data;  // Inside humidity in %
	UART_SendString(UART_DEBUG, "(Data) Inside humidity: ");
	UART_SendInt(UART_DEBUG, inHum, 0);
	UART_SendString(UART_DEBUG, "%\r\n");
	#endif  // DEBUG_WM_EN
	
	WeatherData[DATA_INSIDE_HUMIDITY][DATA_CURRENT_VALUE] = data;  // Update current data
	if(data > WeatherData[DATA_INSIDE_HUMIDITY][DATA_MAX_VALUE]){
		WeatherData[DATA_INSIDE_HUMIDITY][DATA_MAX_VALUE] = data;  // Update max data
	}
	if(data < WeatherData[DATA_INSIDE_HUMIDITY][DATA_MIN_VALUE]){
		WeatherData[DATA_INSIDE_HUMIDITY][DATA_MIN_VALUE] = data;  // Update min data
	}
	// Update avg data
	aux = ((uint32_t)WeatherData[DATA_INSIDE_HUMIDITY][DATA_AVG_VALUE] << 16) + WeatherData[DATA_INSIDE_HUMIDITY][DATA_AVG_VALUE + 1] + data;
	WeatherData[DATA_INSIDE_HUMIDITY][DATA_AVG_VALUE] = (uint16_t)(aux >> 16);
	WeatherData[DATA_INSIDE_HUMIDITY][DATA_AVG_VALUE + 1] = (uint16_t)(aux);
}

/**
 * @brief  Update outside humidity data.                          \n
 *         Update current, maximum, minimum and average values.
 *
 * @param  data : Current value
 *
 * @return Nothing
 */
void updateOutsideHumidity(uint16_t data){
	uint32_t aux;
	
	#if DEBUG_WM_EN
	uint16_t outHum = data;  // Outside humidity in %
	UART_SendString(UART_DEBUG, "(Data) Outside humidity: ");
	UART_SendInt(UART_DEBUG, outHum, 0);
	UART_SendString(UART_DEBUG, "%\r\n");
	#endif  // DEBUG_WM_EN
	
	WeatherData[DATA_OUTSIDE_HUMIDITY][DATA_CURRENT_VALUE] = data;  // Update current data
	if(data > WeatherData[DATA_OUTSIDE_HUMIDITY][DATA_MAX_VALUE]){
		WeatherData[DATA_OUTSIDE_HUMIDITY][DATA_MAX_VALUE] = data;  // Update max data
	}
	if(data < WeatherData[DATA_OUTSIDE_HUMIDITY][DATA_MIN_VALUE]){
		WeatherData[DATA_OUTSIDE_HUMIDITY][DATA_MIN_VALUE] = data;  // Update min data
	}
	// Update avg data
	aux = ((uint32_t)WeatherData[DATA_OUTSIDE_HUMIDITY][DATA_AVG_VALUE] << 16) + WeatherData[DATA_OUTSIDE_HUMIDITY][DATA_AVG_VALUE + 1] + data;
	WeatherData[DATA_OUTSIDE_HUMIDITY][DATA_AVG_VALUE] = (uint16_t)(aux >> 16);
	WeatherData[DATA_OUTSIDE_HUMIDITY][DATA_AVG_VALUE + 1] = (uint16_t)(aux);
}

/**
 * @brief  Update wind speed data.                                \n
 *         Update current, maximum, minimum and average values.
 *
 * @param  data : Current value
 *
 * @return Nothing
 */
void updateWindSpeed(uint16_t data){
	uint32_t aux;
	
	#if DEBUG_WM_EN
	uint16_t windSpeed_mph = data;  // Wind speed in mph
	uint16_t windSpeed_kph = windSpeed_mph * 1.60934;  // Wind speed in kph
	UART_SendString(UART_DEBUG, "(Data) Wind speed: ");
	UART_SendInt(UART_DEBUG, windSpeed_mph, 0);
	UART_SendString(UART_DEBUG, "mph / ");
	UART_SendInt(UART_DEBUG, windSpeed_kph, 0);
	UART_SendString(UART_DEBUG, "kph\r\n");
	#endif  // DEBUG_WM_EN
	
	WeatherData[DATA_WIND_SPEED][DATA_CURRENT_VALUE] = data;  // Update current data
	if(data > WeatherData[DATA_WIND_SPEED][DATA_MAX_VALUE]){
		WeatherData[DATA_WIND_SPEED][DATA_MAX_VALUE] = data;  // Update max data
	}
	if(data < WeatherData[DATA_WIND_SPEED][DATA_MIN_VALUE]){
		WeatherData[DATA_WIND_SPEED][DATA_MIN_VALUE] = data;  // Update min data
	}
	// Update avg data
	aux = ((uint32_t)WeatherData[DATA_WIND_SPEED][DATA_AVG_VALUE] << 16) + WeatherData[DATA_WIND_SPEED][DATA_AVG_VALUE + 1] + data;
	WeatherData[DATA_WIND_SPEED][DATA_AVG_VALUE] = (uint16_t)(aux >> 16);
	WeatherData[DATA_WIND_SPEED][DATA_AVG_VALUE + 1] = (uint16_t)(aux);
}

/**
 * @brief  Update wind chill data.                                \n
 *         Update current, maximum, minimum and average values.
 *
 * @param  data : Current value
 *
 * @return Nothing
 */
void updateWindChill(uint16_t data){
	uint32_t aux;
	
	#if DEBUG_WM_EN
	double windChill_F = ((int16_t) data) / 10.0;  // Wind chill in �F
	double windChill_C = (windChill_F - 32) / 1.8;  // Wind chill in �C
	UART_SendString(UART_DEBUG, "(Data) Wind chill: ");
	UART_SendDouble(UART_DEBUG, windChill_F, 0, 1);
	UART_SendString(UART_DEBUG, "F / ");
	UART_SendDouble(UART_DEBUG, windChill_C, 0, 1);
	UART_SendString(UART_DEBUG, "C\r\n");
	#endif  // DEBUG_WM_EN
	
	WeatherData[DATA_WIND_CHILL][DATA_CURRENT_VALUE] = data;  // Update current data
	if(data > WeatherData[DATA_WIND_CHILL][DATA_MAX_VALUE]){
		WeatherData[DATA_WIND_CHILL][DATA_MAX_VALUE] = data;  // Update max data
	}
	if(data < WeatherData[DATA_WIND_CHILL][DATA_MIN_VALUE]){
		WeatherData[DATA_WIND_CHILL][DATA_MIN_VALUE] = data;  // Update min data
	}
	// Update avg data
	aux = ((uint32_t)WeatherData[DATA_WIND_CHILL][DATA_AVG_VALUE] << 16) + WeatherData[DATA_WIND_CHILL][DATA_AVG_VALUE + 1] + data;
	WeatherData[DATA_WIND_CHILL][DATA_AVG_VALUE] = (uint16_t)(aux >> 16);
	WeatherData[DATA_WIND_CHILL][DATA_AVG_VALUE + 1] = (uint16_t)(aux);
}

/**
 * @brief  Update wind direction data.   \n
 *         Update current value.
 *
 * @param  data : Current value
 *
 * @return Nothing
 */
void updateWindDirection(uint16_t data){
	#if DEBUG_WM_EN
	uint8_t* windVane = (uint8_t*) Data_CardinalPoint(data);  // Wind direction as cardinal point
	UART_SendString(UART_DEBUG, "(Data) Wind direction: ");
	UART_SendString(UART_DEBUG, windVane);
	UART_SendString(UART_DEBUG, "\r\n");
	#endif  // DEBUG_WM_EN
	
	WeatherData[DATA_WIND_DIRECTION][DATA_CURRENT_VALUE] = data;  // Update current data
}

/**
 * @brief  Update barometric pressure data.                       \n
 *         Update current, maximum, minimum and average values.
 *
 * @param  data : Current value
 *
 * @return Nothing
 */
void updateBarometricPressure(uint16_t data){
	uint32_t aux;
	
	#if DEBUG_WM_EN
	double pressure_inHg = ((int16_t) data) / 1000.0;  // Barometric pressure in inHg
	double pressure_hPa = pressure_inHg * 33.8638;  // Barometric pressure in hPa
	UART_SendString(UART_DEBUG, "(Data) Barometric pressure: ");
	UART_SendDouble(UART_DEBUG, pressure_inHg, 0, 3);
	UART_SendString(UART_DEBUG, "inHg / ");
	UART_SendDouble(UART_DEBUG, pressure_hPa, 0, 3);
	UART_SendString(UART_DEBUG, "hPa\r\n");
	#endif  // DEBUG_WM_EN
	
	WeatherData[DATA_BAROMETRIC_PRESSURE][DATA_CURRENT_VALUE] = data;  // Update current data
	if(data > WeatherData[DATA_BAROMETRIC_PRESSURE][DATA_MAX_VALUE]){
		WeatherData[DATA_BAROMETRIC_PRESSURE][DATA_MAX_VALUE] = data;  // Update max data
	}
	if(data < WeatherData[DATA_BAROMETRIC_PRESSURE][DATA_MIN_VALUE]){
		WeatherData[DATA_BAROMETRIC_PRESSURE][DATA_MIN_VALUE] = data;  // Update min data
	}
	// Update avg data
	aux = ((uint32_t)WeatherData[DATA_BAROMETRIC_PRESSURE][DATA_AVG_VALUE] << 16) + WeatherData[DATA_BAROMETRIC_PRESSURE][DATA_AVG_VALUE + 1] + data;
	WeatherData[DATA_BAROMETRIC_PRESSURE][DATA_AVG_VALUE] = (uint16_t)(aux >> 16);
	WeatherData[DATA_BAROMETRIC_PRESSURE][DATA_AVG_VALUE + 1] = (uint16_t)(aux);
}

/**
 * @brief  Update daily rain data.   \n
 *         Update current value.
 *
 * @param  data : Current value
 *
 * @return Nothing
 */
void updateDailyRain(uint16_t data){
	#if DEBUG_WM_EN
	uint16_t dailyRain = data;  // Daily rain in mm
	UART_SendString(UART_DEBUG, "(Data) Daily rain: ");
	UART_SendInt(UART_DEBUG, dailyRain, 0);
	UART_SendString(UART_DEBUG, "mm\r\n");
	#endif  // DEBUG_WM_EN
	
	WeatherData[DATA_DAILY_RAIN][DATA_CURRENT_VALUE] = data;  // Update current data
}

/**
 * @brief  Update yearly rain data.    \n
 *         Update current value.
 *
 * @param  data : Current value
 *
 * @return Nothing
 */
void updateYearlyRain(uint16_t data){
	#if DEBUG_WM_EN
	uint16_t yearlyRain = data;  // Yearly rain in mm
	UART_SendString(UART_DEBUG, "(Data) Yearly rain: ");
	UART_SendInt(UART_DEBUG, yearlyRain, 0);
	UART_SendString(UART_DEBUG, "mm\r\n");
	#endif  // DEBUG_WM_EN
	
	WeatherData[DATA_YEARLY_RAIN][DATA_CURRENT_VALUE] = data;  // Update current data
}

/**
 * @brief  Update battery voltage data.    \n
 *         Update current value.
 *
 * @param  data : Current value
 *
 * @return Nothing
 */
void updateBatteryVoltage(uint16_t data){
	#if DEBUG_WM_EN
	double battery_V = data / 100.0;  // Battery voltage in V
	UART_SendString(UART_DEBUG, "(Data) Battery voltage: ");
	UART_SendDouble(UART_DEBUG, battery_V, 0, 2);
	UART_SendString(UART_DEBUG, "V\r\n");
	#endif  // DEBUG_WM_EN
	
	WeatherData[DATA_BATTERY_VOLTAGE][DATA_CURRENT_VALUE] = data;  // Update current data
}

/**
 * @brief  Restart maximum, minimum and average statistics.             \n
 *         Discard old values and keep last value as the only sample.
 *
 * @param  None
 *
 * @return Nothing
 */
void Data_RestartStatiscalData(){
	uint8_t i;
	uint16_t data;
	for(i=0; i<DATA_WEATHER_VARIABLES; i++){
		data = WeatherData[i][0];  // Get last current value
		WeatherData[i][1] = data;  // Max value is last current value
		WeatherData[i][2] = data;  // Min value is last current value
		WeatherData[i][3] = 0;     // New average value fits into a 16bit variable
		WeatherData[i][4] = data;  // Average value is last current value
	}
	Samples = 1;  // Only has last current value
}