/**
 * @file   SD.h
 *
 * @brief  SD.c header.                                                  \n
 *         Driver for SD card.                                           \n
 *         Definitions of file access modes and SD access result codes.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _SD_H_
#define _SD_H_

/* Inclusions */ 
#include <avr/io.h>     // Driver for AVR input/output
#include "SD/mmc_if.h"  // Driver for MMC/SD cards
#include "SD/tff.h"     // FAT filesystem driver

/* File Access Modes */
#define SD_ERROR_MODE          FA__ERROR
#define SD_READ_MODE           FA_READ
#define SD_OPEN_EXISTING_MODE  FA_OPEN_EXISTING
#if !_FS_READONLY
#define SD_WRITE_MODE          FA_WRITE
#define SD_CREATE_NEW_MODE     FA_CREATE_NEW
#define SD_CREATE_ALWAYS_MODE  FA_CREATE_ALWAYS
#define SD_OPEN_ALWAYS_MODE    FA_OPEN_ALWAYS
#define SD_WRITTEN_MODE        FA__WRITTEN
#define SD_APPEND_MODE         0x80  // Append mode: new access mode added
#endif  // !_FS_READONLY

/* SD Access Result Codes */
typedef enum {
	SD_OK = 0,           // 0
	SD_NOT_READY,        // 1
	SD_NO_FILE,          // 2
	SD_NO_PATH,          // 3
	SD_INVALID_NAME,     // 4
	SD_INVALID_DRIVE,    // 5
	SD_DENIED,           // 6
	SD_EXIST,            // 7
	SD_RW_ERROR,         // 8
	SD_WRITE_PROTECTED,  // 9
	SD_NOT_ENABLED,      // 10
	SD_NO_FILESYSTEM,    // 11
	SD_INVALID_OBJECT,   // 12
	SD_MKFS_ABORTED      // 13 (not used)
} SD_RESULT;

/* File Type Redefinition */
typedef FIL FIL;

/* Functions Declaration */
SD_RESULT SD_Begin();
SD_RESULT SD_FileOpen(FIL *p_file, const uint8_t *fileName, uint8_t mode);
SD_RESULT SD_FileLseek(FIL *p_file, uint32_t offset);
SD_RESULT SD_FileRead(FIL *p_file, uint8_t *buffer, uint16_t bytesToRead, uint16_t *p_bytesRead);
SD_RESULT SD_FileWrite(FIL *p_file, uint8_t *buffer, uint16_t bytesToWrite, uint16_t *p_bytesWritten);
SD_RESULT SD_FileClose(FIL *p_file);
SD_RESULT SD_FileTruncate(FIL *p_file);
SD_RESULT SD_DirOpen(DIR *p_dir, const uint8_t *dirPath);
SD_RESULT SD_DirRead(DIR *p_dir, FILINFO *fileInfo);
SD_RESULT SD_DirCreate(const uint8_t *dirPath);
SD_RESULT SD_FileInfo(const uint8_t *filePath, FILINFO *fileInfo);
SD_RESULT SD_FileRename(const uint8_t *pathOld, const uint8_t *pathNew);
SD_RESULT SD_FileChangeAttribute(const uint8_t *filePath, uint8_t value, uint8_t mask);
SD_RESULT SD_FileDelete(const uint8_t *filePath);
SD_RESULT SD_End();

#endif  // _SD_H_