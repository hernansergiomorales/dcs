/**
 * @file   RTC.h
 *
 * @brief  RTC.c header.                                \n
 *         Driver for DS3231 real time clock module.    \n
 *         Defines an struct type for time/date value.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _RTC_H_
#define _RTC_H_

/* Inclusions */
#include <avr/io.h>  // Driver for AVR input/output

/* Type Definitions */
typedef struct rtc {
	uint8_t  hh;
	uint8_t  mm;
	uint8_t  ss;
	uint8_t  DD;
	uint8_t  MM;
	uint16_t YY;
} rtc_t;  // Time/date struct: hh:mm:ss DD/MM/YYYY

/* Functions Declaration */
rtc_t RTC_Make(uint8_t hh, uint8_t mm, uint8_t ss, uint8_t DD, uint8_t MM, uint16_t YY);
void RTC_SetTime(rtc_t time);
void RTC_GetTime(rtc_t *time);

#endif  // _RTC_H_