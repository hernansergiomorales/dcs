/**
 * @file   WeatherStation.c
 *
 * @brief  Driver for Davis Monitor access.                                     \n
 *         All weather variables can be accessed separately or in packages.     \n
 *         Calibration function, time and loop commands.                        \n
 *         There are available four modes to access data from weather monitor.
 *
 * @author Hernán Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "WeatherStation.h"       // Header file
#include "../Utilities/System.h"  // Global system settings
#include "../Protocols/UART.h"    // Driver for UART communications
#include "../Utilities/Timer.h"   // Timing functions

/* Internal Variables */
const uint16_t crc_table [] = {
	0x0000,  0x1021,  0x2042,  0x3063,  0x4084,  0x50A5,  0x60C6,  0x70E7,
	0x8108,  0x9129,  0xA14A,  0xB16B,  0xC18C,  0xD1AD,  0xE1CE,  0xF1EF,
	0x1231,  0x0210,  0x3273,  0x2252,  0x52B5,  0x4294,  0x72F7,  0x62D6,
	0x9339,  0x8318,  0xB37B,  0xA35A,  0xD3BD,  0xC39C,  0xF3FF,  0xE3DE,
	0x2462,  0x3443,  0x0420,  0x1401,  0x64E6,  0x74C7,  0x44A4,  0x5485,
	0xA56A,  0xB54B,  0x8528,  0x9509,  0xE5EE,  0xF5CF,  0xC5AC,  0xD58D,
	0x3653,  0x2672,  0x1611,  0x0630,  0x76D7,  0x66F6,  0x5695,  0x46B4,
	0xB75B,  0xA77A,  0x9719,  0x8738,  0xF7DF,  0xE7FE,  0xD79D,  0xC7BC,
	0x48C4,  0x58E5,  0x6886,  0x78A7,  0x0840,  0x1861,  0x2802,  0x3823,
	0xC9CC,  0xD9ED,  0xE98E,  0xF9AF,  0x8948,  0x9969,  0xA90A,  0xB92B,
	0x5AF5,  0x4AD4,  0x7AB7,  0x6A96,  0x1A71,  0x0A50,  0x3A33,  0x2A12,
	0xDBFD,  0xCBDC,  0xFBBF,  0xEB9E,  0x9B79,  0x8B58,  0xBB3B,  0xAB1A,
	0x6CA6,  0x7C87,  0x4CE4,  0x5CC5,  0x2C22,  0x3C03,  0x0C60,  0x1C41,
	0xEDAE,  0xFD8F,  0xCDEC,  0xDDCD,  0xAD2A,  0xBD0B,  0x8D68,  0x9D49,
	0x7E97,  0x6EB6,  0x5ED5,  0x4EF4,  0x3E13,  0x2E32,  0x1E51,  0x0E70,
	0xFF9F,  0xEFBE,  0xDFDD,  0xCFFC,  0xBF1B,  0xAF3A,  0x9F59,  0x8F78,
	0x9188,  0x81A9,  0xB1CA,  0xA1EB,  0xD10C,  0xC12D,  0xF14E,  0xE16F,
	0x1080,  0x00A1,  0x30C2,  0x20E3,  0x5004,  0x4025,  0x7046,  0x6067,
	0x83B9,  0x9398,  0xA3FB,  0xB3DA,  0xC33D,  0xD31C,  0xE37F,  0xF35E,
	0x02B1,  0x1290,  0x22F3,  0x32D2,  0x4235,  0x5214,  0x6277,  0x7256,
	0xB5EA,  0xA5CB,  0x95A8,  0x8589,  0xF56E,  0xE54F,  0xD52C,  0xC50D,
	0x34E2,  0x24C3,  0x14A0,  0x0481,  0x7466,  0x6447,  0x5424,  0x4405,
	0xA7DB,  0xB7FA,  0x8799,  0x97B8,  0xE75F,  0xF77E,  0xC71D,  0xD73C,
	0x26D3,  0x36F2,  0x0691,  0x16B0,  0x6657,  0x7676,  0x4615,  0x5634,
	0xD94C,  0xC96D,  0xF90E,  0xE92F,  0x99C8,  0x89E9,  0xB98A,  0xA9AB,
	0x5844,  0x4865,  0x7806,  0x6827,  0x18C0,  0x08E1,  0x3882,  0x28A3,
	0xCB7D,  0xDB5C,  0xEB3F,  0xFB1E,  0x8BF9,  0x9BD8,  0xABBB,  0xBB9A,
	0x4A75,  0x5A54,  0x6A37,  0x7A16,  0x0AF1,  0x1AD0,  0x2AB3,  0x3A92,
	0xFD2E,  0xED0F,  0xDD6C,  0xCD4D,  0xBDAA,  0xAD8B,  0x9DE8,  0x8DC9,
	0x7C26,  0x6C07,  0x5C64,  0x4C45,  0x3CA2,  0x2C83,  0x1CE0,  0x0CC1,
	0xEF1F,  0xFF3E,  0xCF5D,  0xDF7C,  0xAF9B,  0xBFBA,  0x8FD9,  0x9FF8,
	0x6E17,  0x7E36,  0x4E55,  0x5E74,  0x2E93,  0x3EB2,  0x0ED1,  0x1EF0,
};

/* Internal Functions Declaration */
static uint16_t checkCRC(uint8_t data []);
static uint8_t getAcknowledge();
static uint16_t getResponse(uint8_t command, uint8_t address);
uint8_t getData(uint8_t command, uint8_t address, uint16_t *data);
void getWeatherChipRam(uint8_t command,  uint8_t address);

/**
 * @brief  Weather link model getter.
 *
 * @param  None
 *
 * @return Weather link model or NACK if monitor doesn't answer.
 */
uint16_t WeatherStation_GetweatherLinkModel(){
    uint16_t data;
    getData(DAVIS_MODEL, &data);
    return data;
}

/**
 * @brief  Inside temperature getter.
 *
 * @param  data : Pointer to where to store data.
 *
 * @return WS_NACK if weather monitor doesn't answer, no error if it does.
 */
uint8_t WeatherStation_GetInsideTemperature(int16_t *data){
    uint8_t status = getData(WCR_INSIDE_TEMPERATURE, data);
	if(status != WS_PENDING_RESPONSE){
		if(*data == WS_NACK){
			*data = WS_POSITIVE_SIGNED_4NIBBLE_ERROR;
			return WS_NACK;
		}
		if(!(*data == WS_POSITIVE_SIGNED_4NIBBLE_ERROR || *data == WS_NEGATIVE_SIGNED_4NIBBLE_ERROR)){
			#if SOFTWARE_CALIBRATION
			*data = *data + SOFTWARE_CAL_INSIDE_TEMPERATURE;
			#else
			int16_t calibration;
			getData(WCR_CAL_INSIDE_TEMPERATURE, &calibration);  // read the Cal number out of the buffer
			*data = *data + calibration;  // InTemp is now Calibrated
			#endif
		}
	}
	return status;
}

/**
 * @brief  Average inside temperature getter.
 *
 * @param  avg : Pointer to where to store data.
 *
 * @return Nothing
 */
void WeatherStation_GetAvgInsideTemperature(int16_t *avg){
    *avg = (int16_t) WeatherStation_GetLinkChipRam(CCR_AVG_INSIDE_TEMPERATURE);
}

/**
 * @brief  High inside temperature getter.
 *
 * @param  high    : Pointer to where to store data.
 * @param  month   : Month of the sample.
 * @param  day     : Day of the sample.
 * @param  hour    : Hour of the sample.
 * @param  minutes : Minute of the sample.
 *
 * @return Nothing
 */
void WeatherStation_GetHighInsideTemperature(int16_t *high, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes){
    uint16_t aux;
    getData(WCR_HIGH_INSIDE_TEMPERATURE, high);
    getData(WCR_HIGH_INSIDE_TEMPERATURE_TIME, &aux);
    *hour = aux >> 8;
    *minutes = aux;
    getData(WCR_HIGH_INSIDE_TEMPERATURE_DATE, &aux);
    *day = aux >> 8;
    *month = aux;
}

/**
 * @brief  Low inside temperature getter.
 *
 * @param  low     : Pointer to where to store data.
 * @param  month   : Month of the sample.
 * @param  day     : Day of the sample.
 * @param  hour    : Hour of the sample.
 * @param  minutes : Minute of the sample.
 *
 * @return Nothing
 */
void WeatherStation_GetLowInsideTemperature(int16_t *low, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes){
    uint16_t aux;
    getData(WCR_LOW_INSIDE_TEMPERATURE, low);
    getData(WCR_LOW_INSIDE_TEMPERATURE_TIME, &aux);
    *hour = aux >> 8;
    *minutes = aux;
    getData(WCR_LOW_INSIDE_TEMPERATURE_DATE, &aux);
    *day = aux >> 8;
    *month = aux;
}

/**
 * @brief  Outside temperature getter.
 *
 * @param  data : Pointer to where to store data.
 *
 * @return WS_NACK if weather monitor doesn't answer, no error if it does.
 */
uint8_t WeatherStation_GetOutsideTemperature(int16_t *data){
    uint8_t status = getData(WCR_OUTSIDE_TEMPERATURE, data);
    if(status != WS_PENDING_RESPONSE){
	    if(*data == WS_NACK){
		    *data = WS_POSITIVE_SIGNED_4NIBBLE_ERROR;
		    return WS_NACK;
	    }
	    if(!(*data == WS_POSITIVE_SIGNED_4NIBBLE_ERROR || *data == WS_NEGATIVE_SIGNED_4NIBBLE_ERROR)){
		    #if SOFTWARE_CALIBRATION
		    *data = *data + SOFTWARE_CAL_OUTSIDE_TEMPERATURE;
		    #else
		    int16_t calibration;
			getData(WCR_CAL_OUTSIDE_TEMPERATURE, &calibration);  // read the Cal number out of the buffer
		    *data = *data + calibration;  // InTemp is now Calibrated
		    #endif
	    }
    }
    return status;
} 

/**
 * @brief  Average outside temperature getter.
 *
 * @param  avg : Pointer to where to store data.
 *
 * @return Nothing
 */
void WeatherStation_GetAvgOutsideTemperature(int16_t *avg){
    *avg = (int16_t)WeatherStation_GetLinkChipRam(CCR_AVG_OUTSIDE_TEMPERATURE);
}

/**
 * @brief  High outside temperature getter.
 *
 * @param  high    : Pointer to where to store data.
 * @param  month   : Month of the sample.
 * @param  day     : Day of the sample.
 * @param  hour    : Hour of the sample.
 * @param  minutes : Minute of the sample.
 *
 * @return Nothing
 */
void WeatherStation_GetHighOutsideTemperature(int16_t *high, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes){
    uint16_t aux;
    *high = (int16_t) getData(WCR_HIGH_OUTSIDE_TEMPERATURE, high);
    getData(WCR_HIGH_OUTSIDE_TEMPERATURE_TIME, &aux);
    *hour = aux >> 8;
    *minutes = aux;
    getData(WCR_HIGH_OUTSIDE_TEMPERATURE_DATE, &aux);
    *day = aux >> 8;
    *month = aux;
}

/**
 * @brief  Low outside temperature getter.
 *
 * @param  low     : Pointer to where to store data.
 * @param  month   : Month of the sample.
 * @param  day     : Day of the sample.
 * @param  hour    : Hour of the sample.
 * @param  minutes : Minute of the sample.
 *
 * @return Nothing
 */
void WeatherStation_GetLowOutsideTemperature(int16_t *low, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes){
    uint16_t aux;
    getData(WCR_LOW_OUTSIDE_TEMPERATURE, low);
    getData(WCR_LOW_OUTSIDE_TEMPERATURE_TIME, &aux);
    *hour = aux >> 8;
    *minutes = aux;
    getData(WCR_LOW_OUTSIDE_TEMPERATURE_DATE, &aux);
    *day = aux >> 8;
    *month = aux;
}

/**
 * @brief  Dew point getter.
 *
 * @param  data : Pointer to where to store data.
 *
 * @return WS_NACK if weather monitor doesn't answer, no error if it does.
 */
uint8_t WeatherStation_GetDewPoint(uint16_t *data){
    return getData(WCR_DEW_POINT, data);
}

/**
 * @brief  High dew point getter.
 *
 * @param  high    : Pointer to where to store data.
 * @param  month   : Month of the sample.
 * @param  day     : Day of the sample.
 * @param  hour    : Hour of the sample.
 * @param  minutes : Minute of the sample.
 *
 * @return Nothing
 */
void WeatherStation_GetHighDewPoint(uint16_t *high, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes){
    uint16_t aux;
    getData(WCR_HIGH_DEW_POINT, high);
    getData(WCR_HIGH_DEW_POINT_TIME, &aux);
    *hour = aux >> 8;
    *minutes = aux;
    getData(WCR_HIGH_DEW_POINT_DATE, &aux);
    *day = aux >> 8;
    *month = aux;
}

/**
 * @brief  Low dew point getter.
 *
 * @param  low     : Pointer to where to store data.
 * @param  month   : Month of the sample.
 * @param  day     : Day of the sample.
 * @param  hour    : Hour of the sample.
 * @param  minutes : Minute of the sample.
 *
 * @return Nothing
 */
void WeatherStation_GetLowDewPoint(uint16_t *low, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes){
    uint16_t aux;
    getData(WCR_LOW_DEW_POINT, low);
    getData(WCR_LOW_DEW_POINT_TIME, &aux);
    *hour = aux >> 8;
    *minutes = aux;
    getData(WCR_LOW_DEW_POINT_DATE, &aux);
    *day = aux >> 8;
    *month = aux;
}

/**
 * @brief  Inside humidity getter.
 *
 * @param  data : Pointer to where to store data.
 *
 * @return WS_NACK if weather monitor doesn't answer, no error if it does.
 */
uint8_t WeatherStation_GetInsideHumidity(uint16_t *data){
    uint8_t status = getData(WCR_INSIDE_HUMIDITY, data);
	if(status != WS_PENDING_RESPONSE){
		if(*data == WS_NACK){
			*data = WS_POSITIVE_SIGNED_2NIBBLE_ERROR;
			return WS_NACK;
		}
		if(*data != WS_POSITIVE_SIGNED_2NIBBLE_ERROR){
			#if SOFTWARE_CALIBRATION
			*data = *data + SOFTWARE_CAL_HUMIDITY;
			#else
			int16_t calibration;
			getData(WCR_CAL_HUMIDITY, &calibration);    // read the Cal number out of the buffer
			*data = *data + calibration;											// data is now Calibrated
			#endif
			if (*data > 100)
			*data = 100;
			if(*data < 1)
			*data = 1;
		}
	}
	return status;
}

/**
 * @brief  High inside humidity getter.
 *
 * @param  high    : Pointer to where to store data.
 * @param  month   : Month of the sample.
 * @param  day     : Day of the sample.
 * @param  hour    : Hour of the sample.
 * @param  minutes : Minute of the sample.
 *
 * @return Nothing
 */
void WeatherStation_GetHighInsideHumidity(uint16_t *high, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes){
    uint16_t aux;
    getData(WCR_HIGH_INSIDE_HUMIDITY, high);
    getData(WCR_HIGH_INSIDE_HUMIDITY_TIME, &aux);
    *hour = aux >> 8;
    *minutes = aux;
    getData(WCR_HIGH_INSIDE_HUMIDITY_DATE, &aux);
    *day = aux >> 8;
    *month = aux;
}

/**
 * @brief  Low inside humidity getter.
 *
 * @param  low     : Pointer to where to store data.
 * @param  month   : Month of the sample.
 * @param  day     : Day of the sample.
 * @param  hour    : Hour of the sample.
 * @param  minutes : Minute of the sample.
 *
 * @return Nothing
 */
void WeatherStation_GetLowInsideHumidity(uint16_t *low, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes){
    uint16_t aux;
    getData(WCR_LOW_INSIDE_HUMIDITY, low);
    getData(WCR_LOW_INSIDE_HUMIDITY_TIME, &aux);
    *hour = aux >> 8;
    *minutes = aux;
    getData(WCR_LOW_INSIDE_HUMIDITY_DATE, &aux);
    *day = aux >> 8;
    *month = aux;
}

/**
 * @brief  Outside humidity getter.
 *
 * @param  data : Pointer to where to store data.
 *
 * @return WS_NACK if weather monitor doesn't answer, no error if it does.
 */
uint8_t WeatherStation_GetOutsideHumidity (uint16_t *data){
    uint8_t status = getData(WCR_OUTSIDE_HUMIDITY, data);
    if(status != WS_PENDING_RESPONSE){
	    if(*data == WS_NACK){
		    *data = WS_POSITIVE_SIGNED_2NIBBLE_ERROR;
		    return WS_NACK;
	    }
	    if(*data != WS_POSITIVE_SIGNED_2NIBBLE_ERROR){
		    #if SOFTWARE_CALIBRATION
		    *data = *data + SOFTWARE_CAL_HUMIDITY;
		    #else
		    int16_t calibration;
			getData(WCR_CAL_HUMIDITY, &calibration);  // read the Cal number out of the buffer
		    *data = *data + calibration;              // data is now Calibrated
		    #endif
		    if (*data > 100)
		    *data = 100;
		    if(*data < 1)
		    *data = 1;
	    }
    }
    return status;
}

/**
 * @brief  High outside humidity getter.
 *
 * @param  high    : Pointer to where to store data.
 * @param  month   : Month of the sample.
 * @param  day     : Day of the sample.
 * @param  hour    : Hour of the sample.
 * @param  minutes : Minute of the sample.
 *
 * @return Nothing
 */
void WeatherStation_GetHighOutsideHumidity(uint16_t *high, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes){
    uint16_t aux;
    getData(WCR_HIGH_OUTSIDE_HUMIDITY, high);
    getData(WCR_HIGH_OUTSIDE_HUMIDITY_TIME, &aux);
    *hour = aux >> 8;
    *minutes = aux;
    getData(WCR_HIGH_OUTSIDE_HUMIDITY_DATE, &aux);
    *day = aux >> 8;
    *month = aux;
}

/**
 * @brief  Low outsider humidity getter.
 *
 * @param  low     : Pointer to where to store data.
 * @param  month   : Month of the sample.
 * @param  day     : Day of the sample.
 * @param  hour    : Hour of the sample.
 * @param  minutes : Minute of the sample.
 *
 * @return Nothing
 */
void WeatherStation_GetLowOutsideHumidity(uint16_t *low, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes){
    uint16_t aux;
    getData(WCR_LOW_OUTSIDE_HUMIDITY, low);
    getData(WCR_LOW_OUTSIDE_HUMIDITY_TIME, &aux);
    *hour = aux >> 8;
    *minutes = aux;
    getData(WCR_LOW_OUTSIDE_HUMIDITY_DATE, &aux);
    *day = aux >> 8;
    *month = aux;
}

/**
 * @brief  Wind speed getter.
 *
 * @param  data : Pointer to where to store data.
 *
 * @return WS_NACK if weather monitor doesn't answer, no error if it does.
 */
uint8_t WeatherStation_GetWindSpeed(uint16_t *data){
    return getData(WCR_WIND_SPEED, data);
}

/**
 * @brief  Average wind speed getter.
 *
 * @param  avg : Pointer to where to store data.
 *
 * @return Nothing
 */
void WeatherStation_GetAvgWindSpeed(uint8_t *avg){
    *avg = WeatherStation_GetLinkChipRam(CCR_AVG_WIND_SPEED);
}

/**
 * @brief  High wind chill getter.
 *
 * @param  high    : Pointer to where to store data.
 * @param  month   : Month of the sample.
 * @param  day     : Day of the sample.
 * @param  hour    : Hour of the sample.
 * @param  minutes : Minute of the sample.
 *
 * @return Nothing
 */
void WeatherStation_GetHighWindSpeed(uint16_t *high, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes){
    uint16_t aux;
    getData(WCR_HIGH_WIND_SPEED, high);
    getData(WCR_HIGH_WIND_SPEED_TIME, &aux);
    *hour = aux >> 8;
    *minutes = aux;
    getData(WCR_HIGH_WIND_SPEED_DATE, &aux);
    *day = aux >> 8;
    *month = aux;
}

/**
 * @brief  Wind chill getter.
 *
 * @param  data : Pointer to where to store data.
 *
 * @return WS_NACK if weather monitor doesn't answer, no error if it does.
 */
uint8_t WeatherStation_GetWindChill(uint16_t *data){
    return getData(WCR_WIND_CHILL, data);
}

/**
 * @brief  Low wind chill getter.
 *
 * @param  low     : Pointer to where to store data.
 * @param  month   : Month of the sample.
 * @param  day     : Day of the sample.
 * @param  hour    : Hour of the sample.
 * @param  minutes : Minute of the sample.
 *
 * @return Nothing
 */
void WeatherStation_GetLowWindChill(uint16_t *low, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes){
    uint16_t aux;
    getData(WCR_LOW_WIND_CHILL, low);
    getData(WCR_LOW_WIND_CHILL_TIME, &aux);
    *hour = aux >> 8;
    *minutes = aux;
    getData(WCR_LOW_WIND_CHILL_DATE, &aux);
    *day = aux >> 8;
    *month = aux;
}

/**
 * @brief  Wind direction getter.
 *
 * @param  data : Pointer to where to store data.
 *
 * @return WS_NACK if weather monitor doesn't answer, no error if it does.
 */
uint8_t WeatherStation_GetWindDirection(uint16_t *data){
    uint8_t status = getData(WCR_WIND_DIRECTION, data);
	if(status != WS_PENDING_RESPONSE){
		if(*data == WS_NACK){
			*data = WS_POSITIVE_SIGNED_4NIBBLE_ERROR;
			return WS_NACK;
		}
	}
	return status;
}

/**
 * @brief  Barometric pressure getter.
 *
 * @param  data : Pointer to where to store data.
 *
 * @return WS_NACK if weather monitor doesn't answer, no error if it does.
 */
uint8_t WeatherStation_GetBarometricPressure(uint16_t *data){
    uint8_t status = getData(WCR_BAROMETER, data);
    if(status != WS_PENDING_RESPONSE){
		if(*data == WS_NACK){
			*data = WS_POSITIVE_SIGNED_4NIBBLE_ERROR;
			return WS_NACK;
		}
		else if(!(*data == WS_POSITIVE_SIGNED_4NIBBLE_ERROR || *data == WS_NEGATIVE_SIGNED_4NIBBLE_ERROR)){
			#if SOFTWARE_CALIBRATION
			*data= *data + SOFTWARE_CAL_BAROMETER;
			#else    
			int16_t calibration;
			getData(WCR_CAL_BAROMETER, &calibration);  // Read the cal number out of the buffer
			*data = *data + calibration;  // Data is now calibrated
			#endif
		}
	}
	return status;
}

/**
 * @brief  Daily rain getter.
 *
 * @param  data : Pointer to where to store data.
 *
 * @return WS_NACK if weather monitor doesn't answer, no error if it does.
 */
uint8_t WeatherStation_GetDailyRain(uint16_t *data){
	uint8_t status = getData(WCR_DAILY_RAIN, data);
	if(status != WS_PENDING_RESPONSE){
		if(*data == WS_NACK){
			return WS_NACK;
		} else {
			#if SOFTWARE_CALIBRATION
			*data += SOFTWARE_CAL_RAIN;
			#else   
			int16_t calibration;
			getData(WCR_CAL_RAIN, &calibration);  // Read the cal number out of the buffer 
			*data /= calibration;  // Data is now calibrated
			#endif
		}
	}
	return status;
}

/**
 * @brief  Yearly rain getter.
 *
 * @param  data : Pointer to where to store data.
 *
 * @return WS_NACK if weather monitor doesn't answer, no error if it does.
 */
uint8_t WeatherStation_GetYearlyRain(uint16_t *data){
    uint8_t status = getData(WCR_YEARLY_RAIN, data);
    if(status != WS_PENDING_RESPONSE){
	    if(*data == WS_NACK){
		    return WS_NACK;
		} else {
		    #if SOFTWARE_CALIBRATION
		    *data += SOFTWARE_CAL_RAIN;
		    #else
		    int16_t calibration;
			getData(WCR_CAL_RAIN, &calibration);  // Read the cal number out of the buffer
		    *data /= calibration;  // Data is now calibrated
		    #endif
	    }
    }
    return status;
}

/**
 * @brief  Enables station timer.
 *
 * @param  None
 *
 * @return WS_NACK if weather monitor doesn't answer, no error if it does.
 */
uint8_t WeatherStation_EnableTimer(){
    UART_SendString(UART_RS232, "EBT");
    UART_TxChar(UART_RS232, WS_CR);
    return getAcknowledge();
}

/**
 * @brief  Disable station timer.
 *
 * @param  None
 *
 * @return WS_NACK if weather monitor doesn't answer, no error if it does.
 */
uint8_t WeatherStation_DisableTimer(){
    UART_SendString(UART_RS232, "DBT");
    UART_TxChar(UART_RS232, WS_CR);
    return getAcknowledge();
}

/**
 * @brief  Set station date/time.
 *
 * @param  month   : Month to be set.
 * @param  day     : Day to be set.
 * @param  hour    : Hour to be set.
 * @param  minutes : Minutes to be set.
 *
 * @return Nothing
 */
void WeatherStation_SetStationTime(uint8_t month, uint8_t day, uint8_t hour, uint8_t minutes){  
    // Set Date
    UART_SendString(UART_RS232, "WWR");
    UART_TxChar(UART_RS232, WCR_DATE_COMMAND);
    UART_TxChar(UART_RS232, WCR_DATE_ADDRESS);
    UART_TxChar(UART_RS232, day);
    UART_TxChar(UART_RS232, month);
    UART_TxChar(UART_RS232, WS_CR);
    getAcknowledge();

    // Set Time
    WeatherStation_DisableTimer();
    UART_SendString(UART_RS232, "WWR");
    UART_TxChar(UART_RS232, WCR_TIME_COMMAND);
    UART_TxChar(UART_RS232, WCR_TIME_ADDRESS);
    UART_TxChar(UART_RS232, hour);
    UART_TxChar(UART_RS232, minutes);
    UART_TxChar(UART_RS232, 0);
    UART_TxChar(UART_RS232, WS_CR);
    getAcknowledge();
    WeatherStation_EnableTimer();
}

/**
 * @brief  Get station date/time.
 *
 * @param  month   : Pointer to where to store month data.
 * @param  day     : Pointer to where to store day data.
 * @param  hour    : Pointer to where to store hour data.
 * @param  minutes : Pointer to where to store minutes data.
 * @param  seconds : Pointer to where to store seconds data.
 *
 * @return Nothing
 */
void WeatherStation_GetStationTime(uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes, uint8_t *seconds){
    getData(WCR_HOUR, hour);
    getData(WCR_MINUTES, minutes);
    getData(WCR_SECONDS, seconds);
    getData(WCR_DAY, day);
    getData(WCR_MONTH, month);
}

/**
 * @brief  Set calibration numbers.
 *
 * @param  None
 *
 * @return Nothing
 */
void WeatherStation_SetCalibrationNumbers(){
    // Rain calibration
    UART_SendString(UART_RS232, "WWR");
    UART_TxChar(UART_RS232, WCR_CAL_RAIN_COMMAND);
    UART_TxChar(UART_RS232, WCR_CAL_RAIN_ADDRESS);
    UART_TxChar(UART_RS232, SOFTWARE_CAL_RAIN >> 8);
    UART_TxChar(UART_RS232, SOFTWARE_CAL_RAIN);

    // Barometric pressure calibration
    UART_SendString(UART_RS232, "WWR");
    UART_TxChar(UART_RS232, WCR_CAL_BAROMETER_COMMAND);
    UART_TxChar(UART_RS232, WCR_CAL_BAROMETER_ADDRESS);
    UART_TxChar(UART_RS232, SOFTWARE_CAL_BAROMETER >> 8);
    UART_TxChar(UART_RS232, SOFTWARE_CAL_BAROMETER);

    // Inside temperature calibration
    UART_SendString(UART_RS232, "WWR");
    UART_TxChar(UART_RS232, WCR_CAL_INSIDE_TEMPERATURE_COMMAND);
    UART_TxChar(UART_RS232, WCR_CAL_INSIDE_TEMPERATURE_ADDRESS);
    UART_TxChar(UART_RS232, SOFTWARE_CAL_INSIDE_TEMPERATURE >> 8);
    UART_TxChar(UART_RS232, SOFTWARE_CAL_INSIDE_TEMPERATURE);

    // Outside temperature calibration
    UART_SendString(UART_RS232, "WWR");
    UART_TxChar(UART_RS232, WCR_CAL_OUTSIDE_TEMPERATURE_COMMAND);
    UART_TxChar(UART_RS232, WCR_CAL_OUTSIDE_TEMPERATURE_ADDRESS);
    UART_TxChar(UART_RS232, SOFTWARE_CAL_OUTSIDE_TEMPERATURE >> 8);
    UART_TxChar(UART_RS232, SOFTWARE_CAL_OUTSIDE_TEMPERATURE);

    // Humidity calibration
    UART_SendString(UART_RS232, "WWR");
    UART_TxChar(UART_RS232, WCR_CAL_HUMIDITY_COMMAND);
    UART_TxChar(UART_RS232, WCR_CAL_HUMIDITY_ADDRESS);
    UART_TxChar(UART_RS232, SOFTWARE_CAL_HUMIDITY >> 8);
    UART_TxChar(UART_RS232, SOFTWARE_CAL_HUMIDITY);
}

/**
 * @brief  Set sample period for loop command.
 *
 * @param  newSamplePeriod : Sample period to be set.
 *
 * @return WS_NACK if weather monitor doesn't answer, no error if it does.
 */
uint8_t WeatherStation_SetSamplePeriod(uint8_t newSamplePeriod){
   UART_SendString(UART_RS232, "SSP");
   UART_TxChar(UART_RS232, 256 - newSamplePeriod);  // Write the new period
   UART_TxChar(UART_RS232, WS_CR);                  // Write CR.
   return getAcknowledge();
}

/**
 * @brief  Start loop command.
 *
 * @param  packets : Number of packets to receive.
 *
 * @return WS_NACK if weather monitor doesn't answer, no error if it does.
 */
uint8_t WeatherStation_InitLoopCommand(uint16_t packets){
    UART_SendString(UART_RS232, "LOOP");
    UART_TxChar(UART_RS232, 65535 - packets);  // Write the new period
    UART_TxChar(UART_RS232, WS_CR);            // Write CR.
    return getAcknowledge();
}

/**
 * @brief  Get packets from loop command.
 *
 * @param  data : Pointer to where to store data.
 *
 * @return Requested data or NACK if weather station doesn't respond.
 */
uint8_t WeatherStation_GetLoopPackets(uint8_t data[]){
    uint8_t header, received;
    received = UART_RxChar(UART_RS232, &header);
    while(received == LOCAL_ERROR)
        received = UART_RxChar(UART_RS232, &header); 
    if(header != WS_HEADER)
        return WS_HEADER_ERROR;
    else {
        UART_RecvString(UART_RS232, data, WS_SENSOR_IMAGE_LENGTH - 1);  
        return checkCRC(data);
    }
}

/**
 * @brief  Send commands to the station.
 *
 * @param  command : Command to be sent.
 * @param  address : Memory address where data is stored.
 *
 * @return Nothing
 */
void getWeatherChipRam(uint8_t command,  uint8_t address){
    // Read (1-8) nibbles of data from the station memory starting at 'address'
    UART_SendString(UART_RS232, "WRD");
    UART_TxChar(UART_RS232, command);  // Write the command
    UART_TxChar(UART_RS232, address);  // Write the address
    UART_TxChar(UART_RS232, WS_CR);    // Write CR
}

/**
 * @brief   Send commands to the weather link.
 *
 * @param   bank              : Memory bank where data is stored.
 * @param   address           : Memory address where data is stored.
 * @param   number_of_nibbles : Number of nibbles to be read.
 *
 * @return  Requested data or NACK if weather link doesn't respond.
 */
uint16_t WeatherStation_GetLinkChipRam(uint8_t bank, uint8_t address, uint8_t number_of_nibbles){
    uint16_t bitdata16=0;
    uint8_t bitdata8, received, i; 

    // Read (1-8) nibbles from the link memory from 'address' in 'bank'
    UART_SendString(UART_RS232, "RRD");
    UART_TxChar(UART_RS232, bank);                   // Write the bank
    UART_TxChar(UART_RS232, address);                // Write the address
    UART_TxChar(UART_RS232, number_of_nibbles - 1);  // Get number of nibbles
    UART_TxChar(UART_RS232, WS_CR);                  // Write CR

    if(getAcknowledge() == WS_ACK){
        for (i=0; i<(number_of_nibbles + 1)/2; i++){
            received = UART_RxChar(UART_RS232, &bitdata8);
            while(received == LOCAL_ERROR)
                received = UART_RxChar(UART_RS232, &bitdata8); 
            bitdata16 = bitdata16 | (bitdata8 << (8 * i));  
        }
        return bitdata16;
    } 
    else 
        return WS_NACK;  // Weather link doesn't respond
}

/**
 * @brief   Checks CRC for received data.
 *
 * @param   data : Data to check CRC.
 *
 * @return  CRC error if CRC is not 0, no error if CRC is 0.
 */
static uint16_t checkCRC(uint8_t data[]){
    uint8_t i;
    uint16_t crc=0;
    for (i=0; i<WS_SENSOR_IMAGE_LENGTH-3; i++)
        crc = crc_table [(crc >> 8) ^ data[i]] ^ (crc << 8);  // Calculate CRC
    if(crc)
        return WS_CRC_ERROR;  // CRC is not 0
    else
		return WS_NO_ERROR;  // CRC is 0
}

/**
 * @brief  Get ACK from weather monitor.
 *
 * @param  None
 *
 * @return Data received (ACK or NACK) or NACK if weather monitor doesn't respond.
 */
static uint8_t getAcknowledge(){
    uint8_t data;
    Timer_Start();

    #if WS_DATA_ACCESS_MODE == 3
    if(UART_GetBuffer(UART_RS232, &data, 1)){
        return data;  // ACK or NACK received
    } else {
        return WS_NACK;  // Weather monitor doesn't respond
    }

    #else
    uint8_t received = UART_RxChar(UART_RS232, &data);
    while((received == LOCAL_ERROR) && (Timer_GetTime() < WS_ACK_TIMEOUT)){
        received = UART_RxChar(UART_RS232, &data);
    }
    if(Timer_Stop() >= WS_ACK_TIMEOUT){  // If timeout happens, there is an error
        #if DEBUG_WM_EN
        UART_SendString(UART_DEBUG, "(WeatherStation) Timeout error occurred\r\n");
		#endif  // DEBUG_WM_EN
        return WS_NACK;  // Weather monitor doesn't respond
    } else {
        return data;  // ACK or NACK received
    }

    #endif  // WS_DATA_ACCESS_MODE
}

/**
 * @brief  Get weather monitor response.                \n
 *         Receive requested data from weather monitor.
 *
 * @param  command : Command to be sent (number of nibbles to read and bank identifier).
 * @param  address : Memory address where data is stored.
 *
 * @return Requested data or NACK if timeout occurred.
 */
static uint16_t getResponse(uint8_t command, uint8_t address){
	uint16_t bitdata16 = 0;
	uint8_t bitdata8, received, i=0;
    uint8_t i_max = ((command >> 4) + 1) / 2;
    
	Timer_Start();
	while((Timer_GetTime() < WS_RESPONSE_TIMEOUT) && (i < i_max)){
		received = UART_RxChar(UART_RS232, &bitdata8);
		while((Timer_GetTime() < WS_RESPONSE_TIMEOUT) && (received == LOCAL_ERROR))
			received = UART_RxChar(UART_RS232, &bitdata8);
		bitdata16 = bitdata16 | (bitdata8 << (8 * i));
		i++;
	}
	if(Timer_Stop() < WS_RESPONSE_TIMEOUT){
		return bitdata16;
	}
	
    return WS_NACK;  // Timeout occurred
}

/**
 * @brief  Get data from buffer.
 *
 * @param  command : Number of nibbles to read and identify bank.
 * @param  address : Address where to start reading.
 * @param  data    : Pointer to where to store data.
 *
 * @return Status of data reading: pending response, NACK, no error.
 */
uint8_t getData(uint8_t command, uint8_t address, uint16_t *data){
    static uint8_t pending_response = 0;

    if(!pending_response){
        getWeatherChipRam(command, address);  // Indicates variable to get
        pending_response = 1;
        return WS_PENDING_RESPONSE;  // Wait for ACK reception
    }
    if(pending_response == 1){
        if(getAcknowledge() == WS_ACK){
            pending_response = 2;
            return WS_PENDING_RESPONSE;  // Wait for data reception
        } else {
            pending_response = 0;
            return WS_NACK;  // Weather monitor doesn't respond
        }
    }
    pending_response = 0;
    *data = getResponse(command, address);  // Get data
    if(*data == WS_NACK){
        return WS_NACK;  // Data hasn't been received
    }
    return WS_NO_ERROR;  // Data has been received
}