/**
 * @file   RTC.c
 *
 * @brief  Driver for DS3231 real time clock module.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "RTC.h"                 // Header file
#include "../Protocols/I2C.h"    // Driver for I2C communications
#include "../Protocols/UART.h"   // Driver for UART communications
#include "../Utilities/Timer.h"  // Timing functions

/* Internal Variables */
static uint8_t Current_Century;  // Current century, not stored in DS3231

/**
 * @brief  Make an rtc_t variable.
 *
 * @param  hh : Value to set the hours.
 * @param  mm : Value to set the minutes.
 * @param  ss : Value to set the seconds.
 * @param  DD : Value to set the day number.
 * @param  MM : Value to set the month number.
 * @param  YY : Value to set the year.
 *
 * @return The rtc_t variable made.
 */
rtc_t RTC_Make(uint8_t hh, uint8_t mm, uint8_t ss, uint8_t DD, uint8_t MM, uint16_t YY){
	rtc_t time;
	time.ss = ss;
	time.mm = mm;
	time.hh = hh;
	time.DD = DD;
	time.MM = MM;
	time.YY = YY;
	return time;
}

/**
 * @brief  Set the time/date of RTC module.
 *
 * @param  time : time/date value (hours, minutes, seconds, day number, month number and year).
 *
 * @return Nothing
 */
void RTC_SetTime(rtc_t time){
	uint8_t hh, mm, ss, DD, MM, YY;
	
	ss = (((time.ss % 100) / 10) << 4) + (time.ss % 10);
	mm = (((time.mm % 100) / 10) << 4) + (time.mm % 10);
	hh = (((time.hh % 100) / 10) << 4) + (time.hh % 10);
	DD = (((time.DD % 100) / 10) << 4) + (time.DD % 10);
	MM = (((time.MM % 100) / 10) << 4) + (time.MM % 10);
	YY = (((time.YY % 100) / 10) << 4) + (time.YY % 10);
	Current_Century = (time.YY / 100) + 1;
	
	I2C_Start();      // Transmit START condition
	I2C_Write(0xD0);  // Address DS3231 for write
	I2C_Write(0);     // Set register pointer to 0
	I2C_Write(ss);    // Set seconds
	I2C_Write(mm);    // Set minutes
	I2C_Write(hh);    // Set hour
	I2C_Write(0);     // Set day of week
	I2C_Write(DD);    // Set day
	I2C_Write(MM);    // Set month
	I2C_Write(YY);    // Set year
	I2C_Stop();       // Transmit STOP condition
	
	#ifdef DEBUG_EN
	UART_SendString(UART_DEBUG, "(RTC) Set time: ");
	UART_SendInt(UART_DEBUG, time.hh, 2);
	UART_TxChar(UART_DEBUG, ':');
	UART_SendInt(UART_DEBUG, time.mm, 2);
	UART_TxChar(UART_DEBUG, ':');
	UART_SendInt(UART_DEBUG, time.ss, 2);
	UART_SendString(UART_DEBUG, "hs - ");
	UART_SendInt(UART_DEBUG, time.DD, 2);
	UART_TxChar(UART_DEBUG, '/');
	UART_SendInt(UART_DEBUG, time.MM, 2);
	UART_TxChar(UART_DEBUG, '/');
	UART_SendInt(UART_DEBUG, time.YY, 4);
	UART_SendString(UART_DEBUG, "\r\n");
	#endif  // DEBUG_EN
}

/**
 * @brief  Get the time/date value stored in RTC module.
 *
 * @param  time : Pointer to where to store time/date value.
 *
 * @return Nothing
 */
void RTC_GetTime(rtc_t *time){
	uint8_t hh, mm, ss, DD, MM, YY;
	
	I2C_Start();       // Transmit START condition
	I2C_Write(0xD0);   // Address DS3231 for write
	I2C_Write(0);      // Set register pointer to 0
	I2C_Stop();        // Transmit STOP condition

	I2C_Start();       // Transmit START condition
	I2C_Write(0xD1);   // Address DS3231 for read
	ss = I2C_Read(0);  // Read second, return ACK
	mm = I2C_Read(0);  // Read minute, return ACK
	hh = I2C_Read(0);  // Read hour, return NACK
	I2C_Read(0);       // Read day of the week, return ACK
	DD = I2C_Read(0);  // Read day, return ACK
	MM = I2C_Read(0);  // Read month, return ACK
	YY = I2C_Read(1);  // Read year, return NACK
	I2C_Stop();        // Transmit STOP condition
	
	time->ss = (ss & 0x0F) + 10 * (ss >> 4);
	time->mm = (mm & 0x0F) + 10 * (mm >> 4);
	time->hh = (hh & 0x0F) + 10 * ((hh & 0x30) >> 4);
	time->DD = (DD & 0x0F) + 10 * (DD >> 4);
	time->MM = (MM & 0x0F) + 10 * ((MM & 0x10) >> 4);
	time->YY = (YY & 0x0F) + 10 * (YY >> 4) + (Current_Century - 1) * 100;
	
	#ifdef DEBUG_EN
	UART_SendString(UART_DEBUG, "(RTC) Get time: ");
	UART_SendInt(UART_DEBUG, time->hh, 2);
	UART_TxChar(UART_DEBUG, ':');
	UART_SendInt(UART_DEBUG, time->mm, 2);
	UART_TxChar(UART_DEBUG, ':');
	UART_SendInt(UART_DEBUG, time->ss, 2);
	UART_SendString(UART_DEBUG, "hs - ");
	UART_SendInt(UART_DEBUG, time->DD, 2);
	UART_TxChar(UART_DEBUG, '/');
	UART_SendInt(UART_DEBUG, time->MM, 2);
	UART_TxChar(UART_DEBUG, '/');
	UART_SendInt(UART_DEBUG, time->YY, 4);
	UART_SendString(UART_DEBUG, "\r\n");
	#endif  // DEBUG_EN
}