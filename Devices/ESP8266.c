/**
 * @file   ESP8266.c
 *
 * @brief  Driver for ESP8266 WiFi module.                              \n
 *         Configure WiFi module (application and connection modes).    \n
 *         Join to an access point and get connection status.           \n
 *         Check, reset or restore WiFi module.                         \n
 *         Start or close TCP connections, and send data over them.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "ESP8266.h"                 // Header file
#include "../Utilities/System.h"     // Global system settings
#include <stdlib.h>                  // General purpose standard library
#include <string.h>                  // String functionalities
#include "../Protocols/UART.h"       // Driver for UART communications
#include "../Utilities/Timer.h"      // Timing functions
#include <util/delay.h>              // Delay functions
#include "../Utilities/Utilities.h"  // Various useful definitions, settings and functions

/* Internal Variables */
static enum ESP8266_RESPONSE_STATUS Response_Status;             // Last response status
static enum connectionMode_t {Single, Multiple} ConnectionMode;  // Connection mode set

/* Internal Functions Declaration */
void readResponse(char* expectedResponse);
void startReadResponse(char* expectedResponse);
uint8_t waitForExpectedResponse(char* ExpectedResponse);
uint8_t sendATandExpectResponse(char* ATCommand, char* expectedResponse);

/**
 * @brief  Read response given by WiFi module and compare it with expected response.   \n
 *         Response status is set.
 *
 * @param  expectedResponse : Response that WiFi module should return.
 *
 * @return Nothing
 */
void readResponse(char* expectedResponse){
	Response_Status = ESP8266_RESPONSE_WAITING;
	uint8_t expectedResponseLength = strlen(expectedResponse);
	uint8_t currentChar='0', n=0, buffer_pos=0;
	Timer_Start();
	while((buffer_pos < expectedResponseLength) && (Timer_GetTime() < ESP8266_DEFAULT_RESPONSE_TIMEOUT)){
		n = UART_RxChar(UART_WIFI, &currentChar);
		// Compare WiFi module response and expected response character by character
		while((n == 0) && (Timer_GetTime() < ESP8266_DEFAULT_RESPONSE_TIMEOUT)){
			n = UART_RxChar(UART_WIFI, &currentChar);
		}
		if(currentChar == expectedResponse[buffer_pos])
			buffer_pos++;  // Compare with next character
		else
			buffer_pos = 0;  // Start again with first character
	}
	if(Timer_Stop() >= ESP8266_DEFAULT_RESPONSE_TIMEOUT){
		Response_Status = ESP8266_RESPONSE_TIMEOUT;  // If timeout happens, there is an error
	} else {
		Response_Status = ESP8266_RESPONSE_FINISHED;  // WiFi module has responded
	}
}

/**
 * @brief  Start to read WiFi module response.   \n
 *         Response status is set.
 *
 * @param  expectedResponse : Response that WiFi module should return.
 *
 * @return Nothing
 */
void startReadResponse(char* expectedResponse){
	Response_Status = ESP8266_RESPONSE_STARTING;
	do {
		readResponse(expectedResponse);
	} while(Response_Status == ESP8266_RESPONSE_WAITING);
	
	#if DEBUG_WIFI_EN
	if(Response_Status == ESP8266_RESPONSE_TIMEOUT) UART_SendString(UART_DEBUG, "(WiFi) Timeout error occurred\r\n");
	else if(Response_Status == ESP8266_RESPONSE_FINISHED) UART_SendString(UART_DEBUG, "(WiFi) Response gotten successfully\r\n");
	#endif  // DEBUG_WIFI_EN
}

/**
 * @brief  Wait until WiFi module respond or timeout happen.   \n
 *
 * @param  expectedResponse : Response that WiFi module should return.
 *
 * @return Response status: timeout, success.
 */
uint8_t waitForExpectedResponse(char* expectedResponse){
	startReadResponse(expectedResponse);	// First read response
	if((Response_Status != ESP8266_RESPONSE_TIMEOUT))
		return LOCAL_OK;
	return LOCAL_ERROR;
}

/**
 * @brief  Send AT command to WiFi module and wait for the response.
 *
 * @param  ATCommand        : AT command to be sent.
 * @param  expectedResponse : Response that WiFi module should return.
 *
 * @return Response status: timeout, success.
 */
uint8_t sendATandExpectResponse(char* ATCommand, char* expectedResponse){
	UART_SendString(UART_WIFI, (uint8_t*)ATCommand);  // Send AT command to ESP8266
	UART_SendString(UART_WIFI, "\r\n");
	return waitForExpectedResponse(expectedResponse);
}

/**
 * @brief  Restore WiFi module (factory reset).
 *
 * @param  None
 *
 * @return ESP9266 response status.
 */
esp8266_response_status_t ESP8266_Restore(){
	#if DEBUG_WIFI_EN
	UART_SendString(UART_DEBUG, "(WiFi) Restoring ESP8266...\r\n");
	#endif  // DEBUG_WIFI_EN
	
	sendATandExpectResponse("AT+RESTORE", "OK");
	return Response_Status;
}

/**
 * @brief  Reset WiFi module (restart).
 *
 * @param  None
 *
 * @return ESP9266 response status.
 */
esp8266_response_status_t ESP8266_Reset(){
	#if DEBUG_WIFI_EN
	UART_SendString(UART_DEBUG, "(WiFi) Resetting ESP8266...\r\n");
	#endif  // DEBUG_WIFI_EN
	
	sendATandExpectResponse("AT+RST", "OK");
	return Response_Status;
}

/**
 * @brief  Set ESP8266 application mode.
 *
 * @param  mode : TCP transfer mode to set (normal or transparent).
 *
 * @return ESP9266 response status.
 */
esp8266_response_status_t ESP8266_ApplicationMode(uint8_t mode){
	#if DEBUG_WIFI_EN
	UART_SendString(UART_DEBUG, "(WiFi) Setting application mode...\r\n");
	#endif  // DEBUG_WIFI_EN
	
	char _atCommand[20];
	memset(_atCommand, 0, 20);
	sprintf(_atCommand, "AT+CIPMODE=%d", mode);
	_atCommand[19] = 0;
	
	sendATandExpectResponse(_atCommand, "OK");
	return Response_Status;
}

/**
 * @brief  Set ESP8266 connection mode.
 *
 * @param  mode : TCP connection mode to set (single or multiple).
 *
 * @return ESP9266 response status.
 */
esp8266_response_status_t ESP8266_ConnectionMode(uint8_t mode){
	#if DEBUG_WIFI_EN
	UART_SendString(UART_DEBUG, "(WiFi) Setting connection mode...\r\n");
	#endif  // DEBUG_WIFI_EN
	
	char _atCommand[20];
	memset(_atCommand, 0, 20);
	sprintf(_atCommand, "AT+CIPMUX=%d", mode);
	_atCommand[19] = 0;
	
	sendATandExpectResponse(_atCommand, "OK");
	if(Response_Status == ESP8266_RESPONSE_FINISHED){
		ConnectionMode = (mode) ? Multiple : Single;
	}
	return Response_Status;
}

/**
 * @brief  Test ESP8266.                                                    \n
 *         Check if WiFi module is working properly and switch echo off.
 *
 * @param None
 *
 * @return ESP9266 response status.
 */
esp8266_response_status_t ESP8266_Begin(){
	#if DEBUG_WIFI_EN
	UART_SendString(UART_DEBUG, "(WiFi) Checking ESP8266 connection...\r\n");
	#endif  // DEBUG_WIFI_EN
	
	if((sendATandExpectResponse("ATE0", "OK") == LOCAL_OK) || (sendATandExpectResponse("AT", "OK") == LOCAL_OK)){
		#if DEBUG_WIFI_EN
		UART_SendString(UART_DEBUG, "(WiFi) ESP8266 connected correctly\r\n");
		#endif  // DEBUG_WIFI_EN
	}
	return Response_Status;
}

/**
 * @brief  Close TCP connection.
 *
 * @param  None
 *
 * @return ESP9266 response status.
 */
esp8266_response_status_t ESP8266_Close(){
	#if DEBUG_WIFI_EN
	UART_SendString(UART_DEBUG, "(WiFi) Closing connection...\r\n");
	#endif  // DEBUG_WIFI_EN
	
	if(ConnectionMode == Single){
		sendATandExpectResponse("AT+CIPCLOSE", "OK");
	} else {
		sendATandExpectResponse("AT+CIPCLOSE=1", "OK");
	}
	return Response_Status;
}

/**
 * @brief  Set ESP8266 WiFi mode.
 *
 * @param  mode : WiFi mode to set (station, access point or both).
 *
 * @return ESP9266 response status.
 */
esp8266_response_status_t ESP8266_WIFIMode(uint8_t mode){
	#if DEBUG_WIFI_EN
	UART_SendString(UART_DEBUG, "(WiFi) Setting WiFi mode...\r\n");
	#endif  // DEBUG_WIFI_EN
	
	char _atCommand[20];
	memset(_atCommand, 0, 20);
	sprintf(_atCommand, "AT+CWMODE_DEF=%d", mode);
	_atCommand[19] = 0;
	
	sendATandExpectResponse(_atCommand, "OK");
	return Response_Status;
}

/**
 * @brief  Join to an access point.
 *
 * @param  ssid     : SSID of the access point to join.
 * @param  password : Password to connect to the access point.
 *
 * @return ESP9266 connection status with the access point.
 */
esp8266_joinAP_status_t ESP8266_JoinAccessPoint(char* ssid, char* password){
	#if DEBUG_WIFI_EN
	UART_SendString(UART_DEBUG, "(WiFi) Connecting to Access Point...\r\n");
	#endif  // DEBUG_WIFI_EN
	
	char _atCommand[60];
	uint8_t status;
	memset(_atCommand, 0, 60);
	sprintf(_atCommand, "AT+CWJAP_DEF=\"%s\",\"%s\"", ssid, password);
	_atCommand[59] = 0;
	
	if(sendATandExpectResponse(_atCommand, "OK") == LOCAL_OK){
		#if DEBUG_WIFI_EN
		UART_SendString(UART_DEBUG, "(WiFi) Connected to Access Point successfully\r\n");
		#endif  // DEBUG_WIFI_EN
		return ESP8266_WIFI_CONNECTED;
	} else{
		sendATandExpectResponse(_atCommand, "CWJAP:");
		status = UART_RxChar_Block(UART_WIFI);  // Get ESP8266 error code
		if(status == '1'){
			#if DEBUG_WIFI_EN
			UART_SendString(UART_DEBUG, "(WiFi) Timeout occurred while connecting to Access Point\r\n");
			#endif  // DEBUG_WIFI_EN
			return ESP8266_CONNECTION_TIMEOUT;
		} else if(status == '2') {
			#if DEBUG_WIFI_EN
			UART_SendString(UART_DEBUG, "(WiFi) Wrong password\r\n");
			#endif  // DEBUG_WIFI_EN
			return ESP8266_WRONG_PASSWORD;
		} else if(status == '3'){
			#if DEBUG_WIFI_EN
			UART_SendString(UART_DEBUG, "(WiFi) Target Access Point not found\r\n");
			#endif  // DEBUG_WIFI_EN
			return ESP8266_NOT_FOUND_TARGET_AP;
		} else if(status == '4'){
			#if DEBUG_WIFI_EN
			UART_SendString(UART_DEBUG, "(WiFi) Connection to Access Point failed\r\n");
			#endif  // DEBUG_WIFI_EN
			return ESP8266_CONNECTION_FAILED;
		} else {
			#if DEBUG_WIFI_EN
			UART_SendString(UART_DEBUG, "(WiFi) Error occurred while connecting to Access Point\r\n");
			#endif  // DEBUG_WIFI_EN
			return ESP8266_JOIN_UNKNOWN_ERROR;
		}
	}
}

/**
 * @brief  Get TCP connection status.
 *
 * @param  None
 *
 * @return ESP9266 TCP connection status.
 */
esp8266_connect_status_t ESP8266_ConnectionStatus(){
	#if DEBUG_WIFI_EN
	UART_SendString(UART_DEBUG, "(WiFi) Checking connection status...\r\n");
	#endif  // DEBUG_WIFI_EN
	
	uint8_t status;
	
	sendATandExpectResponse("AT+CIPSTATUS", "STATUS:");
	status = UART_RxChar_Block(UART_WIFI);  // Get ESP8266 connection status code
	if(status == '2'){
		#if DEBUG_WIFI_EN
		UART_SendString(UART_DEBUG, "(WiFi) Status: Connected to Access Point\r\n");
		#endif  // DEBUG_WIFI_EN
		return ESP8266_ConnectionStatus_TO_AP;
	} else if(status == '3'){
		#if DEBUG_WIFI_EN
		UART_SendString(UART_DEBUG, "(WiFi) Status: Transmission created\r\n");
		#endif  // DEBUG_WIFI_EN
		return ESP8266_CREATED_TRANSMISSION;
	} else if(status == '4'){
		#if DEBUG_WIFI_EN
		UART_SendString(UART_DEBUG, "(WiFi) Status: Transmission disconnected\r\n");
		#endif  // DEBUG_WIFI_EN
		return ESP8266_TRANSMISSION_DISCONNECTED;
	} else if(status == '5'){
		#if DEBUG_WIFI_EN
		UART_SendString(UART_DEBUG, "(WiFi) Status: Not connected to Access Point\r\n");
		#endif  // DEBUG_WIFI_EN
		return ESP8266_NOT_CONNECTED_TO_AP;
	} else {
		#if DEBUG_WIFI_EN
		UART_SendString(UART_DEBUG, "(WiFi) Status: Connection error\r\n");
		#endif  // DEBUG_WIFI_EN
		return ESP8266_CONNECT_UNKNOWN_ERROR;
	}
}

/**
 * @brief  Establish TCP connection.
 *
 * @param  connectionNumber : Number of the connection to be established.
 * @param  domain           : Domain to establish connection.
 * @param  port             : Remote port used to establish connection.
 *
 * @return ESP9266 response status.
 */
esp8266_response_status_t ESP8266_Start(uint8_t connectionNumber, char* domain, char* port){
	#if DEBUG_WIFI_EN
	UART_SendString(UART_DEBUG, "(WiFi) Establishing connection...\r\n");
	#endif  // DEBUG_WIFI_EN
	
	char _atCommand[60];
	memset(_atCommand, 0, 60);
	_atCommand[59] = 0;

	if(sendATandExpectResponse("AT+CIPMUX?", "CIPMUX:0") == LOCAL_OK)
		sprintf(_atCommand, "AT+CIPSTART=\"TCP\",\"%s\",%s", domain, port);
	else
		sprintf(_atCommand, "AT+CIPSTART=\"%d\",\"TCP\",\"%s\",%s", connectionNumber, domain, port);

	if(sendATandExpectResponse(_atCommand, "OK") == LOCAL_ERROR){
		if(sendATandExpectResponse(_atCommand, "CONNECT") == LOCAL_ERROR){
			if(Response_Status == ESP8266_RESPONSE_TIMEOUT){
				#if DEBUG_WIFI_EN
				UART_SendString(UART_DEBUG, "(WiFi) Timeout occurred while connecting\r\n");
				#endif  // DEBUG_WIFI_EN
				return ESP8266_RESPONSE_TIMEOUT;
			}
			#if DEBUG_WIFI_EN
			UART_SendString(UART_DEBUG, "(WiFi) Error occurred while connecting\r\n");
			#endif  // DEBUG_WIFI_EN
			return ESP8266_RESPONSE_ERROR;
		}
	}
	#if DEBUG_WIFI_EN
	UART_SendString(UART_DEBUG, "(WiFi) Connection established\r\n");
	#endif  // DEBUG_WIFI_EN
	return ESP8266_RESPONSE_FINISHED;
}

/**
 * @brief  Send data over established TCP connection.
 *
 * @param  data : Data to be sent.
 *
 * @return ESP9266 response status.
 */
esp8266_response_status_t ESP8266_Send(uint8_t* data){
	#if DEBUG_WIFI_EN
	UART_SendString(UART_DEBUG, "(WiFi) Sending message...\r\n");
	UART_SendString(UART_DEBUG, data);
	UART_SendString(UART_DEBUG, "\r\n");
	#endif  // DEBUG_WIFI_EN
	
	char _atCommand[20];
	memset(_atCommand, 0, 20);
	sprintf(_atCommand, "AT+CIPSEND=%d", (strlen(data) + 2));
	_atCommand[19] = 0;
	
	sendATandExpectResponse(_atCommand, "OK\r\n>");
	if(sendATandExpectResponse(data, "SEND OK") == LOCAL_ERROR){
		if(Response_Status == ESP8266_RESPONSE_TIMEOUT){
			#if DEBUG_WIFI_EN
			UART_SendString(UART_DEBUG, "(WiFi) Timeout occurred while sending message\r\n");
			#endif  // DEBUG_WIFI_EN
			return ESP8266_RESPONSE_TIMEOUT;
		}
		#if DEBUG_WIFI_EN
		UART_SendString(UART_DEBUG, "(WiFi) Error occurred while sending message\r\n");
		#endif  // DEBUG_WIFI_EN
		return ESP8266_RESPONSE_ERROR;
	}
	#if DEBUG_WIFI_EN
	UART_SendString(UART_DEBUG, "(WiFi) Message sent successfully\r\n");
	#endif  // DEBUG_WIFI_EN
	return ESP8266_RESPONSE_FINISHED;
}
