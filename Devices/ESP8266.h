/**
 * @file   ESP8266.h
 *
 * @brief  ESP8266.c header.                                                                \n
 *         Driver for WiFi module.                                                          \n
 *         Definitions for WiFi modes, connection modes and application modes.              \n
 *         Definitions of response status, TCP connection status and AP connection status.
 *
 * @author Hernn Sergio Morales (hernansergiomorales)
 */

#ifndef ESP2866_H_
#define ESP2866_H_

/* Inclusions */
#include <stdio.h>  // Standard input/output library

/* Definitions */
#define ESP8266_DEFAULT_BUFFER_SIZE       160
#define ESP8266_DEFAULT_RESPONSE_TIMEOUT  20000  // 20 milliseconds
// WiFi Mode
#define ESP8266_STATION                       1
#define ESP8266_ACCESSPOINT                   2
#define ESP8266_BOTH_STATION_AND_ACCESSPOINT  3
// Connection Mode
#define ESP8266_SINGLE    0
#define ESP8266_MULTIPLE  1
// Application Mode
#define ESP8266_NORMAL       0
#define ESP8266_TRANSPARENT  1
// Required Connection Values
#define ESP8266_DOMAIN    "things.ubidots.com"
#define ESP8266_PORT      "80"
#define ESP8266_SSID      "SSID"
#define ESP8266_PASSWORD  "PASSWORD"

/* Types Definition */

// ESP8266 Response Status
typedef enum ESP8266_RESPONSE_STATUS{
	ESP8266_RESPONSE_WAITING,
	ESP8266_RESPONSE_FINISHED,
	ESP8266_RESPONSE_TIMEOUT,
	ESP8266_RESPONSE_BUFFER_FULL,
	ESP8266_RESPONSE_STARTING,
	ESP8266_RESPONSE_ERROR
} esp8266_response_status_t;

// ESP8266 TCP Connection Status
typedef enum ESP8266_CONNECT_STATUS {
	ESP8266_ConnectionStatus_TO_AP,
	ESP8266_CREATED_TRANSMISSION,
	ESP8266_TRANSMISSION_DISCONNECTED,
	ESP8266_NOT_CONNECTED_TO_AP,
	ESP8266_CONNECT_UNKNOWN_ERROR
} esp8266_connect_status_t;

// ESP8266 Connection Status with the Access Point
typedef enum ESP8266_JOINAP_STATUS {
	ESP8266_WIFI_CONNECTED,
	ESP8266_CONNECTION_TIMEOUT,
	ESP8266_WRONG_PASSWORD,
	ESP8266_NOT_FOUND_TARGET_AP,
	ESP8266_CONNECTION_FAILED,
	ESP8266_JOIN_UNKNOWN_ERROR
} esp8266_joinAP_status_t;

/* Functions Declaration */
esp8266_response_status_t ESP8266_Restore();
esp8266_response_status_t ESP8266_Reset();
esp8266_response_status_t ESP8266_ApplicationMode(uint8_t mode);
esp8266_response_status_t ESP8266_ConnectionMode(uint8_t mode);
esp8266_response_status_t ESP8266_Begin();
esp8266_response_status_t ESP8266_Close();
esp8266_response_status_t ESP8266_WIFIMode(uint8_t mode);
esp8266_joinAP_status_t ESP8266_JoinAccessPoint(char* ssid, char* password);
esp8266_connect_status_t ESP8266_ConnectionStatus();
esp8266_response_status_t ESP8266_Start(uint8_t connectionNumber, char* domain, char* port);
esp8266_response_status_t ESP8266_Send(uint8_t* data);

#endif  // ESP2866_H_
