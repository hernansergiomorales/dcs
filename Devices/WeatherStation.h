/**
 * @file   WeatherStation.h
 *
 * @brief  WeatherStation.c header.          \n
 *         Driver for Davis Monitor access.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _WEATHER_STATION_H_
#define _WEATHER_STATION_H_

/* Inclusions */
#include "../Protocols/UART.h"       // Driver for UART communications
#include "../Utilities/Utilities.h"  // Various useful definitions, settings and functions

/* Definitions */

// Weather Monitor ACK and Response Timeouts
#define WS_ACK_TIMEOUT       60000  // 60 milliseconds
#define WS_RESPONSE_TIMEOUT  60000  // 60 milliseconds

// Davis Monitor Model on Station (Command, Address)
#define DAVIS_MODEL  0x14, 0x4d

// Weather Variables on Station (Command, Address)
#define WCR_INSIDE_TEMPERATURE   0x44, 0x30
#define WCR_OUTSIDE_TEMPERATURE  0x44, 0x56
#define WCR_OUTSIDE_HUMIDITY     0x24, 0x98
#define WCR_INSIDE_HUMIDITY      0x24, 0x80
#define WCR_WIND_SPEED           0x22, 0x5e
#define WCR_WIND_DIRECTION       0x44, 0xb4
#define WCR_WIND_CHILL           0x42, 0xa8
#define WCR_BAROMETER            0x44, 0x00
#define WCR_YEARLY_RAIN          0x44, 0xce
#define WCR_DAILY_RAIN           0x44, 0xd2
#define WCR_DEW_POINT            0x42, 0x8a

// High and Low Values on Station (Command, Address)
// -- Inside Temperature
#define WCR_HIGH_INSIDE_TEMPERATURE        0x44, 0x34
#define WCR_HIGH_INSIDE_TEMPERATURE_TIME   0x44, 0x3c
#define WCR_HIGH_INSIDE_TEMPERATURE_DATE   0x34, 0x44
#define WCR_LOW_INSIDE_TEMPERATURE         0x44, 0x38
#define WCR_LOW_INSIDE_TEMPERATURE_TIME    0x44, 0x40
#define WCR_LOW_INSIDE_TEMPERATURE_DATE    0x34, 0x47
// -- Outside Temperature
#define WCR_HIGH_OUTSIDE_TEMPERATURE       0x44, 0x5a
#define WCR_HIGH_OUTSIDE_TEMPERATURE_TIME  0x44, 0x62
#define WCR_HIGH_OUTSIDE_TEMPERATURE_DATE  0x34, 0x6a
#define WCR_LOW_OUTSIDE_TEMPERATURE        0x44, 0x5e
#define WCR_LOW_OUTSIDE_TEMPERATURE_TIME   0x44, 0x66
#define WCR_LOW_OUTSIDE_TEMPERATURE_DATE   0x34, 0x6d
// -- Wind Speed
#define WCR_HIGH_WIND_SPEED                0x42, 0x60
#define WCR_HIGH_WIND_SPEED_TIME           0x42, 0x64
#define WCR_HIGH_WIND_SPEED_DATE           0x32, 0x68
#define WCR_LOW_WIND_CHILL                 0x42, 0xac
#define WCR_LOW_WIND_CHILL_TIME            0x42, 0xb0
#define WCR_LOW_WIND_CHILL_DATE            0x32, 0xb4
#define WCR_HIGH_DEW_POINT                 0x42, 0x8e
#define WCR_HIGH_DEW_POINT_TIME            0x42, 0x96
#define WCR_HIGH_DEW_POINT_DATE            0x32, 0xa1
// -- Dew Point
#define WCR_LOW_DEW_POINT                  0x42, 0x92
#define WCR_LOW_DEW_POINT_TIME             0x42, 0x9a
#define WCR_LOW_DEW_POINT_DATE             0x32, 0x9e
// -- Inside Humidity
#define WCR_HIGH_INSIDE_HUMIDITY           0x24, 0x82
#define WCR_HIGH_INSIDE_HUMIDITY_TIME      0x44, 0x86
#define WCR_HIGH_INSIDE_HUMIDITY_DATE      0x34, 0x8e
#define WCR_LOW_INSIDE_HUMIDITY            0x24, 0x84
#define WCR_LOW_INSIDE_HUMIDITY_TIME       0x44, 0x8a
#define WCR_LOW_INSIDE_HUMIDITY_DATE       0x34, 0x91
// -- Outside Humidity
#define WCR_HIGH_OUTSIDE_HUMIDITY          0x24, 0x9a
#define WCR_HIGH_OUTSIDE_HUMIDITY_TIME     0x44, 0x9e
#define WCR_HIGH_OUTSIDE_HUMIDITY_DATE     0x34, 0xa6
#define WCR_LOW_OUTSIDE_HUMIDITY           0x24, 0x9c
#define WCR_LOW_OUTSIDE_HUMIDITY_TIME      0x44, 0xa2
#define WCR_LOW_OUTSIDE_HUMIDITY_DATE      0x34, 0xa9

// Average Values on Station (Bank, Address, Number of Nibbles)
#define CCR_AVG_INSIDE_TEMPERATURE   1, 0x94, 4
#define CCR_AVG_OUTSIDE_TEMPERATURE  1, 0x98, 4
#define CCR_AVG_WIND_SPEED           1, 0x9c, 2

// Calibration Numbers on Station
// -- Rain Calibration
#define WCR_CAL_RAIN                         0x44, 0xd6
#define WCR_CAL_RAIN_COMMAND                 0x43
#define WCR_CAL_RAIN_ADDRESS                 0xd6
// -- Barometric Pressure Calibration
#define WCR_CAL_BAROMETER                    0x44, 0x2c
#define WCR_CAL_BAROMETER_COMMAND            0x43
#define WCR_CAL_BAROMETER_ADDRESS            0x2c
// -- Inside Temperature Calibration
#define WCR_CAL_INSIDE_TEMPERATURE           0x44, 0x52
#define WCR_CAL_INSIDE_TEMPERATURE_COMMAND   0x43
#define WCR_CAL_INSIDE_TEMPERATURE_ADDRESS   0x52
// -- Outside Temperature Calibration
#define WCR_CAL_OUTSIDE_TEMPERATURE          0x44, 0x78
#define WCR_CAL_OUTSIDE_TEMPERATURE_COMMAND  0x43
#define WCR_CAL_OUTSIDE_TEMPERATURE_ADDRESS  0x78
// -- Humidity Calibration
#define WCR_CAL_HUMIDITY                     0x44, 0xda
#define WCR_CAL_HUMIDITY_COMMAND             0x43
#define WCR_CAL_HUMIDITY_ADDRESS             0xda

// Date/Time on Station
#define WCR_DATE_COMMAND  0x43
#define WCR_DATE_ADDRESS  0xc8
#define WCR_TIME_COMMAND  0x63
#define WCR_TIME_ADDRESS  0xbe
#define WCR_MONTH         0x14, 0xca
#define WCR_DAY           0x24, 0xc8
#define WCR_HOUR          0x24, 0xbe
#define WCR_MINUTES       0x24, 0xc0
#define WCR_SECONDS       0x24, 0xc2

// User Defined Calibration Numbers
#define SOFTWARE_CALIBRATION              1
#define SOFTWARE_CAL_RAIN                 0
#define SOFTWARE_CAL_BAROMETER            0
#define SOFTWARE_CAL_INSIDE_TEMPERATURE   0
#define SOFTWARE_CAL_OUTSIDE_TEMPERATURE  0
#define SOFTWARE_CAL_HUMIDITY             0

// Error Return Values
#define WS_POSITIVE_SIGNED_4NIBBLE_ERROR  0x7fff
#define WS_NEGATIVE_SIGNED_4NIBBLE_ERROR  0x8000
#define WS_POSITIVE_SIGNED_2NIBBLE_ERROR  0x7f
#define WS_NEGATIVE_SIGNED_2NIBBLE_ERROR  0x80
#define WS_NO_ERROR                       0
#define WS_HEADER_ERROR                   1
#define WS_CRC_ERROR                      2
#define WS_PENDING_RESPONSE               3

// Loop Constants
#define WS_HEADER               0x01
#define WS_SENSOR_IMAGE_LENGTH  18

// ACK, CR And LF
#define WS_ACK   0x06
#define WS_NACK  0x21
#define WS_CR    0x0d

/* Functions Declaration */

// Model Commands
uint16_t WeatherStation_GetWeatherLinkModel();

// Inside Temperature
uint8_t WeatherStation_GetInsideTemperature(int16_t *data);
void WeatherStation_GetAvgInsideTemperature(int16_t *avg);
void WeatherStation_GetHighInsideTemperature(int16_t *high, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes);
void WeatherStation_GetLowInsideTemperature(int16_t *low, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes);

// Outside Temperature
uint8_t WeatherStation_GetOutsideTemperature(int16_t *data);
void WeatherStation_GetAvgOutsideTemperature(int16_t *avg);
void WeatherStation_GetHighOutsideTemperature(int16_t *high, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes);
void WeatherStation_GetLowOutsideTemperature(int16_t *low, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes);

// Dew Point
uint8_t WeatherStation_GetDewPoint(uint16_t *data);
void WeatherStation_GetHighDewPoint(uint16_t *high, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes);
void WeatherStation_GetLowDewPoint(uint16_t *low, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes);

// Inside Humidity
uint8_t WeatherStation_GetInsideHumidity(uint16_t *data);
void WeatherStation_GetHighInsideHumidity(uint16_t *high, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes);
void WeatherStation_GetLowInsideHumidity(uint16_t *low, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes);

// Outside Humidity
uint8_t WeatherStation_GetOutsideHumidity(uint16_t *data);
void WeatherStation_GetHighOutsideHumidity(uint16_t *high, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes);
void WeatherStation_GetLowOutsideHumidity(uint16_t *low, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes);

// Wind Speed
uint8_t WeatherStation_GetWindSpeed(uint16_t *data);
void WeatherStation_GetAvgWindSpeed(uint8_t *avg);
void WeatherStation_GetHighWindSpeed(uint16_t *high, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes);

// Wind Chill
uint8_t WeatherStation_GetWindChill(uint16_t *data);
void WeatherStation_GetLowWindChill(uint16_t *low, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes);

// Wind Direction
uint8_t WeatherStation_GetWindDirection(uint16_t *data);

// Barometric Pressure
uint8_t WeatherStation_GetBarometricPressure(uint16_t *data);

// Daily Rain
uint8_t WeatherStation_GetDailyRain(uint16_t *data);

// Yearly Rain
uint8_t WeatherStation_GetYearlyRain(uint16_t *data);

// Timer Commands
uint8_t WeatherStation_EnableTimer();
uint8_t WeatherStation_DisableTimer();
void WeatherStation_SetStationTime(uint8_t month, uint8_t day, uint8_t hour, uint8_t minutes);
void WeatherStation_GetStationTime(uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *minutes, uint8_t *seconds);

// Weather Link Getter
uint16_t WeatherStation_GetLinkChipRam(uint8_t bank, uint8_t address, uint8_t number_of_nibbles);

// Loop Commands
void WeatherStation_SetCalibrationNumbers();
uint8_t WeatherStation_SetSamplePeriod(uint8_t newSamplePeriod);
uint8_t WeatherStation_InitLoopCommand(uint16_t packets);
uint8_t WeatherStation_GetLoopPackets();  

#endif  // _WEATHER_STATION_H_
