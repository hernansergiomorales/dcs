/**
 * @file   PTT.h
 *
 * @brief  PTT.c header.                                   \n
 *         Driver for PTT access.                          \n
 *         Definition of PTT commands and PTT responses.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _PTT_H_
#define _PTT_H_

/* Inclusions */
#include "../Protocols/UART.h"  // Driver for UART communications

/* Definitions */
#define PTT_TRANSMISSION_COMPLETE  0xFF   // Return code that indicates transmission has been completed
#define PTT_ACK_TIMEOUT            80000  // 80 milliseconds
// Status Messages
#define PTT_READY  0x8a
#define PTT_BUSY   0x85
#define PTT_ACK    0xaa
#define PTT_NACK   0x05
// Command Set
#define PTT_START                  0x10
#define PTT_STOP                   0x20
#define PTT_STMODE_AUTOREPEAT_OFF  0x30
#define PTT_STMODE_AUTOREPEAT_ON   0x32
#define PTT_STMODE_BUF1            0x30
#define PTT_STMODE_BUF2            0x31
#define PTT_LDMODE                 0x40
#define PTT_STDATA                 0x50
#define PTT_LDDATA                 0x60
#define PTT_BUF1                   0x00
#define PTT_BUF2                   0x01
#define PTT_NOP                    0x7a
#define PTT_LDINFO                 0x7d

/* Functions Declaration */
uint8_t PTT_Start();
uint8_t PTT_Stop();
uint8_t PTT_SetMode(uint8_t command);
uint8_t PTT_GetMode(uint8_t *autorepeatMode, uint8_t *buffer);
uint8_t PTT_StoreData(uint8_t buffer, uint8_t dataLength, uint8_t *data, uint16_t *p_step);
uint8_t PTT_ReadData(uint8_t buffer, uint8_t *dataLength, uint8_t *data);
uint8_t PTT_Nop();
uint8_t PTT_GetInfo(uint16_t *serialNumber, uint32_t *id, uint16_t *autorepeatPeriod);

#endif  // _PTT_H_