/**
 * @file   PTT.c
 *
 * @brief  Driver for PTT access.                                                        \n
 *         It can start and stop PTT transmission, read and store data in PTT buffers,
 *         set PTT mode and get current mode, get PTT information and send NOP command.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "PTT.h"                     // Header file
#include "../Utilities/Utilities.h"  // Various useful definitions, settings and functions
#include "../Utilities/Timer.h"      // Timing functions

/* Internal Functions Declaration */
static uint8_t getAck();

/**
 * @brief  Start PTT transmission.
 *
 * @param  None
 *
 * @return ACK or BUSY if PTT answers, NACK if not.
 */
uint8_t PTT_Start(){
	#if DEBUG_PTT_EN
	UART_SendString(UART_DEBUG, "(PTT) Sending START command...\r\n");
	#endif  // DEBUG_PTT_EN
	
	UART_TxChar(UART_PTT, PTT_START);
	return getAck();
}

/**
 * @brief  Stop PTT transmission.
 *
 * @param  None
 *
 * @return ACK or BUSY if PTT answers, NACK if not.
 */
uint8_t PTT_Stop(){
	#if DEBUG_PTT_EN
	UART_SendString(UART_DEBUG, "(PTT) Sending STOP command...\r\n");
	#endif  // DEBUG_PTT_EN
	
	UART_TxChar(UART_PTT, PTT_STOP);
	return getAck();
}	

/**
 * @brief  Set PTT mode.
 *
 * @param  command : Mode to be set
 *
 * @return ACK or BUSY if PTT answers, NACK if not.
 */
uint8_t PTT_SetMode(uint8_t command){
	#if DEBUG_PTT_EN
	UART_SendString(UART_DEBUG, "(PTT) Sending STMODE command...\r\n");
	#endif  // DEBUG_PTT_EN
	
	UART_TxChar(UART_PTT, command);
	return getAck();
}

/**
 * @brief  Get PTT current mode.
 *
 * @param  autorepeatMmode : Pointer to where to store autorepeat current configuration.
 * @param  buffer          : Pointer to where to store buffer current configuration.
 *
 * @return ACK or BUSY if PTT answers, NACK if not.
 */
uint8_t PTT_GetMode(uint8_t *autorepeatMode, uint8_t *buffer){
	#if DEBUG_PTT_EN
	UART_SendString(UART_DEBUG, "(PTT) Sending LDMODE command...\r\n");
	#endif  // DEBUG_PTT_EN
	
	uint8_t ack, received, data;
	UART_TxChar(UART_PTT, PTT_LDMODE);
	ack = getAck();
	received = UART_RxChar(UART_PTT, &data);
    while(received == LOCAL_ERROR){
        received = UART_RxChar(UART_PTT, &data);
    }
	*buffer = data & PTT_STMODE_BUF2;
	*autorepeatMode = data & PTT_STMODE_AUTOREPEAT_ON;

	return ack; 
}

/**
 * @brief  Stores data in specific PTT buffer.                                   \n
 *         The task is set up to work cooperatively, the task is divided into
 *         subtasks and on each function call one of the parts is executed.      \n
 *         Every byte is sent in a different subtask.
 *
 * @param  buffer     : Buffer to where to store data.
 * @param  dataLength : The length of the data to be sent.
 * @param  data       : Pointer to data to be sent.
 * @param  p_step     : Current step in data storage.
 *
 * @return ACK or BUSY if PTT answers, NACK if not.
 */
uint8_t PTT_StoreData(uint8_t buffer, uint8_t dataLength, uint8_t *data, uint16_t *p_step){
	static uint8_t i, j;
	if(*p_step == 0){  // First step: send command
		#if DEBUG_PTT_EN
		UART_SendString(UART_DEBUG, "(PTT) Sending STDATA command...\r\n");
		UART_SendString(UART_DEBUG, "(PTT) Data: ");
		UART_SendInt(UART_DEBUG, data[0], 0);
		for(i=1; i<dataLength; i++){
			UART_SendString(UART_DEBUG, (uint8_t*)", ");
			UART_SendInt(UART_DEBUG, data[i], 0);
		}
		UART_SendString(UART_DEBUG, "\r\n");
		#endif  // DEBUG_PTT_EN
		UART_TxChar(UART_PTT, PTT_STDATA | buffer);
		(*p_step)++;  // Next step is to send data length
		i = 0;
		j = 0;
		return getAck();
	}
	if(*p_step == 1){  // Second step: send data length
		UART_TxChar(UART_PTT, (dataLength + 3) / 4);
		(*p_step)++;  // Next step is to send data
		return PTT_BUSY;
	}
	#if SIMULATION
	uint8_t k;
	for(k=0; k<dataLength/4; k++)
	#endif  // SIMULATION
	if(i < dataLength){  // Next steps: send data
		UART_TxChar(UART_PTT, data[i]);
		#if DEBUG_PTT_EN
		UART_SendString(UART_DEBUG, (uint8_t*)"(PTT) Sent ");
		UART_SendHex(UART_DEBUG, data[i], 2);
		UART_SendString(UART_DEBUG, (uint8_t*)" (");
		UART_SendInt(UART_DEBUG, i+1, 0);
		UART_SendString(UART_DEBUG, (uint8_t*)"/");
		UART_SendInt(UART_DEBUG, dataLength, 0);
		UART_SendString(UART_DEBUG, (uint8_t*)")\r\n");
		#endif  // DEBUG_PTT_EN
		i++;
		(*p_step)++;
		#if SIMULATION
		if((i % 4) == 0)
		#endif  // SIMULATION
		return ((i == dataLength) && (dataLength % 4 == 0)) ? PTT_TRANSMISSION_COMPLETE : PTT_BUSY;
	}
	if(j < (4 - dataLength % 4)){  // Last steps: complete message with zeros
		UART_TxChar(UART_PTT, 0);
		#if DEBUG_PTT_EN
		UART_SendString(UART_DEBUG, "(PTT) Sent 0x00 to complete\r\n");
		#endif  // DEBUG_PTT_EN
		j++;
		(*p_step)++;
		return (j == (4 - dataLength % 4)) ? PTT_TRANSMISSION_COMPLETE : PTT_BUSY;
	}
	return PTT_TRANSMISSION_COMPLETE;  // Data transmission has been completed
}

/**
 * @brief  Read data in specific PTT buffer.
 *
 * @param  buffer     : Buffer where to read data.
 * @param  dataLength : The length of the data to be read.
 * @param  data       : Pointer to where to store read data.
 *
 * @return ACK or BUSY if PTT answers, NACK if not.
 */
uint8_t PTT_ReadData(uint8_t buffer, uint8_t *dataLength, uint8_t *data){
	#if DEBUG_PTT_EN
	UART_SendString(UART_DEBUG, "(PTT) Sending LDDATA command...\r\n");
	#endif  // DEBUG_PTT_EN
	
	uint8_t ack, received;
	UART_TxChar(UART_PTT, PTT_LDDATA | buffer);
	ack = getAck();
	received = UART_RxChar(UART_PTT, dataLength);
    while(received == LOCAL_ERROR)
        received = UART_RxChar(UART_PTT, dataLength);
    *dataLength = *dataLength * 4;
    UART_RecvString(UART_PTT, data, (*dataLength));

	return ack; 
}

/**
 * @brief  PTT no operation function.
 *
 * @param  None
 *
 * @return ACK or BUSY if PTT answers, NACK if not.
 */
uint8_t PTT_Nop(){
	#if DEBUG_PTT_EN
	UART_SendString(UART_DEBUG, "(PTT) Sending NOP command...\r\n");
	#endif  // DEBUG_PTT_EN
	
	UART_TxChar(UART_PTT, PTT_NOP);
	return getAck();
}

/**
 * @brief  Get PTT information.                                  \n
 *         Get serial number, ID and autorepeat period of PTT.
 *
 * @param  serialNumber     : Pointer to where to store PTT serial number.
 * @param  id               : Pointer to where to store PTT id.
 * @param  autorepeatPeriod : Pointer to where to store PTT autorepeat period.
 *
 * @return ACK or BUSY if PTT answers, NACK if not.
 */
uint8_t PTT_GetInfo(uint16_t *serialNumber, uint32_t *id, uint16_t *autorepeatPeriod){
	#if DEBUG_PTT_EN
	UART_SendString(UART_DEBUG, "(PTT) Sending LDINFO command...\r\n");
	#endif  // DEBUG_PTT_EN
	
	uint8_t i, received, data, ack;
	UART_TxChar(UART_PTT, PTT_LDINFO);
	ack = getAck();
	for(i=0; i<2; i++){
		received = UART_RxChar(UART_PTT, &data);
    	while(received == LOCAL_ERROR)
        	received = UART_RxChar(UART_PTT, &data);
		*serialNumber = ((*serialNumber) << 8) | data;
	}
	for(i=0; i<4; i++){
		received = UART_RxChar(UART_PTT, &data);
    	while(received == LOCAL_ERROR)
        	received = UART_RxChar(UART_PTT, &data);
		*id = ((*id) << 8) | data;
	}
	for(i=0; i<2; i++){
		received = UART_RxChar(UART_PTT, &data);
    	while(received == LOCAL_ERROR)
        	received = UART_RxChar(UART_PTT, &data);
		*autorepeatPeriod = ((*autorepeatPeriod) << 8) | data;
	}
	return ack;
}

/**
 * @brief  Get ACK.
 *
 * @param  None
 *
 * @return ACK or BUSY if PTT answers, NACK if not.
 */
static uint8_t getAck(){
	Timer_Start();
	uint8_t data;
    uint8_t received = UART_RxChar(UART_PTT, &data);
    while((received == LOCAL_ERROR) && (Timer_GetTime() < PTT_ACK_TIMEOUT)){
        received = UART_RxChar(UART_PTT, &data);
    }
    if(Timer_Stop() >= PTT_ACK_TIMEOUT){
		return PTT_NACK;  // Timeout occurred
	} else {
		return data;  // ACK or BUSY received
	}
}