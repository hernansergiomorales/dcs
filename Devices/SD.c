/**
 * @file   SD.c
 *
 * @brief  Driver for SD card.                                      \n
 *         Initialize MMC/SD card or umount SD filesystem.          \n
 *         Get file and directory information.                      \n
 *         Files handle (create, open, close, write, read,
 *         delete, seek, truncate, rename and change attributes).   \n
 *         Directories handle (create and open).
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "SD.h"                 // Header file
#include <stdio.h>              // Standard input/output library
#include "../Protocols/UART.h"  // Driver for UART communications

/* Internal Variables */
FATFS fatfs;  // FAT filesystem

/**
 * @brief  Initialize MMC/SD card and mount filesystem.
 *
 * @param  None
 *
 * @return SD access result code.
 *
 * @note   This function must be called before using any other SD functionality.
 */
SD_RESULT SD_Begin(){
	#if DEBUG_SD_EN
	UART_SendString(UART_DEBUG, "(SD) Checking SD connection...\r\n");
	#endif  // DEBUG_SD_EN
	
	if(mmc_init() == 0){
		#if DEBUG_SD_EN
		UART_SendString(UART_DEBUG, "(SD) Mounting file system...\r\n");
		#endif  // DEBUG_SD_EN
		return (SD_RESULT) f_mount(0, &fatfs);
	}
	#if DEBUG_SD_EN
	UART_SendString(UART_DEBUG, "(SD) SD not ready\r\n");
	#endif  // DEBUG_SD_EN
	return SD_NOT_READY;
}

/**
 * @brief  Open file in SD filesystem.
 *
 * @param  p_file   : Pointer to file to be opened.
 * @param  fileName : Name of file to be opened.
 * @param  mode     : File access mode.
 *
 * @return SD access result code.
 */
SD_RESULT SD_FileOpen(FIL *p_file, const uint8_t *fileName, uint8_t mode){
	#if DEBUG_SD_EN
	UART_SendString(UART_DEBUG, "(SD) Opening file...\r\n");
	#endif  // DEBUG_SD_EN
	
	FRESULT res;
	if(mode & SD_APPEND_MODE){  // If file access mode is Append
		res = f_open(p_file, fileName, FA_CREATE_NEW);  // Create new file
		#if DEBUG_SD_EN
		if(res == FR_OK) UART_SendString(UART_DEBUG, "(SD) New file created successfully\r\n");
		#endif  // DEBUG_SD_EN
		res = f_open(p_file, fileName, FA_WRITE);  // Try to open an existing file
		if(res == FR_OK){  // If file could be opened or created
			#if DEBUG_SD_EN
			UART_SendString(UART_DEBUG, "(SD) File opened successfully\r\n");
			#endif  // DEBUG_SD_EN
			return (SD_RESULT) f_lseek(p_file, p_file->fsize);  // Seek to end of file
		} else {
			#if DEBUG_SD_EN
			UART_SendString(UART_DEBUG, "(SD) Failed to open file\r\n");
			#endif  // DEBUG_SD_EN
			return (SD_RESULT) res;  // Return SD access error code
		}
	} else {  // If file access mode is not Append
		return (SD_RESULT) f_open(p_file, fileName, mode);  // Try to open file
	}
}

/**
 * @brief  Change pointer position to file content.
 *
 * @param  p_file : Pointer to file to be sought.
 * @param  offset : Number of positions to move.
 *
 * @return SD access result code.
 */
SD_RESULT SD_FileLseek(FIL *p_file, uint32_t offset){
	return (SD_RESULT) f_lseek(p_file, offset);
}

/**
 * @brief  Read file content.
 *
 * @param  p_file      : Pointer to file to be read.
 * @param  buffer      : Buffer where to store data read.
 * @param  bytesToRead : Number of bytes to read.
 * @param  p_bytesRead : Pointer to where to return the number of bytes read.
 *
 * @return SD access result code.
 */
SD_RESULT SD_FileRead(FIL *p_file, uint8_t *buffer, uint16_t bytesToRead, uint16_t *p_bytesRead){
	return (SD_RESULT) f_read(p_file, buffer, bytesToRead, p_bytesRead);
}

/**
 * @brief  Write data in file.
 *
 * @param  p_file      : Pointer to file to be written.
 * @param  buffer      : Buffer with data to write.
 * @param  bytesToRead : Number of bytes to read.
 * @param  p_bytesRead : Pointer to where to return the number of bytes read.
 *
 * @return SD access result code.
 */
SD_RESULT SD_FileWrite(FIL *p_file, uint8_t *buffer, uint16_t bytesToWrite, uint16_t *p_bytesWritten){
	#if DEBUG_SD_EN
	UART_SendString(UART_DEBUG, "(SD) Writing data in file...\r\n");
	#endif  // DEBUG_SD_EN
	
	FRESULT res;
	res = f_write(p_file, buffer, bytesToWrite, p_bytesWritten);
	if(res == FR_OK){
		#if DEBUG_SD_EN
		UART_SendString(UART_DEBUG, "(SD) Making writing effective...\r\n");
		#endif  // DEBUG_SD_EN
		return (SD_RESULT) f_sync(p_file);  // Sync the data in case of power lost
	}
	#if DEBUG_SD_EN
	UART_SendString(UART_DEBUG, "(SD) Failed to write data in file\r\n");
	#endif  // DEBUG_SD_EN
	return (SD_RESULT) res;
}

/**
 * @brief  Close file.
 *
 * @param  p_file : Pointer to file to be closed.
 *
 * @return SD access result code.
 */
SD_RESULT SD_FileClose(FIL *p_file){
	#if DEBUG_SD_EN
	UART_SendString(UART_DEBUG, "(SD) Closing file...\r\n");
	#endif  // DEBUG_SD_EN
	
	return (SD_RESULT) f_close(p_file);
}

/**
 * @brief  Truncate file content.
 *
 * @param  p_file : Pointer to file to be truncated.
 *
 * @return SD access result code.
 */
SD_RESULT SD_FileTruncate(FIL *p_file){
	return (SD_RESULT) f_truncate(p_file);
}

/**
 * @brief  Open directory in SD filesystem.
 *
 * @param  p_dir   : Pointer to directory to be read.
 * @param  dirPath : Path to directory to be opened.
 *
 * @return SD access result code.
 */
SD_RESULT SD_DirOpen(DIR *p_dir, const uint8_t *dirPath){
	return (SD_RESULT) f_opendir(p_dir, dirPath);
}

/**
 * @brief  Read directory information.
 *
 * @param  p_dir    : Pointer to directory to be read.
 * @param  fileInfo : Pointer to where to return directory information.
 *
 * @return SD access result code.
 */
SD_RESULT SD_DirRead(DIR *p_dir, FILINFO *fileInfo){
	return (SD_RESULT) f_readdir(p_dir, fileInfo);
}

/**
 * @brief  Create directory in SD filesystem.
 *
 * @param  dirPath : Path of directory to be created.
 *
 * @return SD access result code.
 */
SD_RESULT SD_DirCreate(const uint8_t *dirPath){
	return (SD_RESULT) f_mkdir(dirPath);
}

/**
 * @brief  Get file information.
 *
 * @param  filePath : Path of file to get information.
 * @param  fileInfo : Pointer to where to return file information.
 *
 * @return SD access result code.
 */
SD_RESULT SD_FileInfo(const uint8_t *filePath, FILINFO *fileInfo){
	return (SD_RESULT) f_stat(filePath, fileInfo);
}

/**
 * @brief  Rename file in SD filesystem.
 *
 * @param  pathOld : Path of file to be renamed.
 * @param  pathNew : New path of the file.
 *
 * @return SD access result code.
 */
SD_RESULT SD_FileRename(const uint8_t *pathOld, const uint8_t *pathNew){
	return (SD_RESULT) f_rename(pathOld, pathNew);
}

/**
 * @brief  Change file attributes.
 *
 * @param  filePath : Path of file.
 * @param  value : Attribute bits.
 * @param  mask : Attribute mask to change.
 *
 * @return SD access result code.
 */
SD_RESULT SD_FileChangeAttribute(const uint8_t *filePath, uint8_t value, uint8_t mask){
	return (SD_RESULT) f_chmod(filePath, value, mask);
}

/**
 * @brief  Delete file in SD filesystem.
 *
 * @param  filePath : Path of file to be deleted.
 *
 * @return SD access result code.
 */
SD_RESULT SD_FileDelete(const uint8_t *filePath){
	return (SD_RESULT) f_unlink(filePath);
}

/**
 * @brief  Umount SD filesystem.
 *
 * @param  None
 *
 * @return SD access result code.
 *
 * @note   This function must be called before extracting SD card, to prevent errors.
 */
SD_RESULT SD_End(){
	#if DEBUG_SD_EN
	UART_SendString(UART_DEBUG, "(SD) Umounting file system...\r\n");
	#endif  // DEBUG_SD_EN
	
	return (SD_RESULT) f_mount(0, NULL);  // Umount filesystem
}