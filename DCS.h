/**
 * @file   DCS.h
 *
 * @brief  DCS.c header.                          \n
 *         DCS tasks manager.                     \n
 *         Main system tasks (scheduled tasks).
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _DCS_H_
#define _DCS_H_

/* Inclusions */
#include "Utilities/System.h"        // Global system settings
#include "Utilities/Utilities.h"     // Various useful definitions, settings and functions
#include "Devices/PTT.h"             // Driver for PTT access
#include "Data/Buffer.h"             // Manage buffers to send to PTT
#include "Data/Data.h"               // Weather variables data storage
#include "Protocols/UART.h"          // Driver for UART communications
#include "Devices/WeatherStation.h"  // Driver for Davis Monitor access
#include "Devices/SD.h"              // Driver for SD card
#include "Devices/RTC.h"             // Driver for DS3231 real time clock module
#include "Devices/ESP8266.h"         // Driver for WiFi module
#include "Utilities/ADC.h"           // Driver for analogical-digital converser
#include "Utilities/Timer.h"         // Timing functions
#include <util/delay.h>              // Delay functions
#include <avr/io.h>                  // Driver for AVR input/output

/* Definitions */

// Period Times
#if SIMULATION
#define OBSERVATION_TIME  120    // 2 minutes
#define REPEAT_TIME       18     // 18 seconds
#define SAMPLE_TIME       24     // 24 seconds
#define MONITOR_TIME      1      // 1 second
#define CHECK_PERIOD      300    // 5 minutes
#else
#define OBSERVATION_TIME  14400  // 4 hours
#define REPEAT_TIME       90     // 90 seconds
#define SAMPLE_TIME       120    // 2 minutes
#define MONITOR_TIME      1      // 1 second
#define CHECK_PERIOD      60     // 5 minutes
#endif  // SIMULATION

// Current Time for Initializing DS3231
#define INIT_SECONDS  0
#define INIT_MINUTES  0
#define INIT_HOURS    12
#define INIT_DAY      26
#define INIT_MONTH    2
#define INIT_YEAR     2022

// Time Pressing SD Button to Extract SD Card
#define TIME_SD_EXTRACT  2  // 2 seconds

/* Type Definitions */

typedef struct led_status {
	uint8_t PTT_LedStatus   : 1;
	uint8_t WiFi_LedStatus  : 1;
	uint8_t SD_LedStatus    : 1;
} led_status_t;

typedef struct working_status {
	uint8_t PTT_WorkingStatus   : 1;
	uint8_t WiFi_WorkingStatus  : 1;
	uint8_t SD_WorkingStatus    : 1;
} working_status_t;

/* Functions Declaration */
// DCS Initialization Task
void DCS_Init_Task();
// DCS Scheduled Tasks
void DCS_UpdateData_Task();
void DCS_UpdateBuffers_Task();
void DCS_PTT_Task();
void DCS_SD_Task();
void DCS_WiFi_Task();
void DCS_ConectionsCheck_Task();
void DCS_SystemMonitor_Task();

#endif  // _DCS_H_