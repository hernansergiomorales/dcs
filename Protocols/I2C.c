/**
 * @file   I2C.c
 *
 * @brief  Driver for inter-integrated circuit (I2C) communications.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Inclusions */
#include "I2C.h"  // Header file

/**
 * @brief  Initialize I2C module.
 *
 * @param  None
 *
 * @return Nothing
 *
 * @note   This function must be called before using any other functionality.
 */
void I2C_Init(void){
	TWSR = 0x00;  // Set prescaler bits to zero
	TWBR = 12;    // SCL frequency is 200kHz for XTAL = 8MHz
	TWCR = 0x04;  // Enable the I2C module
}

/**
 * @brief  Send data via I2C protocol.
 *
 * @param  data : Byte to send.
 *
 * @return Nothing
 */
void I2C_Write(unsigned char data){
  TWDR = data;
  TWCR = (1 << TWINT) | (1 << TWEN);
  while ((TWCR & (1 << TWINT)) == 0);
}

/**
 * @brief  Read data via I2C protocol.
 *
 * @param  isLast : Indicates if received byte is the last and must send an ACK.
 *
 * @return Value received via I2C protocol.
 */
uint8_t I2C_Read(uint8_t isLast){
  if (!isLast)	//send ACK
    TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA); // Send ACK
  else			
    TWCR = (1 << TWINT) | (1 << TWEN); // Send NACK
  while ((TWCR & (1 << TWINT)) == 0);
  return TWDR;
}

/**
 * @brief  Start communication via I2C protocol.
 *
 * @param  None
 *
 * @return Nothing
 */
void I2C_Start(void){
  TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
  while ((TWCR & (1 << TWINT)) == 0);
}

/**
 * @brief  Stop communication via I2C protocol.
 *
 * @param  None
 *
 * @return Nothing
 */
void I2C_Stop(){
  TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);
}
