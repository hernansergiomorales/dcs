/**
 * @file   I2C.h
 *
 * @brief  I2C.c header.                                               \n
 *         Driver for inter-integrated circuit (I2C) communications.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _I2C_H_
#define _I2C_H_

/* Inclusions */
#include <avr/io.h>  // Driver for AVR input/output

/* Functions Declaration */
void I2C_Init(void);
void I2C_Write(unsigned char data);
uint8_t I2C_Read(uint8_t isLast);
void I2C_Start(void);
void I2C_Stop();

#endif  // _I2C_H_