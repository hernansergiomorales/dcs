/**
 * @file   UART.h
 *
 * @brief  UART.c header.             \n
 *         Driver for UART modules.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

#ifndef _UART_H_
#define _UART_H_

#include "../Utilities/System.h"  // Global system settings
#include <math.h>                 // Mathematics functions
#include <avr/io.h>               // Include AVR std. library file

/* UART settings */
#define UART0_BAUDRATE 115200  // Debug UART
#define UART1_BAUDRATE 2400	   // RS232 UART
#define UART2_BAUDRATE 2048	   // PTT UART
#define UART3_BAUDRATE 115200  // WiFi UART
#define DOUBLE_SPEED_MODE 1    // Enable double speed mode

/* Function Declarations */
uint8_t UART_Init(uint8_t uart_num);                      // UART initialization
uint8_t UART_EnableRxInterrupt(uint8_t uart_num);         // Enable UART Rx interrupt
uint8_t UART_DisableRxInterrupt(uint8_t uart_num);        // Disable UART Rx interrupt
uint8_t UART_RxChar(uint8_t uart_num, uint8_t *buffer);   // Data reception via UART protocol
uint8_t UART_RxChar_Block(uint8_t uart_num);              // Blocking data reception via UART protocol
uint8_t UART_TxChar(uint8_t uart_num, uint8_t data);      // Data transmission via UART protocol
uint8_t UART_SendString(uint8_t uart_num, uint8_t *str);  // Send string via UART protocol
uint8_t UART_SendInt(uint8_t uart_num, uint32_t n, uint8_t zeros_left);              // Send integer value via UART protocol
uint8_t UART_SendHex(uint8_t uart_num, uint32_t n, uint8_t zeros_left);              // Send hexadecimal value via UART protocol
uint8_t UART_SendDouble(uint8_t uart_num, double n, uint8_t zeros_left, uint8_t d);  // Send double value via UART protocol
uint8_t UART_SendBlock(uint8_t uart_num, uint8_t *buffer, uint32_t buffer_size);     // Send block of bytes via UART protocol
uint8_t UART_RecvString(uint8_t uart_num, uint8_t *buffer, uint16_t buffer_size);    // Receive string via UART protocol
uint16_t UART_GetBuffer(uint8_t uart_num, uint8_t* buf, uint16_t buf_size);          // Get received data from buffer

#endif  // _UART_H_