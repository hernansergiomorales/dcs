/**
 * @file   UART.c
 *
 * @brief  Driver for UART modules.                                                    \n
 *         It can be sent bytes, strings or blocks of bytes.                           \n
 *         It can be sent decimal and hexadecimal integer values, or double values.    \n
 *         It can be received bytes or strings.                                        \n
 *         Rx interrupts can be enabled so received data will be buffered.
 *
 * @author Hern�n Sergio Morales (hernansergiomorales)
 */

/* Includes */
#include "UART.h"                    // Header file
#include "../Utilities/System.h"     // Global system settings
#include <math.h>                    // Mathematics functions
#include <stdlib.h>                  // General purpose standard library
#include <avr/interrupt.h>           // Driver for AVR interrupts
#include "../Utilities/Utilities.h"  // Various useful definitions, settings and functions

/* Size of buffer for every UART */
#define UART0_BufferSize 256
#define UART1_BufferSize 256
#define UART2_BufferSize 256
#define UART3_BufferSize 256

/* Internal Variables */
// Buffer for every UART
uint8_t UART0_Buffer[UART0_BufferSize], UART1_Buffer[UART1_BufferSize],
        UART2_Buffer[UART2_BufferSize], UART3_Buffer[UART3_BufferSize];
// Pointers to write and read in buffers
uint32_t UART0_pRead=0, UART0_pWrite=0, UART1_pRead=0, UART1_pWrite=0,
         UART2_pRead=0, UART2_pWrite=0, UART3_pRead=0, UART3_pWrite=0;

/**
 * @brief  Initialize UART module.
 *
 * @param  uart_num : Number of UART module.
 *
 * @return Initialization status: error, OK.
 *
 * @note   This function must be called before using any other functionality of the same UART module.
 */
uint8_t UART_Init(uint8_t uart_num){
    uint32_t uart_baud_prescale;
    switch (uart_num){
        case 0:
            #if DOUBLE_SPEED_MODE
            UCSR0A |= (1 << U2X1);
            uart_baud_prescale = (uint32_t)round(((((double)F_CPU / ((double)UART0_BAUDRATE * 8.0))) - 1.0));   // Define prescale value
            #else
            uart_baud_prescale = (uint32_t)round(((((double)F_CPU / ((double)UART0_BAUDRATE * 16.0))) - 1.0));  // Define prescale value
            #endif  // DOUBLE_SPEED_MODE
            DDRE |= (1 << PINE1);                     // Tx as output
            DDRE &= ~(1 << PINE0);                    // Rx as input
            UCSR0B |= (1 << RXEN0) | (1 << TXEN0);    // Enable UART transmitter and receiver
            UCSR0C |= (1 << UCSZ00) | (1 << UCSZ01);  // Set 8N1 mode (8 bit data, no parity and 1 stop bit)
            UBRR0 = uart_baud_prescale;               // Load prescale value
            break;
        case 1:
            #if DOUBLE_SPEED_MODE
            UCSR1A |= (1 << U2X1);
            uart_baud_prescale = (uint32_t)round(((((double)F_CPU / ((double)UART1_BAUDRATE * 8.0))) - 1.0));   // Define prescale value
            #else
            uart_baud_prescale = (uint32_t)round(((((double)F_CPU / ((double)UART1_BAUDRATE * 16.0))) - 1.0));  // Define prescale value
            #endif  // DOUBLE_SPEED_MODE
            DDRD |= (1 << PIND3);                     // Tx as output
            DDRD &= ~(1 << PIND2);                    // Rx as input
            UCSR1B |= (1 << RXEN1) | (1 << TXEN1);    // Enable UART transmitter and receiver
            UCSR1C |= (1 << UCSZ10) | (1 << UCSZ11);  // Set 8N1 mode (8 bit data, no parity and 1 stop bit)
            UBRR1 = uart_baud_prescale;               // Load prescale value
            break;
        case 2:
            #if DOUBLE_SPEED_MODE
            UCSR2A |= (1 << U2X1);
            uart_baud_prescale = (uint32_t)round(((((double)F_CPU / ((double)UART2_BAUDRATE * 8.0))) - 1.0));   // Define prescale value
            #else
            uart_baud_prescale = (uint32_t)round(((((double)F_CPU / ((double)UART2_BAUDRATE * 16.0))) - 1.0));  // Define prescale value
            #endif  // DOUBLE_SPEED_MODE
            DDRH |= (1 << PINH1);                     // Tx as output
            DDRH &= ~(1 << PINH0);                    // Rx as input
            UCSR2B |= (1 << RXEN2) | (1 << TXEN2);    // Enable UART transmitter and receiver
            UCSR2C |= (1 << UCSZ20) | (1 << UCSZ21);  // Set 8N1 mode (8 bit data, no parity and 1 stop bit)
            UBRR2 = uart_baud_prescale;               // Load prescale value
            break;
        case 3:
            #if DOUBLE_SPEED_MODE
            UCSR3A |= (1 << U2X1);
            uart_baud_prescale = (uint32_t)round(((((double)F_CPU / ((double)UART3_BAUDRATE * 8.0))) - 1.0));   // Define prescale value
            #else
            uart_baud_prescale = (uint32_t)round(((((double)F_CPU / ((double)UART3_BAUDRATE * 16.0))) - 1.0));  // Define prescale value
            #endif  // DOUBLE_SPEED_MODE
            DDRJ |= (1 << PINJ1);                     // Tx as output
            DDRJ &= ~(1 << PINJ0);                    // Rx as input
            UCSR3B |= (1 << RXEN3) | (1 << TXEN3);    // Enable UART transmitter and receiver
            UCSR3C |= (1 << UCSZ30) | (1 << UCSZ31);  // Set 8N1 mode (8 bit data, no parity and 1 stop bit)
            UBRR3 = uart_baud_prescale;               // Load prescale value
            break;
        default: return LOCAL_ERROR;  // Return error
    }
    return LOCAL_OK;  // Return OK
}

/**
 * @brief  Enable UART Rx interrupt.
 *
 * @param  uart_num : Number of UART module.
 *
 * @return Initialization status: error, OK.
 */
uint8_t UART_EnableRxInterrupt(uint8_t uart_num){
    switch (uart_num){
        case 0:
            UCSR0B |= (1<<RXCIE0);  // Enable Rx interrupt
            break;
        case 1:
            UCSR1B |= (1<<RXCIE1);  // Enable Rx interrupt
            break;
        case 2:
            UCSR2B |= (1<<RXCIE2);  // Enable Rx interrupt
            break;
        case 3:
            UCSR3B |= (1<<RXCIE3);  // Enable Rx interrupt
            break;
        default: return LOCAL_ERROR;
    }
    return LOCAL_OK;
}

/**
 * @brief  Disable UART Rx interrupt.
 *
 * @param  uart_num : Number of UART module.
 *
 * @return Initialization status: error, OK.
 */
uint8_t UART_DisableRxInterrupt(uint8_t uart_num){
    switch (uart_num){
        case 0:
            UCSR0B &= ~(1<<RXCIE0);    // Disable Rx interrupt
            break;
        case 1:
            UCSR1B &= ~(1<<RXCIE1);    // Disable Rx interrupt
            break;
        case 2:
            UCSR2B &= ~(1<<RXCIE2);    // Disable Rx interrupt
            break;
        case 3:
            UCSR3B &= ~(1<<RXCIE3);    // Disable Rx interrupt
            break;
        default: return LOCAL_ERROR;
    }
    return LOCAL_OK;
}

/**
 * @brief  Blocking data reception via UART protocol.                    \n
 *         Function waits and doesn't return until a byte is received.
 *
 * @param  uart_num : Number of UART module.
 *
 * @return The received byte.
 */
uint8_t UART_RxChar_Block(uint8_t uart_num){
    switch (uart_num){
        case 0:
            while(!(UCSR0A & (1 << RXC0)));  // Wait until new data is received
            return UDR0;
        case 1:
            while(!(UCSR1A & (1 << RXC1)));  // Wait until new data is received
            return UDR1;
        case 2:
            while(!(UCSR2A & (1 << RXC2)));  // Wait until new data is received
            return UDR2;
        case 3:
            while(!(UCSR3A & (1 << RXC3)));  // Wait until new data is received
            return UDR3;
        default: return 0;
    }
}

/**
 * @brief  Data reception via UART protocol.                  \n
 *         Check if a byte has been received and return it.
 *
 * @param  uart_num : Number of UART module.
 * @param  buffer   : Pointer to where return received data.
 *
 * @return Indicates if data has been received (OK) or not (error).
 */
uint8_t UART_RxChar(uint8_t uart_num, uint8_t *buffer){
    switch (uart_num){
        case 0:
            if(UCSR0A & (1 << RXC0)){
                *buffer = UDR0;
                return LOCAL_OK;
            }
            return LOCAL_ERROR;  // No data received
        case 1:
            if(UCSR1A & (1 << RXC1)){
                *buffer = UDR1;
                return LOCAL_OK;
            }
            return LOCAL_ERROR;  // No data received
        case 2:
            if(UCSR2A & (1 << RXC2)){
                *buffer = UDR2;
                return LOCAL_OK;
            }
            return LOCAL_ERROR;  // No data received
        case 3:
            if(UCSR3A & (1 << RXC3)){
                *buffer = UDR3;
                return LOCAL_OK;
            }
            return LOCAL_ERROR;  // No data received
        default: return LOCAL_ERROR;
    }
}

/**
 * @brief  Data transmission via UART protocol.
 *
 * @param  uart_num : Number of UART module.
 * @param  data     : Data to send.
 *
 * @return Transmission status: error, OK.
 */
uint8_t UART_TxChar(uint8_t uart_num, uint8_t data){
    switch (uart_num){
        case 0:
            UDR0 = data;                     // Write data to be transmitted
            while (!(UCSR0A & (1<<UDRE0)));  // Wait until data transmit and buffer get empty
            break;
        case 1:
            UDR1 = data;                     // Write data to be transmitted
            while (!(UCSR1A & (1<<UDRE1)));  // Wait until data transmit and buffer get empty
            break;
        case 2:
            UDR2 = data;                     // Write data to be transmitted
            while (!(UCSR2A & (1<<UDRE2)));  // Wait until data transmit and buffer get empty
            break;
        case 3:
            UDR3 = data;                     // Write data to be transmitted
            while (!(UCSR3A & (1<<UDRE3)));  // Wait until data transmit and buffer get empty
            break;
        default: return LOCAL_ERROR;
    }
    return LOCAL_OK;
}

/**
 * @brief  Send string via UART protocol.
 *
 * @param  uart_num : Number of UART module.
 * @param  str      : String to be transmitted.
 *
 * @return Transmission status: error, OK.
 */
uint8_t UART_SendString(uint8_t uart_num, uint8_t *str){
    uint16_t i = 0;
    while(str[i] != 0){
        UART_TxChar(uart_num, str[i]);  // Send every char of string till the NULL
        i++;
    }
    return LOCAL_OK;
}

/**
 * @brief  Send integer value via UART protocol.                                   \n
 *         Integer value is sent digit by digit, and can be added zeros at left.
 *
 * @param  uart_num   : Number of UART module.
 * @param  number     : Integer to be transmitted.
 * @param  zeros_left : Number of zeros to put at left.
 *
 * @return Transmission status: error, OK.
 */
uint8_t UART_SendInt(uint8_t uart_num, uint32_t number, uint8_t zeros_left){
    int8_t i;
    uint8_t digits[11];
    digits[10] = 0;
    for(i=9; i>=0; i--){
        digits[i] = number%10 + 0x30;  // Get lowest digit
        number /= 10;  // Discard lowest digit
    }
    i = 0;
    if(zeros_left > 0){
        while(i < 10-zeros_left && digits[i] == '0')
            i++;  // Discard zeros on left
    } else {
        while(i < 9 && digits[i] == '0')
            i++;  // Discard all zeros on left
    }
    return UART_SendString(uart_num, digits+i);
}

/**
 * @brief  Send hexadecimal value via UART protocol.                     \n
 *         Hexadecimal value is sent digit by digit, with "0x" prefix,
 *         and can be added zeros at left.
 *
 * @param  uart_num   : Number of UART module.
 * @param  number     : Integer to be transmitted as an hexadecimal value.
 * @param  zeros_left : Number of zeros to put at left.
 *
 * @return Transmission status: error, OK.
 */
uint8_t UART_SendHex(uint8_t uart_num, uint32_t number, uint8_t zeros_left){
    int8_t i, digit;
    uint8_t digits[11];
    digits[10] = 0;
    for(i=9; i>=0; i--){
        digit = number % 16;  // Get lowest digit
        digits[i] = (digit < 10) ? (digit + '0') : ((digit - 10) + 'A');
        number /= 16;  // Discard lowest digit
    }
    i = 0;
    if(zeros_left > 0){
        while((i < (10 - zeros_left)) && (digits[i] == '0'))
        i++;  // Discard zeros on left
        } else {
        while(i < 9 && digits[i] == '0')
        i++;  // Discard all zeros on left
    }
    UART_SendString(uart_num, "0x");
    return UART_SendString(uart_num, digits+i);
}

/**
 * @brief  Send block of bytes via UART protocol.
 *
 * @param  uart_num    : Number of UART module.
 * @param  buffer      : Buffer of data to be transmitted.
 * @param  buffer_size : Number of bytes to transmit.
 *
 * @return Transmission status: error, OK.
 */
uint8_t UART_SendBlock(uint8_t uart_num, uint8_t *buffer, uint32_t buffer_size){
    uint16_t i = 0;
    for(i=0; i<buffer_size; i++){
        UART_TxChar(uart_num, buffer[i]);  // Send every byte of the block
    }
    return LOCAL_OK;
}

/**
 * @brief  Send double value via UART protocol.                         \n
 *         Double value is sent digit by digit, with '.' separating
 *         integer and decimal parts, and can be added zeros at left.
 *
 * @param  uart_num   : Number of UART module.
 * @param  number     : Double value to be transmitted.
 * @param  zeros_left : Number of zeros to put at left.
 * @param  decimals   : Number of decimals to be sent.
 *
 * @return Transmission status: error, OK.
 */
uint8_t UART_SendDouble(uint8_t uart_num, double number, uint8_t zeros_left, uint8_t decimals){
    if(number < 0){
        UART_TxChar(uart_num, '-');
        number = fabs(number);
    }
    uint32_t n = (uint32_t) number;
    UART_SendInt(uart_num, n, zeros_left);  // Transmit integer part
    UART_TxChar(uart_num, '.');
    n = round((number - n) * pow(10, decimals));
    UART_SendInt(uart_num, n, decimals);  // Transmit decimal part
    return LOCAL_OK;
}

/**
 * @brief  Receive string via UART protocol.
 *
 * @param  uart_num    : Number of UART module.
 * @param  buffer      : Buffer where to store received bytes.
 * @param  buffer_size : Number of bytes to receive.
 *
 * @return Useless value yet.
 */
uint8_t UART_RecvString(uint8_t uart_num, uint8_t *buffer, uint16_t buffer_size){
    for(uint16_t i=0; i<buffer_size; i++){
        buffer[i] = UART_RxChar_Block(uart_num);
    }
    buffer[buffer_size] = 0;
    return LOCAL_OK;
}

/**
 * @brief  Get received data from buffer.
 *
 * @param  uart_num : Number of UART module.
 * @param  buf      : Buffer where to store received and buffered bytes.
 * @param  buf_size : Maximum number of bytes to return.
 *
 * @return Number of bytes returned.
 */
uint16_t UART_GetBuffer(uint8_t uart_num, uint8_t* buf, uint16_t buf_size){
    uint16_t buffer_size;
    uint16_t i = 0;
    switch (uart_num){
        case 0:
            buffer_size = (buf_size > UART0_BufferSize) ? UART0_BufferSize : buf_size;
            while((UART0_pRead != UART0_pWrite) && (i < buffer_size)){
                buf[i] = UART0_Buffer[UART0_pRead];  // Get bytes from buffer
                UART0_pRead = (UART0_pRead + 1) % UART0_BufferSize;
                i++;
            }
            return i;
        case 1:
            buffer_size = (buf_size > UART1_BufferSize) ? UART1_BufferSize : buf_size;
            while((UART1_pRead != UART1_pWrite) && (i < buffer_size)){
                buf[i] = UART1_Buffer[UART1_pRead];  // Get bytes from buffer
                UART1_pRead = (UART1_pRead + 1) % UART1_BufferSize;
                i++;
            }
            return i;
        case 2:
            buffer_size = (buf_size > UART2_BufferSize) ? UART2_BufferSize : buf_size;
            while((UART2_pRead != UART2_pWrite) && (i < buffer_size)){
                buf[i] = UART2_Buffer[UART2_pRead];  // Get bytes from buffer
                UART2_pRead = (UART2_pRead + 1) % UART2_BufferSize;
                i++;
            }
            return i;
        case 3:
            buffer_size = (buf_size > UART3_BufferSize) ? UART3_BufferSize : buf_size;
            while((UART3_pRead != UART3_pWrite) && (i < buffer_size)){
                buf[i] = UART3_Buffer[UART3_pRead];  // Get bytes from buffer
                UART3_pRead = (UART3_pRead + 1) % UART3_BufferSize;
                i++;
            }
            return i;
        default: return 0;
    }
}

/* Interrupt service routines for UARTs Rx interrupt */
ISR(USART0_RX_vect){
    UART0_Buffer[UART0_pWrite] = UDR0;
    UCSR0A &= ~(1<<RXC0);
    UART0_pWrite = (UART0_pWrite + 1) % UART0_BufferSize;
}
ISR(USART1_RX_vect){  // USART1 Rx ISR
    UART1_Buffer[UART1_pWrite] = UDR1;
    UCSR1A &= ~(1<<RXC1);
    UART1_pWrite = (UART1_pWrite + 1) % UART1_BufferSize;
}
ISR(USART2_RX_vect){  // USART2 Rx ISR
    UART2_Buffer[UART2_pWrite] = UDR0;
    UCSR2A &= ~(1<<RXC2);
    UART2_pWrite = (UART2_pWrite + 1) % UART2_BufferSize;
}
ISR(USART3_RX_vect){  // USART3 Rx ISR
    UART3_Buffer[UART3_pWrite] = UDR1;
    UCSR3A &= ~(1<<RXC3);
    UART3_pWrite = (UART3_pWrite + 1) % UART3_BufferSize;
}